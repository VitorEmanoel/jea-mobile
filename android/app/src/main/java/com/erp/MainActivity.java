package com.erp;

import com.facebook.react.ReactActivity;
import android.view.View;
import android.os.Bundle;
import android.os.Handler;


public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */

   @Override
   protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();
         final View decorView = getWindow().getDecorView();
                decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                hideSystemUI();
                                            }
                                        }, 1000);
                            }
                        }
                    });
   }

   @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

   private void hideSystemUI() {
           View decorView = getWindow().getDecorView();
           decorView.setSystemUiVisibility(
                   View.SYSTEM_UI_FLAG_IMMERSIVE
                   | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                   | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                   | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                   // Hide the nav bar and status bar
                   | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                   | View.SYSTEM_UI_FLAG_FULLSCREEN);
       }

       // Shows the system bars by removing all the flags
       // except for the ones that make the content appear under the system bars.
       private void showSystemUI() {
           View decorView = getWindow().getDecorView();
           decorView.setSystemUiVisibility(
                   View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                   | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                   | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
       }

  @Override
  protected String getMainComponentName() {
    return "erp";
  }
}
