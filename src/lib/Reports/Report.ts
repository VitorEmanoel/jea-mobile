import Axios, { AxiosResponse } from 'axios';

export interface BaseRequest {
    url: string;
    token: string
}

export const BaseReport = <T, E extends BaseRequest>(path: string): Report<T, E> => {
    const client = Axios.create()
    return request => {
        console.log(request)
        return client.get(`${request.url}/reports/${path}`, {headers: {"Authorization": request.token}})
    }
}

export interface MapReports {
    [key: string]: Report<any, any>
}

export interface Report<T, E extends BaseRequest> {
    (request: E): Promise<AxiosResponse<T>>
}
