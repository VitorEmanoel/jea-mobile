import { AxiosResponse } from 'axios';
import { call, put } from "redux-saga/effects";
import {CreateRequest, DeleteRequest, ListAllRequest, ListRequest, ServiceRequest, UpdateRequest} from "./request";
import {RootState} from "../../store/types";
import {Module, ModuleAction, ModuleState} from "../Module";
import produce from "immer";
import {FindAll} from "../Module/actions";

export type ServiceFunc = (request: ServiceRequest) => Promise<AxiosResponse<any>>

export interface IService<T> {
    name: string;
    findAll(request: ListAllRequest): Promise<AxiosResponse<T[]>>;
    find(request: ListRequest): Promise<AxiosResponse<T>>;
    create(request: CreateRequest<T>): Promise<AxiosResponse<any>>;
    update(request: UpdateRequest<T>):Promise<AxiosResponse<any>>;
    delete(request: DeleteRequest): Promise<AxiosResponse<any>>
}

export interface IStateBuilder {
    RequestHandler: (name: string, type: string, serviceFunc: ServiceFunc) => (action: ModuleAction) => Generator<any>
    Reducer: (name: string, type: string) => (state: ModuleState<any>, action: ModuleAction) => ModuleState<any>
}

export interface DataFlowActions {
    FindAll(request: ListAllRequest): ModuleAction;
}

export interface IDataflow<T> {
    getService: () => IService<T>
    Actions: DataFlowActions;
}

export const DataFlow = <T>(module: Module<T>, service: IService<T>): IDataflow<T> => {

    return {
        getService(): IService<T> {
            return service;
        },
        Actions:  {
            FindAll(request: ListAllRequest): ModuleAction {
                return FindAll(module.name, request)
            }
        }
    }
}

export const StateBuilder: IStateBuilder = {
    RequestHandler: (name,type, serviceFunc) => function* (action): Generator<any> {
        try {
            const response: any = yield call(serviceFunc, {...action.payload})
            if (response.status >= 200 && response.status <= 299) {
                yield put({type: SuccessfulAction(name, type), payload: response.data})
                return
            }
            yield put({type: ErrorAction(name, type), payload: {error: true, message: response.data, code: response.status}})
        } catch (e) {
            yield put({type: ErrorAction(name, type), payload: {error: true, message: e.message, code: e.code}})
        }
    },
    Reducer: (name,type) => (state= INITIAL_STATE, action) => {
        return produce(state, draft => {
            switch (action.type) {
                case RequestAction(name, type):
                    draft.loading = true;
                    draft.error = {error: false};
                    draft.rows = [];
                    draft.action = type;
                    draft.success = false;
                    break;
                case ErrorAction(name, type):
                    draft.loading = false;
                    draft.error = {error: true, ...action.payload}
                    draft.success = false
                    break;

                case SuccessfulAction(name, type):
                    draft.loading = false;
                    draft.error = {error: false};
                    draft.rows = action.payload.items;
                    draft.total = action.payload.total;
                    draft.success = true
                    break;
            }
        })
    },
}


const INITIAL_STATE: ModuleState<any> = {
    error: {error: false},
    objects: {},
    loading: false,
    rows: [],
    total: 0,
    action: "",
    success: false,
}

function RequestAction(name: string, type: string): string {
    return `${name}/${type}_REQUEST`;
}

function SuccessfulAction(name: string, type: string): string {
    return `${name}/${type}_SUCCESSFUL`;
}

function ErrorAction(name: string, type: string): string {
    return `${name}/${type}_ERROR`;
}
