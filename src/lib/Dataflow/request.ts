export interface DataParams {
    [key: string]: any;
}

export interface ServiceRequest {
    url: string;
    token?: string;
    data?: DataParams
}

export interface IdentifyRequest {
    id: any;
}

export interface EntityRequest<T> {
    entity: T
}

export interface ListAllRequest extends ServiceRequest{
    limit?: number;
    page?: number;
}

export interface ListRequest extends ServiceRequest, IdentifyRequest {

}

export interface CreateRequest<T> extends ServiceRequest, EntityRequest<T> {

}

export interface UpdateRequest<T> extends ServiceRequest, IdentifyRequest, EntityRequest<T> {

}

export interface DeleteRequest extends ServiceRequest, IdentifyRequest {

}
