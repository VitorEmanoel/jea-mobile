import React from 'react';
import {ListRenderItemInfo} from "react-native";
import {OrderFilterItem} from "../../components/OrderButton/complement";
import {FilterItem} from "../../components/SearchBar/complement";
import {RootState} from "../../store/types";
import {Error} from "../../store/baseTypes";
import {IDataflow} from "../Dataflow";

export interface ModuleState<T> {
    error: Error
    loading: boolean;
    rows: T[];
    objects: ModuleObject<T>
    success: boolean;
    action: string;
    total: number;
}

export interface ModuleObject<T> {
    [name: string]: T[];

}

export interface ModuleAction {
    type: string;
    payload?: any;
}

export interface GeneralConfiguration {
    showInModuleList?: boolean;
    showInStory?: boolean;
}

export interface ModuleStatistics {
    header?: () => JSX.Element;
    charts?: () => JSX.Element;
    metrics?: () => JSX.Element;
}

export interface ModuleForm<T> {
    title: string;
    icon?: string;
    form: (item: T) => JSX.Element
}

export interface Module<T> {
    name: string;
    configuration: GeneralConfiguration;
    multiCompany: boolean;
    displayName: string;
    titleIcon?: string;
    icon: ModuleIcons;
    colors?: ModuleColors;
    form?: ModuleForm<T>;
    moduleList?: ModuleList<T>;
    dataFlow: IDataflow<T>;
    statistics?: ModuleStatistics;
}

export interface ModuleList<T> {

    orderFilterList: OrderFilterItem[];
    filterList: FilterItem[];

    emptyContent: React.ReactElement;
    renderItem(info: ListRenderItemInfo<T>): React.ReactElement;
}

export interface ModuleColors {
    module: string;
}

export interface ModuleIcons {
    module: string;
    story?: string;
}
