import {ModuleAction} from "./index";
import {ListAllRequest} from "../Dataflow/request";

export function FindAll(name: string, request: ListAllRequest): ModuleAction {
    return ActionBuilder(name, "FINDALL", request)
}

function ActionBuilder(name: string, type: string, payload?: any):ModuleAction {
    return {type: `${name}/${type}_REQUEST`, payload: payload}
}
