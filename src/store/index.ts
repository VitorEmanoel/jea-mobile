import { createStore, applyMiddleware, compose } from 'redux'
import { persistStore, persistReducer, PersistConfig } from "redux-persist"
import { Reducers } from "./rootReducer";
import createSagaMiddleware from 'redux-saga'
import {rootSaga} from "./rootSaga";
import {RootState} from "./types";
import AsyncStorage from "@react-native-community/async-storage";
// @ts-ignore
import {offlineMiddleware, suspendSaga, consumeActionMiddleware} from "redux-offline-queue";
import Reactotron from "../config/reactotron"


// @ts-ignore
const sagaMonitor = Reactotron.createSagaMonitor()

const sagaMiddleware = createSagaMiddleware({sagaMonitor})

const rootReducer = Reducers();

const persistConfig: PersistConfig<RootState> = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ["register"]
}

// @ts-ignore
const persistedReducer = persistReducer(persistConfig, rootReducer)

const middlewares = []
middlewares.push(offlineMiddleware())
middlewares.push(suspendSaga(sagaMiddleware))
middlewares.push(consumeActionMiddleware())

// @ts-ignore
// @ts-ignore
const store = createStore(persistedReducer, compose(applyMiddleware(...middlewares), Reactotron.createEnhancer()))

export const Persistor = persistStore(store)
export const Store = store

sagaMiddleware.run(rootSaga)
