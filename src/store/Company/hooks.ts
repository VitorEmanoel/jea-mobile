import {ModuleState} from "../../lib/Module";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {RootState} from "../types";

export function useCompany(moduleState: ModuleState<any>, isMultiCompany = true): ModuleState<any> {

    const [filteredState, setFilteredState] = useState(moduleState)
    const companies = useSelector((state: RootState) => state.company);

    useEffect(() => {
        const newModuleState = moduleState
        const rows = moduleState.rows.filter(item => {
            if (!isMultiCompany)
                return true
            if (item.company && item.company.id) {
                const company = companies.companies.filter(comp => comp.id === item.company.id && comp.selected)
                return company.length > 0;
            }
            return true
        })
        setFilteredState({...newModuleState, rows: rows})
    }, [moduleState])
    return filteredState;
}
