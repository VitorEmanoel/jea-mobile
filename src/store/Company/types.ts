import {Company} from "../../modules/Company";
import {Error} from "../baseTypes";
import {ListAllRequest} from "../../lib/Dataflow/request";

export interface CustomCompany extends Company {
    selected: boolean
}

export interface CompanyState {
    companies: CustomCompany[]
    loading: boolean;
    error: Error;
    open: boolean;
}

export enum CompanyActions {
    SELECT = "COMPANY/SELECT",
    UNSELECT = "COMPANY/UNSELECT",
    OPEN_SELECT = "COMPANY/OPEN_SELECT",
    CLOSE_SELECT = "COMPANY/CLOSE_SELECT",
    TOGGLE_SELECT = "COMPANY/TOGGLE_SELECT",
    FINDALL_REQUEST = "COMPANY/FINDALL_REQUEST",
    FINDALL_SUCCESSFUL = "COMPANY/FINDALL_SUCCESSFUL",
    FINDALL_ERROR = "COMPANY/FINDALL_ERROR"
}

export interface CompanyPayload {
    id: number;
}

export interface CompanyUnSelectAction {
    type: typeof CompanyActions.UNSELECT
    payload: CompanyPayload
}

export interface CompanySelectAction {
    type: typeof CompanyActions.SELECT
    payload: CompanyPayload
}

export interface CompanyFindAllRequestAction {
    type: typeof CompanyActions.FINDALL_REQUEST
    payload: ListAllRequest
}

interface FindAllResponse<T> {
    items: T[]
    total: number;
}

export interface CompanyFindAllSuccessfulAction {
    type: typeof CompanyActions.FINDALL_SUCCESSFUL
    payload: FindAllResponse<Company>[]
}

export interface CompanyFindAllErrorAction {
    type: typeof CompanyActions.FINDALL_ERROR
    payload: Error
}

export interface CompanyOpenSelectAction {
    type: typeof CompanyActions.OPEN_SELECT
}

export interface CompanyCloseSelectAction {
    type: typeof CompanyActions.CLOSE_SELECT
}

export interface CompanyToggleSelectAction {
    type: typeof CompanyActions.TOGGLE_SELECT
}

export type CompanyAction = CompanySelectAction | CompanyUnSelectAction | CompanyFindAllRequestAction | CompanyFindAllSuccessfulAction | CompanyFindAllErrorAction | CompanyOpenSelectAction | CompanyCloseSelectAction | CompanyToggleSelectAction
