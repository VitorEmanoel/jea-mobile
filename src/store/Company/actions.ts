import {CompanyAction, CompanyActions} from "./types";
import {ListAllRequest} from "../../lib/Dataflow/request";

export interface CompanyFunctionActions {
    companySelect(id: number): CompanyAction;
    companyUnselect(id: number): CompanyAction;
}

export function companySelect(id: number): CompanyAction {
    return {type: CompanyActions.SELECT, payload: {id}}
}

export function companyUnselect(id: number): CompanyAction {
    return {type: CompanyActions.UNSELECT, payload: {id}}
}

export function findAll(request: ListAllRequest): CompanyAction {
    return {type: CompanyActions.FINDALL_REQUEST, payload: request}
}

export function companyOpenSelect(): CompanyAction {
    return {type: CompanyActions.OPEN_SELECT}
}

export function companyCloseSelect(): CompanyAction {
    return {type: CompanyActions.CLOSE_SELECT}
}

export function companyToggleSelect(): CompanyAction {
    return {type: CompanyActions.TOGGLE_SELECT}
}
