import {CompanyAction, CompanyActions, CompanyState} from "./types";
import produce from "immer";

const INITIAL_STATE: CompanyState = {
    companies: [],
    error: {error: false},
    loading: false,
    open: false
}

export const CompanyReducer = (state: CompanyState = INITIAL_STATE, action: CompanyAction) => {
    return produce(state, draft => {
        switch (action.type) {
            case CompanyActions.SELECT:
                draft.companies = draft.companies.map(item => {
                    if (item.id == action.payload.id){
                        item.selected = !item.selected
                    }
                    return item
                })
                break;
            case CompanyActions.UNSELECT:
                draft.companies = draft.companies.map(item => {
                    if (item.id == action.payload.id){
                        item.selected = false
                    }
                    return item
                })
                break;

            case CompanyActions.FINDALL_REQUEST:
                draft.loading = true;
                break;
            case CompanyActions.FINDALL_ERROR:
                draft.loading = false;
                draft.error = {error: true, ...action.payload}
                break;
            case CompanyActions.FINDALL_SUCCESSFUL:
                draft.loading = false;
                draft.error = {error: false}
                draft.companies = action.payload.items.map(company => {
                    const oldCompany = draft.companies.find(item => item.id === company.id);
                    if (!oldCompany)
                        return {selected: false, ...company}
                    return {selected: oldCompany.selected, ...company}
                })
                if (draft.companies.filter(item => item.selected).length === 0 && draft.companies.length > 0) {
                    draft.companies[0].selected = true
                }
                break;
            case CompanyActions.OPEN_SELECT:
                draft.open = true;
                break;
            case CompanyActions.CLOSE_SELECT:
                draft.open = false;
                break;
            case CompanyActions.TOGGLE_SELECT:
                draft.open = !draft.open;
                break;
        }
    })
}
