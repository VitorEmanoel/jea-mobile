import { put, call } from "redux-saga/effects"
import {CompanyService} from "../../services/companyService";
import {CompanyActions, CompanyFindAllRequestAction} from "./types";

export function* findAllRequest(action: CompanyFindAllRequestAction): Generator<any> {
    try {
        const service = CompanyService()
        const response:any = yield call(service.findAll, {...action.payload})
        if (response.status === 200) {
            yield put({type: CompanyActions.FINDALL_SUCCESSFUL, payload: response.data})
            return
        }
        yield put({type: CompanyActions.FINDALL_ERROR, payload: response.data})
    }catch (e) {
        yield put({type: CompanyActions.FINDALL_ERROR, payload: e})
    }
}
