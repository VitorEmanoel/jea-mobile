import {LoginAction, LoginActions, LoginRequestPayload} from "./types";

export interface LoginFunctionActions {
    login(params: LoginRequestPayload): LoginActions
    logout() : LoginActions
}

export function login(params: LoginRequestPayload): LoginActions {
    return {type: LoginAction.LOGIN_REQUEST, payload: params}
}

export function logout(): LoginActions {
    return {type: LoginAction.LOGOUT}
}
