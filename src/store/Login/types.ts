import { Error } from '../baseTypes'

export interface LoginState {
    logged: boolean;
    token: string;
    loading: boolean;
    error: Error;
}

export enum LoginAction {
    LOGIN_REQUEST = "LOGIN/LOGIN_REQUEST",
    LOGIN_SUCCESS = "LOGIN/LOGIN_SUCCESS",
    LOGIN_ERROR = "LOGIN/LOGIN_ERROR",
    LOGOUT = "LOGIN/LOGOUT"
}


export interface LoginErrorPayload {
    message: string
    code: number
}

export interface LoginErrorAction {
    type: typeof LoginAction.LOGIN_ERROR
    payload: LoginErrorPayload
}

export interface LoginSuccessPayload {
    token: string
}

export interface LoginSuccessAction {
    type: typeof LoginAction.LOGIN_SUCCESS
    payload: LoginSuccessPayload
}

export interface LoginRequestPayload{
    username: string;
    password: string
}

export interface LoginRequestAction {
    type: typeof LoginAction.LOGIN_REQUEST
    payload: LoginRequestPayload
}

export interface LogoutAction {
    type: typeof LoginAction.LOGOUT
    payload?: any
}

export type LoginActions  = LoginRequestAction | LoginSuccessAction | LoginErrorAction | LogoutAction
