import {call, put, takeLatest} from 'redux-saga/effects'
import {LoginAction, LoginErrorAction, LoginRequestAction, LoginSuccessAction} from "./types";

export function* loginRequest(action: LoginRequestAction) {
    try {
        //const response: Response<LoginResponse> = yield call(Services.LoginService.login, {password: action.payload.password, username: action.payload.username})
        yield put<LoginSuccessAction>({ type: LoginAction.LOGIN_SUCCESS, payload: { token: "response.data.token"}})
    }catch (e) {
        yield put<LoginErrorAction>({type: LoginAction.LOGIN_ERROR, payload: { message: e, code: 9}})
    }
}
