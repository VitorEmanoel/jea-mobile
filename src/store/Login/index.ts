import {LoginAction, LoginActions, LoginState} from "./types";
import produce from 'immer';

export * from './types'
export * from './sagas'
export * from './actions'

const INITIAL_STATE: LoginState = {
    logged: false,
    token: "",
    error: {error: false},
    loading: false
}

export const LoginReducer = (state: LoginState = INITIAL_STATE, action: LoginActions) => {
    return produce(state, draft => {
        switch (action.type) {
            case LoginAction.LOGOUT:
                draft.logged = false;
                draft.token = ""
                break
            case LoginAction.LOGIN_REQUEST:
                draft.loading = true
                break
            case LoginAction.LOGIN_ERROR:
                draft.loading = false
                draft.error = {
                    error: true,
                    code: action.payload.code,
                    message: action.payload.message
                }
                break
            case LoginAction.LOGIN_SUCCESS:
                draft.loading = false
                draft.logged = true
                draft.token = action.payload.token;
                break
        }
    })
}
