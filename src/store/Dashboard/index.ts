import {DashboardAction, DashboardActions, DashboardState} from "./types";
import produce from "immer";

const INITIAL_STATE: DashboardState = {
    selected: ""
}
export const DashboardReducer = (state: DashboardState = INITIAL_STATE, action: DashboardAction) => produce(state, draft => {
    switch (action.type) {
        case DashboardActions.SELECT:
            draft.selected = action.payload;
            break;
    }
})
