export interface DashboardState {
    selected: string;
}

export enum DashboardActions {
    SELECT = "DASHBOARD/SELECT",
}

export interface DashboardSelectAction {
    type: typeof DashboardActions.SELECT;
    payload: string
}


export type DashboardAction = DashboardSelectAction
