import {DashboardAction, DashboardActions} from "./types";

export function selectModule(moduleName: string): DashboardAction {
    return {
        type: DashboardActions.SELECT,
        payload: moduleName,
    }
}
