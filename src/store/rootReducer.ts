import { combineReducers } from 'redux'
import {DeviceRegisterReducer} from "./DeviceRegister";
import { Modules } from "../modules";
import {CompanyReducer} from "./Company";
import {StateBuilder} from "../lib/Dataflow";
import {DashboardReducer} from "./Dashboard";
// @ts-ignore
import { reducer as offline } from "redux-offline-queue";

export const Reducers = () => {
    const modulesReducer: any = {};
    Modules.forEach(module => {
        modulesReducer[module.name.toUpperCase()] = StateBuilder.Reducer(module.name, "FINDALL");
    })
    return combineReducers({
        offline,
        register: DeviceRegisterReducer,
        company: CompanyReducer,
        dashboard: DashboardReducer,
        ...modulesReducer,
    });
}
