import { all, takeLatest, fork, spawn } from 'redux-saga/effects'
import {DeviceRegisterAction, registerDeviceRequest} from "./DeviceRegister";
import {Modules} from "../modules";
import {CompanyActions} from "./Company/types";
import {findAllRequest} from "./Company/sagas";
import {StateBuilder} from "../lib/Dataflow";
import {initSaga} from "./initSaga";
import {startNetworkWatching} from "./offline";

export function* rootSaga() {
    const moduleMiddlewares: any[] = []
    Modules.forEach(module =>
        moduleMiddlewares.push(
            takeLatest(
                `${module.name.toUpperCase()}/FINDALL_REQUEST`, StateBuilder.RequestHandler(module.name, "FINDALL", module.dataFlow.getService().findAll)
            )
        )
    )
    yield spawn(startNetworkWatching)
    return yield all([
        takeLatest(DeviceRegisterAction.REGISTER_DEVICE_REQUEST, registerDeviceRequest),
        takeLatest(CompanyActions.FINDALL_REQUEST, findAllRequest),
        ...moduleMiddlewares,
    ])
}
