import {DeviceRegisterState} from "./DeviceRegister";
import {CompanyState} from "./Company/types";
import {Module, ModuleState} from "../lib/Module";
import {AccountPayable, AccountReceivable} from "../modules/AccountModule";
import {Billing} from "../modules/BillingModule";
import {Sale} from "../modules/SaleModule";
import {DashboardState} from "./Dashboard/types";

export interface RootState {
    register: DeviceRegisterState
    company: CompanyState
    sale: ModuleState<Sale>
    accountPayable: ModuleState<AccountPayable>
    accountReceivable: ModuleState<AccountReceivable>
    billing: ModuleState<Billing>
    dashboard: DashboardState
}
