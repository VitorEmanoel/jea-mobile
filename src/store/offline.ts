import NetInfo, {NetInfoState} from "@react-native-community/netinfo"
import { eventChannel } from "redux-saga"
import { take, put } from 'redux-saga/effects';
// @ts-ignore
import { ONLINE, OFFLINE } from "redux-offline-queue";
export function* startNetworkWatching() {
    const channel = eventChannel(emitter => {
        return NetInfo.addEventListener(emitter)
    })

    try{
        for(;;){
            const state: NetInfoState = yield take(channel)
            if (state.isConnected) {
                yield put({type: ONLINE})
            }else{
                yield put({type: OFFLINE})
            }
        }
    }finally {
        channel.close()
    }
}
