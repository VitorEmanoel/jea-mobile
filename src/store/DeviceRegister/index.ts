import produce from "immer";
import {DeviceRegisterAction, DeviceRegisterActions, DeviceRegisterState} from "./types";

export * from './types'
export * from './sagas'
export * from './actions'

const INITIAL_STATE: DeviceRegisterState = {
    error: {error: false},
    registered: false,
    loading: false,
    register: {
        version: "",
        url: "",
        token: "",
        plataform: "",
        model: "",
        imei: "",
        serial: ""
    }

}

export const DeviceRegisterReducer = (state: DeviceRegisterState = INITIAL_STATE, action: DeviceRegisterActions) => {
    return produce(state, draft => {
        switch (action.type) {
            case DeviceRegisterAction.RESET_DEVICE:
                draft.register = INITIAL_STATE.register;
                draft.registered = false
                draft.session = undefined
                break;
            case DeviceRegisterAction.REGISTER_DEVICE_REQUEST:
                draft.loading = true;
                draft.error = {error: false}
                draft.register = action.payload
                draft.session = undefined
                break;
            case DeviceRegisterAction.REGISTER_DEVICE_ERROR:
                draft.loading = false
                draft.error = action.error
                draft.registered = false
                break;
            case DeviceRegisterAction.REGISTER_DEVICE_SUCCESSFUL:
                draft.loading = false
                draft.error = {error: false}
                draft.registered = true
                draft.session = action.payload;
                break;
            case DeviceRegisterAction.REMOVE_ERROR:
                draft.loading = false;
                draft.error = {error: false}
                draft.session = undefined
        }
    })
}
