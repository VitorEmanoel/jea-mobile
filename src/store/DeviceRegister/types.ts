import {Error} from "../baseTypes";
import {Session} from "../../services/SessionService";

export interface DeviceRegister {
    model: string;
    plataform: string
    version: string
    imei: string
    serial: string
    url: string
    token: string
}

export interface DeviceRegisterState {
    register: DeviceRegister,
    session?: Session,
    error: Error
    loading: boolean
    registered: boolean
}

export enum DeviceRegisterAction {
    REGISTER_DEVICE_REQUEST = "DEVICE/REGISTER_REQUEST",
    REGISTER_DEVICE_ERROR = "DEVICE/REGISTER_ERROR",
    REGISTER_DEVICE_SUCCESSFUL = "DEVICE/REGISTER_SUCCESSFUL",
    RESET_DEVICE = "DEVICE/RESET",
    REMOVE_ERROR = "DEVICE/REMOVE_ERROR",
}

export interface RegisterDeviceRequestAction {
    type: typeof DeviceRegisterAction.REGISTER_DEVICE_REQUEST,
    payload: DeviceRegister
}

export interface RegisterDeviceSuccessfulAction {
    type: typeof DeviceRegisterAction.REGISTER_DEVICE_SUCCESSFUL
    payload: Session
}

export interface RegisterDeviceErrorAction {
    type: typeof DeviceRegisterAction.REGISTER_DEVICE_ERROR,
    error:  Error,
}

export interface ResetDeviceAction {
    type: typeof DeviceRegisterAction.RESET_DEVICE,
    payload?: any
}

export interface RemoveErrorAction {
    type: typeof DeviceRegisterAction.REMOVE_ERROR,
}

export type DeviceRegisterActions = ResetDeviceAction | RegisterDeviceErrorAction | RegisterDeviceRequestAction | RegisterDeviceSuccessfulAction | RemoveErrorAction
