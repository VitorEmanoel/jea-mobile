import {DeviceRegister, DeviceRegisterAction, DeviceRegisterActions} from "./types";

export interface DeviceRegisterFunctionActions {
    registerDevice(device: DeviceRegister): DeviceRegisterActions
    resetDevice(): DeviceRegisterActions
}

export function registerDevice(device: DeviceRegister): DeviceRegisterActions {
    return {type: DeviceRegisterAction.REGISTER_DEVICE_REQUEST, payload: device}
}

export function resetDevice(): DeviceRegisterActions {
    return { type: DeviceRegisterAction.RESET_DEVICE }
}

export function removeError(): DeviceRegisterActions {
    return {type: DeviceRegisterAction.REMOVE_ERROR}
}
