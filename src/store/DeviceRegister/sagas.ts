import {
    DeviceRegisterAction,
    RegisterDeviceErrorAction,
    RegisterDeviceRequestAction,
    RegisterDeviceSuccessfulAction
} from "./types";
import {call, put} from "redux-saga/effects"
import SessionService from "../../services/SessionService"

export function* registerDeviceRequest(action: RegisterDeviceRequestAction) {
    try{
        const response = yield call(SessionService.register, action.payload)
        if (response.status === 200){
            yield put<RegisterDeviceSuccessfulAction>({type: DeviceRegisterAction.REGISTER_DEVICE_SUCCESSFUL, payload: response.data})
            return
        }
        yield put<RegisterDeviceErrorAction>({type: DeviceRegisterAction.REGISTER_DEVICE_ERROR, error: {error: true, message: response.data, code: response.status}})
    }catch (e) {
        yield put<RegisterDeviceErrorAction>({type: DeviceRegisterAction.REGISTER_DEVICE_ERROR, error: {error: true, message: e.message, code: e.code}})
    }
}
