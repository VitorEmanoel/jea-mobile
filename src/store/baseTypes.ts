import {ModuleState} from "../lib/Module";

export interface Error {
    message?: string;
    code?: number
    error: boolean
}

export interface ModulesStates {
    [name: string]: ModuleState<any>
}
