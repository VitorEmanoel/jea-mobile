import React from 'react';
import { Svg , G , Path  } from 'react-native-svg';

function BackgroundLogin() {
    return (
        <Svg width="375" height="531" viewBox="0 0 375 531" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Path d="M377 5.97441C376.167 106.791 335.037 309.808 177.177 315.345C45.2615 319.973 4.85964 418.874 -1.16481 491.859V531C-2.20478 519.565 -2.34966 506.213 -1.16481 491.859V-2.54528L377 -10V5.97441Z" fill="#3366FF"/>
        </Svg>
    )
}
export default BackgroundLogin;
