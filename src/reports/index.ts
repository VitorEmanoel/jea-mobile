import BestSellersReport from "./BestSellersReport";
import PerformanceReport from "./PerformanceReport";
import {BaseReport, BaseRequest, MapReports, Report} from "../lib/Reports/Report";
import { AxiosResponse } from "axios";

export interface ReportManager {
    get<T, K extends keyof Reports, E extends BaseRequest>(report: K, request: E): Promise<AxiosResponse<T>>
}

const reports: MapReports = {
    "bestSellers": BestSellersReport,
    "performance": PerformanceReport
}

type Reports = typeof reports;



const Manager: ReportManager = {
    get: <T, K extends keyof Reports, E extends BaseRequest>(report: K, request: E):Promise<AxiosResponse<T>> => {
        return reports[report](request)
    }
}

export default Manager;


