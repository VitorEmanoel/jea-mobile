import {Employer} from "../modules/EmployerModule";
import {BaseReport, BaseRequest, Report} from "../lib/Reports/Report";

export interface BestSellersReport {
    seller: Employer
    sellerId: number;
    salesTotal: number;
    salesCount: number;
}

const report: Report<BestSellersReport, BaseRequest> = BaseReport("bestsellers");

export default report;
