import {useEffect, useState} from "react";
import ReportManager from "./index";
import {useSelector} from "react-redux";
import {RootState} from "../store/types";
import {BaseRequest} from "../lib/Reports/Report";

export const useReport = <T,>(name: string) => {

    const [data, setData] = useState<T[]>()
    const register = useSelector((state: RootState) => state.register.register)

    const getData = async (): Promise<void> => {
        const response = await ReportManager.get<T, string, BaseRequest>(name, {url: register.url, token: register.token})
        setData(response.data)
    }

    useEffect(() => {
        getData()
    }, [])
    return data
}
