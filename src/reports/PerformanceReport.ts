import {BaseReport, BaseRequest, Report} from "../lib/Reports/Report";
import {Company} from "../modules/Company";

export interface PerformanceReport {
    accountsPayableTotal: number;
    accountsReceivableTotal: number;
    salesTotal: number;
    salesDiscountTotal: number;
    purchasesTotal: number;
    company: Company;
}

const report: Report<PerformanceReport, BaseRequest> = BaseReport("performance")
export default report;
