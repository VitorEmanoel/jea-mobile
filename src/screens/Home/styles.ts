import styled from "styled-components/native";

export const StoryContainer = styled.View`
  width: 100%;
  elevation: 1;
  background-color: #FFF;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  z-index: 10;
`;
