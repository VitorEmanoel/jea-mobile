import React from 'react';
import HomeTabs from "./tabs"
import {View} from "react-native";
import SafeUpdateScroll from "../../components/SafeUpdateScroll";
import Story from "../../components/Story";
import {StoryContainer} from "./styles";

const Home = (props: any) => {

    return (
        <HomeTabs navigation={props.navigation}/>
    )
}

export default Home;
