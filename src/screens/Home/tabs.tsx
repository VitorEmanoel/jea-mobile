import React from 'react'

import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import Dashboard, {DashboardTabOptions} from "../Dashboard";
import Modules, {ModulesTabOptions} from "../Modules";
import Company, {CompanyTabOptions} from "../Company";
import {View} from "react-native";
import CompanySelect from "../../components/CompanySelect";
import Config, {ConfigTabOptions} from "../Config";

const Tab = createBottomTabNavigator()

const HomeTabs = (props: any) => {

    return (
        <Tab.Navigator tabBarOptions={{showLabel: false, adaptive: true, style: {paddingBottom: 0,  height: '10%', backgroundColor: "#3366FF"}}} backBehavior={"none"} initialRouteName={"Dashboard"}>
            <Tab.Screen name={"Modules"} component={Modules} options={ModulesTabOptions}/>
            <Tab.Screen name={"Dashboard"} component={Dashboard} options={DashboardTabOptions}/>
            <Tab.Screen name={"Config"} component={Config}  options={ConfigTabOptions}/>
        </Tab.Navigator>
    )
}

export default HomeTabs
