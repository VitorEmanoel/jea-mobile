import styled from 'styled-components/native';

interface LoginWrapperProps {
    background: string
}

export const LoginWrapper  = styled.View`
  flex: 1;
  align-items: center;
  position: relative;
  background-color: #FFF;
  z-index: 1;
`;

export const LoginInput = styled.TextInput`
  margin-top: 20px;
  width: 80%;
  border-radius: 60px;
  padding: 15px 20px;
  background: #fcfcfc;
  box-shadow: 1px 4px 4px rgba(0, 0, 0, 0.25);
  elevation: 1;
`

export const LoginButton = styled.TouchableNativeFeedback`
  justify-content: center;
  align-items: center;
  background: #3366FF;
  box-shadow: 3px 5px 4px rgba(0, 0, 0, 0.25);
  border-radius: 60px;
  width: auto;
  padding: 10px 30px;
  margin: 70px 60px;
  elevation: 1;
`

export const LoginButtonText = styled.Text`
  font-size: 26px;
  font-weight: bold;
  color: white;
`

export const LoginButtonContentContainer = styled.View`
    padding: 10px 30px;
`

export const LoginContainer = styled.View`
  width: 90%;
  padding: 10px;
`;

export const InputContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const WarningContainer = styled.View`
  align-items: center;
  margin-top: 10px;
`;

export const BackgroundWrapper = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  flex: 1;
  z-index: -1;
`
