import React, { useState, useEffect } from 'react'
import {View, Text, KeyboardAvoidingView, ActivityIndicator} from 'react-native';
import {Icon} from "react-native-elements";
import {useDispatch, useSelector} from "react-redux";
import { StackActions } from '@react-navigation/native'
import { Logo, BackgroundLogin } from '../../assets/svg'
import {
    LoginWrapper,
    LoginContainer,
    InputContainer,
    WarningContainer,
    LoginButton,
    LoginButtonText,
    LoginInput, BackgroundWrapper
} from "./styles";
import {RootState} from "../../store/types";

export interface Warning {
    error: boolean
    icon?: any
    message?: string
}

const LoginScreen = (props: any) => {

    const navigation = props.navigation;

    const dispatch = useDispatch()

    const login = useSelector((state: RootState) => state.login)

    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [loading, setLoading] = useState(false)
    const [warning, setWarning] = useState<Warning>({error: false})


    const handlerSubmitLogin = () => {
        setLoading(true)
        dispatch({type: "LOGIN/LOGIN_REQUEST", payload: {username: username, password: password}})
    }

    useEffect(() => {
        if (login.logged) {
            navigation.dispatch(StackActions.replace('Home'))
            return
        }
    }, [])

    useEffect(() => {
        if (login.logged) {
            handlerLoginSuccessful()
            return
        }
        if (login.error.error) {
            setWarning({message: login.error.message, icon:"error", error: true})
        }
        setLoading(login.logged)
    }, [login])

    const handlerLoginSuccessful = () => {
        setLoading(false)
        setUsername("")
        setPassword("")
        navigation.dispatch(StackActions.replace('Home'))
    }

    const handlerUserInputSubmit = () => {
        passwordInput.focus();
    }

    let passwordInput: any;

    return (
        <>
            <LoginWrapper>
                <BackgroundWrapper>
                    <BackgroundLogin/>
                </BackgroundWrapper>
                <LoginContainer>
                    <View style={{alignItems: 'center', paddingTop: '35%', marginBottom: '3%'}}>
                        <Logo/>
                    </View>
                    <WarningContainer>
                        <View style={{flexDirection: "row"}}>
                            <Icon name={warning.icon} type={"material-design"} color={"rgb(172,14,14)"} size={24}
                                  containerStyle={{paddingRight: 10}}/>
                            <Text style={{color: "rgb(172,14,14)"}}>{warning.error}</Text>
                        </View>
                    </WarningContainer>
                    <View style={{paddingTop: !warning.error ? '5%' : '15%'}}>
                        <View>
                            <KeyboardAvoidingView behavior={"padding"}>
                                <InputContainer>
                                    <LoginInput placeholder={"Digite seu usuário"}
                                           value={username} disableFullscreenUI={false} onChangeText={(text) => setUsername(text)}
                                           placeholderTextColor="#5B5B5B" returnKeyType={"next"}
                                           onSubmitEditing={handlerUserInputSubmit}
                                           blurOnSubmit={false} />
                                </InputContainer>
                                <InputContainer>
                                    <LoginInput ref={(password) => passwordInput = password} placeholder={"Digite sua senha"} value={password} onChangeText={(text) => setPassword(text)}
                                           onSubmitEditing={handlerSubmitLogin}
                                           blurOnSubmit={true}
                                           placeholderTextColor="#5B5B5B" secureTextEntry={true}/>
                                </InputContainer>
                            </KeyboardAvoidingView>
                        </View>
                        <View>
                            <LoginButton onPress={handlerSubmitLogin}>
                                {loading ? <ActivityIndicator style={{padding: 10}} size={"small"} color={"white"}/> : (<LoginButtonText>Logar</LoginButtonText>)}
                            </LoginButton>
                        </View>
                    </View>
                </LoginContainer>
            </LoginWrapper>
        </>
    )
}

export default LoginScreen;
