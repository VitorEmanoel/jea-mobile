import React, {useEffect, useState} from 'react';
import {Alert, ToastAndroid, TouchableNativeFeedback, Text, Modal, ActivityIndicator} from "react-native";
import {
    ConfigContainer,
    ConfigLogo,
    ConfigTitle,
    ConfigUpdateContainer,
    ConfigUpdateText,
    ConfigWrapper,
    QuitButton,
    QuitButtonWrapper,
    QuitItem,
    QuitKey,
    QuitTitle,
    QuitValue, UpdateModalContainer
} from "./style";
import {Logo} from "../../assets/svg";
import {StackNavigationProp} from "@react-navigation/stack"
import Icon from "react-native-vector-icons/MaterialIcons"
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../store/types";
import * as DeviceRegisterActions from "../../store/DeviceRegister/actions"
import {bindActionCreators} from "redux";
import moment from "moment";
import TabIcon from "../../components/TabIcon";
import CodePush, {RemotePackage} from "react-native-code-push";

interface ConfigProps {
    navigation: StackNavigationProp<any>
}

const Config = (props: ConfigProps) => {

    const register = useSelector((state: RootState) => state.register)

    const dispatch = useDispatch()

    const deviceRegisterAction = bindActionCreators(DeviceRegisterActions, dispatch)

    const [currentVersion, setCurrentVersion] = useState("")
    const [updateProgress, setUpdateProgress] = useState(0)
    const [updateModal, setUpdateModal] = useState(false)

    useEffect(() => {
        CodePush.getUpdateMetadata(CodePush.UpdateState.RUNNING).then(pack => pack && pack.appVersion && setCurrentVersion(pack.appVersion))
    }, [])

    const handlerApplicationQuit = () => {
        Alert.alert(
            "Você realmente deseja sair da sessão atual?",
            "Ao concordar você irá sair da sessão atual e irá perder acesso ao dashboard. Terá que escanear novamente o QRCode para acessar novamente.",
            [
                {
                    text: "Sim",
                    onPress: () => {
                        deviceRegisterAction.resetDevice()
                        props.navigation.push("SplashScreen")
                    }
                },
                {
                    text: "Não",
                    onPress: () => {},
                },
            ],
            {cancelable: false}
        );
    }

    const update = (pack: RemotePackage) => {
        setUpdateModal(true)
        pack.download(progress => {
            const updatePercentage = (progress.receivedBytes/progress.receivedBytes) * 100;
            setUpdateProgress(updatePercentage)
        }).then(local => {
            local.install(CodePush.InstallMode.IMMEDIATE).then(() => {
                setUpdateModal(false)
            })
        })
    }

    const handleCheckUpdate = () => {
        CodePush.checkForUpdate().then(updatePack => {
            if (!updatePack){
                ToastAndroid.showWithGravity("Não foi encontrado nenhuma atualização", 3000, ToastAndroid.BOTTOM)
                return
            }
            Alert.alert("Nova versão disponivel!", "Nova versão está disponivel, deseja atualizar?", [
                {
                    text: "Sim",
                    onPress: () => update(updatePack)
                },
                {
                    text: "Não"
                }
            ])
        })
    }

    return (
        <ConfigWrapper>
            <Modal visible={updateModal} transparent>
                <UpdateModalContainer>
                    {updateProgress < 100 && (
                        <ActivityIndicator size={"large"} color={"#3366BB"}/>
                    )}
                </UpdateModalContainer>
            </Modal>
            <TouchableNativeFeedback onPress={handleCheckUpdate}>
                <ConfigUpdateContainer>
                    <ConfigUpdateText>Verificar atualização</ConfigUpdateText>
                    <Text><Icon color={"#FFF"} size={18} name={"system-update"}/></Text>
                </ConfigUpdateContainer>
            </TouchableNativeFeedback>
            <ConfigLogo>
                <Logo/>
            </ConfigLogo>
            <ConfigTitle>Configurações</ConfigTitle>
            <ConfigContainer>
                {register.session && (
                    <QuitItem>
                        <QuitKey>
                            Usuário:
                        </QuitKey>
                        <QuitValue>{register.session.user.name}</QuitValue>
                    </QuitItem>
                )}
                <QuitItem>
                    <QuitKey>Plataforma: </QuitKey>
                    <QuitValue>{register.register.plataform}</QuitValue>
                </QuitItem>
                <QuitItem>
                    <QuitKey>Modelo: </QuitKey>
                    <QuitValue>{register.register.model}</QuitValue>
                </QuitItem>
                <QuitItem>
                    <QuitKey>URL: </QuitKey>
                    <QuitValue>{register.register.url}</QuitValue>
                </QuitItem>
                <QuitItem>
                    <QuitKey>Versão: </QuitKey>
                    <QuitValue>{register.register.version}</QuitValue>
                </QuitItem>
                <QuitItem>
                    <QuitKey>Versão do Software: </QuitKey>
                    <QuitValue>{currentVersion}</QuitValue>
                </QuitItem>
                {register.session && register.session.access.length >= 2 && (
                    <QuitItem>
                        <QuitKey>Ultimo acesso:</QuitKey>
                        <QuitValue>{moment(register.session.access[register.session.access.length - 2].accessAt).format("L")}</QuitValue>
                    </QuitItem>
                )}
            </ConfigContainer>
            <QuitButton onPress={handlerApplicationQuit}>
                <QuitButtonWrapper>
                <QuitTitle>Sair</QuitTitle>
                <Icon name={"exit-to-app"} color={"white"} size={22}/>
                </QuitButtonWrapper>
            </QuitButton>
        </ConfigWrapper>
    )
};

export default Config;

export const ConfigTabOptions = {
    tabBarIcon: (props: any) => <TabIcon focused={props.focused} icon={"settings"}/>
}
