import styled from 'styled-components/native';

export const ConfigWrapper = styled.View`
  position: relative;
  flex: 1;
  width: 100%;
  flex-direction: column;
  align-items: center;
  background-color: #3366FF;
`;

export const ConfigLogo = styled.View`
  padding: 50px 0 15px;
`;

export const ConfigTitle = styled.Text`
  color: white;
  font-weight: bold;
  font-size: 20px;
  padding-bottom: 10px;
`;

export const ConfigContainer = styled.ScrollView`
  padding: 10px;
  margin: 10px 10px 0 10px;
  background-color: white;
  width: 80%;
  height: 65%;
  border-radius: 10px;
  elevation: 1;
`;

export const QuitButton = styled.TouchableNativeFeedback`
`;

export const QuitButtonWrapper = styled.View`
margin-top: 2.5px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 10px;
  border-radius: 10px;
`;

export const QuitTitle = styled.Text`
  color: white;
  font-size: 18px;
  margin-right: 10px;
`;

export const QuitItem = styled.View`
  flex-direction: row;
  padding: 10px;
  align-items: center;
  border-bottom-width: 1px;
  border-bottom-color: rgba(0,0,0,0.3);
  margin-top: 10px;
`;

export const QuitKey = styled.Text`
  color: #6B6B6B;
  font-size: 18px;
  font-weight: bold;
`;

export const QuitValue = styled.Text`
  margin-left: 5px;
  font-size: 16px;
  color: rgba(107,107,107,0.66);
`;

export const ConfigUpdateContainer = styled.View`
  position: absolute;
  top: 0;
  right: 0;
  padding: 10px;
  flex-direction: row;
`;

export const ConfigUpdateText = styled.Text`
  color: #FFF;
  font-size: 14px;
  padding-right: 5px;
`;

export const UpdateModalContainer = styled.View`
  flex: 1;
  margin: 20px;
  padding: 10px;
  width: 50%;
  height: 50%;
  background-color: #FFF;
  justify-content: center;
  align-items: center;
  elevation: 1;
`;

export const UpdateModalProgress = styled.View`
   margin-top: 20px;
   font-size: 14px;
   color: #6B6B6B;
`;
