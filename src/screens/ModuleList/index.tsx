import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {
    ModuleListContainer,
    ModuleListHeader, ModuleListItemListContainer, ModuleListLoadingContainer,
    ModuleListSortContainer,
    ModuleListTitle,
    ModuleListWrapper
} from "./style";
import {ActivityIndicator, TouchableNativeFeedback, Text, View, ToastAndroid} from "react-native";
import { bindActionCreators } from "redux";
import BackButton from "../../components/BackButton";
import SearchBar from "../../components/SearchBar";
import {FlatList} from "react-native";
import {Module} from "../../lib/Module";
import OrderButton from "../../components/OrderButton";
import OrderButtonComplement, {OrderFilterItem} from "../../components/OrderButton/complement";
import SearchBarComplement, {FilterItem} from "../../components/SearchBar/complement";
import {RootState} from "../../store/types";
import * as ModuleActions from "../../lib/Module/actions"
import {ListAllRequest} from "../../lib/Dataflow/request";
import { ModulesMapped} from "../../modules";
import {ModulesStates} from "../../store/baseTypes";
import {useCompany} from "../../store/Company/hooks";
import {StackNavigationProp} from "@react-navigation/stack";
import Icon from "react-native-vector-icons/MaterialIcons";

interface ModuleListProps {
    navigation: StackNavigationProp<any>;
    route: any;
}

const ModuleList = (props: ModuleListProps) => {

    const pageLimit = 20;

    const module: Module<any> = ModulesMapped()[props.route.params.module];

    const [orderMenuVisible, setOrderMenuVisible] = useState(false)
    const [loading, setLoading] = useState(true)
    const [searchVisible, setSearchVisible] = useState(false)
    const [loadingMore, setLoadingMore] = useState(false)
    const [inOperation, setInOperation] = useState(false)
    const [lastPage, setLastPage] = useState(false)
    const [page, setPage] = useState(1)
    const [data, setData] = useState<any[]>([])

    const dispatch = useDispatch()
    const moduleActions = bindActionCreators(ModuleActions, dispatch)

    const deviceRegister = useSelector((state: RootState) => state.register)

    const moduleState = useCompany(useSelector((state: ModulesStates) => state[module.name]), module.multiCompany)

    const loadData = async () => {
        setLoading(true)
        setInOperation(true)
        const request: ListAllRequest = {url: deviceRegister.register.url, limit: pageLimit, page, data: { token: deviceRegister.register.token }}
        moduleActions.FindAll(module.name, request);
        setPage(page => page + 1)
    }

    useEffect(() => {
        if (!moduleState.loading && !moduleState.success && moduleState.error.error && inOperation) {
            ToastAndroid.showWithGravity(`Não foi possivel realizar buscar os dados. Error: ${moduleState.error.message}`, 3000, ToastAndroid.BOTTOM)
            setInOperation(false)
            setLoadingMore(false)
            setLoading(false)
        }
        if (moduleState.success && inOperation) {
            setLastPage(moduleState.rows.length < pageLimit)
            setData([...data, ...moduleState.rows])
            setLoadingMore(false)
            setLoading(false)
            setInOperation(false)
        }
    }, [moduleState])

    useEffect(() => {
        setData([])
        setPage(1)
        loadData()
    }, [props.route.params.module])

    const handlerOrderItemClick = (item: OrderFilterItem) => {
        setOrderMenuVisible(false)
    }

    const handlerFilterItemClick = (item: FilterItem) => {
        setSearchVisible(false)
    }

    const handlerItemClick = (item: any) => {
        props.navigation.push("ModuleForm", {module: module.name, item: item})
    }

    const handlerEndReached = () => {
        if (!lastPage && data.length > pageLimit) {
            setLoadingMore(true)
            loadData()
        }
    }
    return (
        <ModuleListWrapper>
            <BackButton backRoute={"ModulesList"} navigation={props.navigation}/>
            <ModuleListContainer>
                <SearchBar complementOpen={searchVisible} complementOpened={setSearchVisible}/>
                <SearchBarComplement filters={module.moduleList.filterList} visible={searchVisible} onItemFilterClick={handlerFilterItemClick}/>
                <ModuleListHeader>
                    <Text style={{marginRight: 5}}>{module.titleIcon && (<Icon name={module.titleIcon} size={22} color={"#3366FF"}/>)}</Text>
                    <ModuleListTitle>
                        {module.displayName}
                    </ModuleListTitle>
                    <ModuleListSortContainer>
                        <OrderButton onOrderClick={() => setOrderMenuVisible(!orderMenuVisible)} />
                    </ModuleListSortContainer>
                    <OrderButtonComplement visible={orderMenuVisible} orderFilterItems={module.moduleList.orderFilterList} onItemClick={handlerOrderItemClick}/>
                </ModuleListHeader>
                <ModuleListItemListContainer>
                    <FlatList contentContainerStyle={{ flexGrow: 1 }}
                              ListEmptyComponent={(moduleState.loading || loading) ? (<ModuleListLoadingContainer><ActivityIndicator color={"#3366FF"} size={"large"}/></ModuleListLoadingContainer>) : module.moduleList.emptyContent}
                              ListFooterComponent={() => (
                                  <>
                                  {loadingMore && (
                                      <View style={{justifyContent:"center", alignItems: "center", marginVertical: 20}}>
                                          <ActivityIndicator color={"#3366FF"} size={"small"}/>
                                      </View>)}
                                  </>
                              )}
                              data={data}
                              onEndReached={handlerEndReached}
                              onEndReachedThreshold={0.2}
                              renderItem={item => (
                                    <>
                                        <TouchableNativeFeedback onPress={() => module.form && handlerItemClick(item.item)}>
                                            {module.moduleList.renderItem(item)}
                                        </TouchableNativeFeedback>
                                    </>
                              )} keyExtractor={(item, key) => `${item.id}-${module.name}`}/>
                </ModuleListItemListContainer>
            </ModuleListContainer>
        </ModuleListWrapper>
    )
}

export default ModuleList;
