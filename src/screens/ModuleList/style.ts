import styled from 'styled-components/native';

export const ModuleListWrapper = styled.View`
  position: relative;
  background-color: #3366FF;
  flex: 1;
  width: 100%;
`;

export const ModuleListContainer = styled.View`
  flex-direction: column;
  flex: 1;
  margin: 50px 20px 30px 20px;
  border-radius: 20px;
  background-color: white;
  padding: 10px;
`;

export const ModuleListHeader = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  margin-top: 10px; 
  padding: 10px;
`;

export const ModuleListTitle = styled.Text`
    color: #6B6B6B;
    font-size: 18px;
`;

export const ModuleListSortContainer = styled.View`
  margin-left: auto;
  flex-direction: row;
`;

export const ModuleListSortText = styled.Text`
  color: #6B6B6B;
  font-size: 18px;
`;
export const ModuleListSortIcon = styled.Text`
  margin-left: 5px;
`;
export const ModuleListItemListContainer = styled.View`
  margin: 10px;
  flex: 1;
  border-radius: 20px;
  background-color: #F3F7FF;
`;

export const ModuleListItemContainer = styled.View`
  padding: 10px;
  flex-direction: column;
`;

export const ModuleListItemHeader = styled.View`
  flex-direction: row;
`;

export const ModuleListItemFooter = styled.View`
  flex-direction: row;
`;

export const ModuleListLoadingContainer = styled.View`
  flex: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
`

