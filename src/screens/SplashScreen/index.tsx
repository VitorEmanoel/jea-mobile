import React, {useEffect, useState} from 'react';
import { BackHandler } from "react-native";
import {Logo} from "../../assets/svg";
import {SplashScreenWrapper} from "./styles";
import {useDispatch, useSelector} from "react-redux";
import {StackActions} from "@react-navigation/native";
import {RootState} from "../../store/types";
import * as CompanyActions from "../../store/Company/actions";
import * as DeviceRegisterActions from "../../store/DeviceRegister/actions"
import { bindActionCreators} from "redux";
import {Alert, Animated, Dimensions} from "react-native";


const SplashScreen = (props: any) => {

    const register = useSelector((state: RootState) => state.register)

    const [inOperation, setInOperation] = useState(false)

    const logoAnimated = new Animated.Value(0)

    const dispatch = useDispatch();
    const companyActions = bindActionCreators(CompanyActions, dispatch)
    const registerActions = bindActionCreators(DeviceRegisterActions, dispatch)

    const registerDevice = () => {
        registerActions.registerDevice(register.register)
        setInOperation(true)
    }

    const executeChangeScreen = (callback: () => void) => {
        Animated.timing(logoAnimated, {
            duration: 300,
            toValue: Dimensions.get("window").width,
            useNativeDriver: true,
        }).start(callback)
    }

    useEffect(() => {
        if (!register.registered) {
            executeChangeScreen(() => {
                props.navigation.dispatch(StackActions.replace('Register'))
            })
            return
        }
        registerDevice()
    }, [])

    useEffect(() => {
        if (register.registered && !register.loading && !register.error.error && inOperation) {
            executeChangeScreen(() => {
                props.navigation.dispatch(StackActions.replace('Home'))
            })
            companyActions.findAll({url: register.register.url, data: { token: register.register.token}})
        }
        if (!register.registered && !register.loading && register.error.error && inOperation) {
            Alert.alert(
                "Problema de conexão.",
                "Não foi possivel se conectar com o servidor. ",
                [
                    {
                        text: "Tentar novamente",
                        onPress: () => {
                            registerDevice()
                        }
                    },
                    {
                        text: "Sair",
                        onPress: () => {
                            registerActions.removeError()
                            BackHandler.exitApp()
                        },
                    },
                ],
                {cancelable: false}
            );
        }
    }, [register])

    return (
        <SplashScreenWrapper>
            <Animated.View style={{transform: [{translateX: logoAnimated}]}}>
                <Logo/>
            </Animated.View>
        </SplashScreenWrapper>
    )
}

export default SplashScreen;
