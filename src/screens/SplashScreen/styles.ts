import styled from 'styled-components/native';

export const SplashScreenWrapper = styled.View`
  position: relative;
  flex: 1;
  width: 100%;
  align-items: center;
  justify-content: center;
  background-color: #3366FF;
`;

export const LogoContainer = styled.View`
  position: absolute;
`;
