import React, { useEffect, useState} from 'react';
import {DashboardWarapper, DashboardContainer, DashboardGraphicsContainer, NotDataText, ExpandIcon} from "./styles";
import Icon from 'react-native-vector-icons/MaterialIcons'
import Story from '../../components/Story'
import {Modules, ModulesMapped} from "../../modules"
import {StoryContainer} from "./styles";
import {Module, ModuleState} from "../../lib/Module";
import { bindActionCreators } from "redux";
import {useDispatch, useSelector} from "react-redux";
import * as DashboardActions from "../../store/Dashboard/actions"
import {RootState} from "../../store/types";
import {ActivityIndicator, RefreshControl, Text, TouchableNativeFeedback} from "react-native";
import * as CompanyActions from "../../store/Company/actions";
import * as ModuleActions from "../../lib/Module/actions";
import {ModulesStates} from "../../store/baseTypes";
import {BottomTabNavigationProp} from "@react-navigation/bottom-tabs";
import CompanySelect from "../../components/CompanySelect";
import CompanyModal from "../../components/CompanySelect/modal";
import TabIcon from "../../components/TabIcon";

interface DashboardProps {
    navigation: BottomTabNavigationProp<any>
    route: any;
}

const Dashboard = (props: DashboardProps) => {

    const dashboard = useSelector((state: RootState) => state.dashboard);
    const register = useSelector((state: RootState) => state.register.register)
    const module = ModulesMapped()[dashboard.selected]
    const moduleState = useSelector((state: ModulesStates) => state[dashboard.selected])

    const [refreshLoading, setRefreshLoading] = useState(false)

    const header = module && module.statistics && module.statistics.header

    const dispatch = useDispatch()
    const dashboardActions = bindActionCreators(DashboardActions, dispatch)
    const moduleActions = bindActionCreators(ModuleActions, dispatch)
    const companyActions = bindActionCreators(CompanyActions, dispatch)

    const handlerStoryItemClick = (item: Module<any>) => {
        dashboardActions.selectModule(item.name)
        moduleActions.FindAll(dashboard.selected, {url: register.url, data: { token: register.token }})
    }

    function loadingContainer() {
        if (!module)
            return
        moduleActions.FindAll(dashboard.selected, {url: register.url, data: { token: register.token }})
    }

    useEffect(() => {
        loadingContainer()
        companyActions.findAll({url: register.url, data: { token: register.token }})
    }, [])

    useEffect(() => {
        if (moduleState && !moduleState.loading && refreshLoading){
            setRefreshLoading(false)
        }
    }, [moduleState])

    useEffect(() => {
        return props.navigation.addListener('tabPress', (e) => {
            loadingContainer()
        })
    }, [props.navigation])

    useEffect(() => {
       loadingContainer()
    }, [dashboard])

    const handlerRefresh = () => {
        moduleActions.FindAll(dashboard.selected, {url: register.url, data: { token: register.token }})
        setRefreshLoading(true)
    }

    return (
        <DashboardWarapper>
            <CompanyModal/>
            <DashboardContainer>
                <CompanySelect/>
                <StoryContainer><Story storyItems={Modules} onItemClick={handlerStoryItemClick}/></StoryContainer>
                <DashboardGraphicsContainer showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={refreshLoading} onRefresh={handlerRefresh}/>}>
                    {header && header()}
                </DashboardGraphicsContainer>
            </DashboardContainer>
        </DashboardWarapper>
    )
}


export const DashboardTabOptions = {
    tabBarIcon: (props: any) =>  <TabIcon focused={props.focused} icon={"home"}/>
}

export default Dashboard
