import styled from 'styled-components/native'

export const DashboardWarapper = styled.View`
  position: relative; 
  justify-content: center;
  align-items: center;
  background-color: #3366FF;
  width: 100%;
  z-index: -1;
  overflow: hidden;
`;

export const DashboardContainer = styled.View`
  width: 100%;
  height: 100%;
  background-color: #3366FF;
`;

export const StoryContainer = styled.View`
  width: 100%;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  background-color: #3366FF;
  z-index: 10;
`;

export const ExpandIcon = styled.Text`
  position: absolute;
  top: 0;
  align-self: center;
  z-index: 10;
`

export const DashboardGraphicsContainer = styled.ScrollView`
  width: 100%;
  flex: 1;
  background-color: white;
`;

export const NotDataText = styled.Text`
  color: #6B6B6B;
  font-size: 24px;
  font-weight: 300;
  text-align: center;
`;
