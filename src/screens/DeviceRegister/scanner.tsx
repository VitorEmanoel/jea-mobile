import React, { useState } from 'react';
import QRCodeScanner, {Event} from "react-native-qrcode-scanner";
import {ScannerTitle} from "./styles";

interface ScannerProps {
    onSuccessScanner: (data: any) => void;
}

const Scanner = (props: ScannerProps) => {

    const { onSuccessScanner } = props;

    let scanner: any;

    const handlerOnSuccessScanner = (e: Event) => {
        onSuccessScanner(e.data)
    }

    return (
        <QRCodeScanner ref={elem => scanner = elem} reactivateTimeout={6000} reactivate={true} topContent={<ScannerTitle>Escanei o QRCode de acesso para acessar o app</ScannerTitle>} onRead={handlerOnSuccessScanner}
            cameraStyle={{width: "80%"}} containerStyle={{alignItems: 'center', borderRadius: 10, justifyContent: "center"}}
                       showMarker={true} checkAndroid6Permissions={true}/>
    )
};

export default Scanner
