import styled from 'styled-components/native';
import {Dimensions} from "react-native";

export const ConfigWrapper = styled.View`
  position: relative;
  flex: 1;
  flex-direction: column;
  padding: 10px;
  background-color: #3366FF;
  align-items: center;
`;

export const LogoWrapper = styled.View`
  margin-top: 30px;
`;

export const ModalBackground = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  height: ${Dimensions.get("screen").height}px;
  width: ${Dimensions.get("screen").width}px;
  background-color: rgba(0,0,0,0.8);
  z-index: 998;
`;

export const ModalContainer = styled.View`
  position: absolute;
  top: 35%;
  left: ${Math.floor(Dimensions.get("screen").width/2 - (Dimensions.get("screen").width * 0.8)/2)}px;
  justify-content: center;
  align-items: center;
  z-index: 999;
  background-color: white;
  border-radius: 10px;
  elevation: 1;
  padding: 20px 0;
  width: 80%;
  box-shadow: 1px 1px 5px rgba(0,0,0,0.5);
`;

export const ScannerTitle = styled.Text`
  font-weight: bold;
  font-size: 16px;
  color: white;
  text-align: center;
  margin-top: 20px;
`;

export const LoadingTitle = styled.Text`
  font-size: 16px;
  color: #6B6B6B;
  font-weight: bold;
  text-align: center;
  margin-top: 10px
`;
