import React, { useEffect, useState } from 'react';
import {ToastAndroid, Platform, ActivityIndicator} from "react-native";
import {
    ConfigWrapper, LoadingTitle, LogoWrapper, ModalBackground, ModalContainer
} from "./styles";
import { Logo } from "../../assets/svg";
import {StackActions} from "@react-navigation/native";
import {RootState} from "../../store/types";
import {useDispatch, useSelector} from "react-redux";
import DeviceInfo from "react-native-device-info";
import Scanner from "./scanner";
import {DeviceRegister, registerDevice} from "../../store/DeviceRegister";

const Register = (props: any) => {

    const deviceRegister = useSelector((state: RootState) => state.register)

    const [model, setModel] = useState("")
    const [plataform, setPlataform] = useState("")
    const [version, setVersion] = useState("")
    const [serial, setSerial] = useState("")
    const [imei, setImei] = useState("")

    const [loading, setLoading] = useState(false);

    const dispatch = useDispatch()

    const loadDeviceData = async () => {
        setImei(DeviceInfo.getDeviceId())
        const deviceSerial = await DeviceInfo.getSerialNumber()
        setSerial(deviceSerial)
        setPlataform(Platform.OS)
        setVersion(DeviceInfo.getSystemVersion())
        setModel(DeviceInfo.getModel())
    }

    useEffect(() => {
        if (deviceRegister.registered) {
            props.navigation.dispatch(StackActions.replace('Home'))
            return
        }
        loadDeviceData()
    }, [])

    useEffect(() => {
        if (deviceRegister.registered) {
            handlerSuccessfulRegister()
            return
        }
        if (deviceRegister.error.error) {
            const message: string = "Não foi possivel registrar o dispositivo";
            ToastAndroid.showWithGravity(
                message,
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
            );
            setLoading(false)
            return
        }
    }, [deviceRegister])

    const handlerSuccessfulRegister = () => {
        ToastAndroid.showWithGravity(
            "Dispositivo cadastrado com sucesso!",
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
        );
        props.navigation.dispatch(StackActions.replace('Home'))
        setLoading(false)
    }

    const handlerScannerSuccess = (data: any) => {
        setLoading(true)
        const object = JSON.parse(data)
        const deviceInfo: DeviceRegister = {serial, imei, model, plataform, version, token: object.token, url: object.url}
        dispatch(registerDevice(deviceInfo))
    }

    return (
        <ConfigWrapper>
            {loading && (
                <>
                <ModalBackground/>
                    <ModalContainer>
                        <ActivityIndicator style={{marginTop: 20, marginBottom: 20}} color={"#3366FF"} size={"large"}/>
                        <LoadingTitle>Registrando dispositivo...</LoadingTitle>
                    </ModalContainer>
                </>
            )}
            <LogoWrapper>
                <Logo/>
            </LogoWrapper>
            <Scanner onSuccessScanner={handlerScannerSuccess}/>
        </ConfigWrapper>
    )
}

export default Register;
