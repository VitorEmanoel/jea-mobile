import React, {useEffect} from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./Home";
import LoginScreen from './Login';
import SplashScreen from "./SplashScreen";
import DeviceRegister from "./DeviceRegister";
import Config from "./Config";

const Stack = createStackNavigator()

const Screens = () => {

    return (
        <NavigationContainer >
            <Stack.Navigator initialRouteName={"SplashScreen"} screenOptions={{headerShown: false, gestureDirection: "horizontal"}}>
                <Stack.Screen name="SplashScreen" component={SplashScreen} />
                <Stack.Screen name="Register" component={DeviceRegister} />
                <Stack.Screen name="Config" component={Config}/>
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="Login" component={LoginScreen}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
};

export default Screens;
