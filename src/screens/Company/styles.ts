import styled from 'styled-components/native';

export const CompanyWrapper = styled.View`
  flex: 1;
  background-color: #3366FF;
  width: 100%;
  align-items: center;
  padding: 10px;
`;

export const LogoWrapper = styled.View`
  margin-top: 40px;
`;

export const CompanyTitle = styled.Text`
  text-align: center;
  font-size: 18px;
  color: white;
  font-weight: bold;
  margin-top: 20px;
`;


export const CompanyConfig = styled.TouchableOpacity`
  position: absolute;
  top: 0;
  right: 0;
  margin: 20px;
`;
