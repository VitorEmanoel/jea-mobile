import React, { useState, useEffect } from 'react';
import {Text, ToastAndroid} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import {
    CompanyConfig,
    CompanyTitle,
    CompanyWrapper,
    LogoWrapper
} from "./styles";
import {Logo} from "../../assets/svg";
import {StackActions} from "@react-navigation/native";
import {useDispatch, useSelector} from "react-redux";
import { bindActionCreators } from "redux";
import CompanyList, {CompanyItem} from "../../components/CompanyList";
import {RootState} from "../../store/types";
import * as CompanyActions from "../../store/Company/actions";
import TabIcon from "../../components/TabIcon";

const Company = (props: any) => {

    const device = useSelector((state: RootState) => state.register.register)
    const companys = useSelector((state: RootState) => state.company)

    const dispatch = useDispatch();
    const companyAction = bindActionCreators(CompanyActions, dispatch)


    useEffect(() => {
        companyAction.findAll({url: device.url, data: {token: device.token}})
    }, [])

    useEffect(() => {
        const selectedCompanies = companys.companies.filter(item => item.selected)
        if (selectedCompanies.length === 0 && companys.companies.length > 0) {
            companyAction.companySelect(companys.companies[0].id)
        }
    }, [companys])

    const handlerConfigClick = async () => {
        props.route.params.mainNavigation.dispatch(StackActions.replace('Config'))
    }

    const handlerSelectedCompany = (companyItem: CompanyItem) => {
        const selectedCompanies = companys.companies.filter(item => item.selected)
        if (selectedCompanies.find(item => item.id === companyItem.id) && selectedCompanies.length === 1){
            ToastAndroid.showWithGravity("Mantenha pelo menos uma filial selecionada.", 3000, ToastAndroid.BOTTOM)
            return
        }
        companyAction.companySelect(companyItem.id)
    }

    return (
        <CompanyWrapper>
            <CompanyConfig onPress={handlerConfigClick}><Text><Icon name={"settings"} size={28} color={"white"}/></Text></CompanyConfig>
            <LogoWrapper>
                <Logo/>
            </LogoWrapper>
            <CompanyTitle>Selecione a empresa desejada:</CompanyTitle>
            <CompanyList company={companys.companies} onCompanySelected={handlerSelectedCompany}/>
        </CompanyWrapper>
    )
}

export default Company;

export const CompanyTabOptions = {
    tabBarIcon: (props: any) => <TabIcon focused={props.focused} icon={"location-city"}/>
}
