import React from 'react';
import {ModulesListTitle, ModulesLogo, ModulesWrapper} from "./style";
import ModuleList from "../../components/ModuleList";
import {Logo} from "../../assets/svg";
import { Modules } from "../../modules"
import {Module} from "../../lib/Module";
import { StackNavigationProp } from "@react-navigation/stack";


interface ModulesListProps {
    navigation: StackNavigationProp<any>
}

const ModulesList = (props: ModulesListProps) => {

    const handlerModuleClick = (module: Module<any>) => {
        props.navigation.push('ModuleList', {module: module.name})
    }
    return (
        <ModulesWrapper>
            <ModulesLogo>
                <Logo/>
            </ModulesLogo>
            <ModulesListTitle>
                Selecione o módulo desejado:
            </ModulesListTitle>
            <ModuleList modules={Modules} onModuleClick={handlerModuleClick}/>
        </ModulesWrapper>
    )
}

export default ModulesList
