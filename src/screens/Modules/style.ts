import styled from 'styled-components/native';

export const ModulesWrapper = styled.View`
  flex: 1;
  background-color: #3366FF;
  width: 100%;
  padding: 25px 10px 10px 10px;
`;

export const ModulesLogo = styled.View`
  align-items: center;
  margin-top: 20px;
  text-align: center;
`

export const ModulesListTitle = styled.Text`
  color: white;
  font-size: 16px;
  text-align: center;
  margin: 20px 0;
  font-weight: bold;
`;

