import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import Icon from "react-native-vector-icons/MaterialIcons";
import ModulesList from "./modulesList";
import ModuleContainerList from "../ModuleList";
import ModuleForm from "../ModuleForm";
import TabIcon from "../../components/TabIcon";

const Stack = createStackNavigator();

const ModulesRoutes = () => {
    return (
        <Stack.Navigator initialRouteName={"ModulesList"} screenOptions={{headerShown: false}}>
            <Stack.Screen name={"ModulesList"} component={ModulesList} />
            <Stack.Screen name={"ModuleList"} component={ModuleContainerList}/>
            <Stack.Screen name={"ModuleForm"} component={ModuleForm}/>
        </Stack.Navigator>
    )
}

export default ModulesRoutes


export const ModulesTabOptions = {
    tabBarIcon: (props: any) => <TabIcon focused={props.focused} icon={"apps"}/>
}
