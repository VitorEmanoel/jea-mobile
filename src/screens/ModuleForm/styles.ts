import styled from "styled-components/native";

export const ModuleFormWrapper = styled.View`
  position: relative;
  flex: 1;
  width: 100%;
  flex-direction: column;
  background-color: #3366FF;
`;

export const ModuleFormContainer = styled.View`
  flex: 1;
  flex-direction: column;
  margin: 50px 20px 20px 20px;
  border-radius: 10px;
  background-color: white;
  padding: 10px;
`

export const ModuleFormHeader = styled.View`
  flex-direction: row;
  width: 100%;
  align-items: center;
  justify-content: center;
  margin: 15px 0 20px 0;
`;

export const ModuleFormTitle = styled.Text`
  font-size: 20px;
  color: #6B6B6B;
  font-weight: bold;
  margin-left: 7px;
`;


