import React from 'react';
import {ModuleFormContainer, ModuleFormHeader, ModuleFormTitle, ModuleFormWrapper} from "./styles";
import BackButton from "../../components/BackButton";
import {StackNavigationProp} from "@react-navigation/stack";
import {Module} from "../../lib/Module";
import {ModulesMapped} from "../../modules";
import Icon from "react-native-vector-icons/MaterialIcons"
import {ScrollView} from "react-native";

interface ModuleFormProps {
    navigation: StackNavigationProp<any>;
    route: any
}

const ModuleForm = (props: ModuleFormProps) => {

    const module: Module<any> = ModulesMapped()[props.route.params.module];
    const item: any = props.route.params.item;

    return (
        <ModuleFormWrapper >
            <BackButton navigation={props.navigation} backRoute={"ModuleList"} data={{module: module.name}} />
            <ModuleFormContainer>
                <ModuleFormHeader>{(module && module.form && (module.form.title || module.form.icon)) && (<>{module.form.icon && <Icon name={module.form.icon} size={20} color={"#3366FF"}/>}<ModuleFormTitle>{module.form.title}</ModuleFormTitle></>) }</ModuleFormHeader>
                {module && module.form && module.form.form(item)}
            </ModuleFormContainer>
        </ModuleFormWrapper>
    )
}

export default ModuleForm;
