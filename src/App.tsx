import React from "react";
import Screens from "./screens";
import {Provider} from "react-redux";
import {PersistGate} from 'redux-persist/integration/react'
import {Persistor, Store} from "./store"
import {StatusBar} from "react-native";
import CodePush, {CodePushOptions} from "react-native-code-push";

const updaterOptions: CodePushOptions = {
    checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
    installMode: CodePush.InstallMode.IMMEDIATE,
}

const App = () => {
    return (
        <Provider store={Store}>
            <PersistGate persistor={Persistor} >
                <StatusBar hidden={true} backgroundColor={"#3366FF"}/>
                <Screens/>
            </PersistGate>
        </Provider>
    )
}
export default CodePush(updaterOptions)(App)
