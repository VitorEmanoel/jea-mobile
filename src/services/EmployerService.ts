import {IService} from "../lib/Dataflow";
import {Employer} from "../modules/EmployerModule";
import BaseService from "./BaseService";

export const EmployerService = (): IService<Employer> => BaseService({name: "EMPLOYER", router: "employers"})
