import {DeviceRegister} from "../store/DeviceRegister";
import Axios, { AxiosResponse, AxiosInstance } from "axios";

export interface SessionUser {
    id: number;
    name: string;
    username: string
}

export interface SessionAccess {
    id: number;
    ipAddress: string;
    accessAt: string;
}

export interface Session {
    id: number;
    user: SessionUser;
    access: SessionAccess[];
    model: string;
    deviceId: string;
    platform: string;
    version: string;
    type: string;

}

export interface ISessionService {
    register: (register: DeviceRegister) => Promise<AxiosResponse<Session>>
}

const SessionService = ():ISessionService => {

    const client: AxiosInstance = Axios.create({
        timeout: 5000
    })

    return {
        register(register: DeviceRegister): Promise<AxiosResponse<Session>> {
            return client.post(`${register.url}/session/mobile`,
                {model: register.model, deviceId: register.imei, platform: register.plataform, version: register.version},
                {headers: {"Authorization": register.token}});
        }
    }
}

export default SessionService()
