import {IService} from "../lib/Dataflow";
import {Perfomance} from "../modules/PerfomanceModule";
import {AxiosResponse} from "axios";
import {CreateRequest, DeleteRequest, ListAllRequest, ListRequest, UpdateRequest} from "../lib/Dataflow/request";
import ReportManager from "../reports"
import {TokenNotFound} from "./BaseService";

const PerformanceService = (): IService<Perfomance> => {
    return {
        findAll(request: ListAllRequest): Promise<AxiosResponse<Perfomance[]>> {
            if (!request.data || !request.data.token) {
                throw new TokenNotFound()
            }
            return ReportManager.get("performance", {url: request.url, token: request.data.token})
        },
        delete(request: DeleteRequest): Promise<AxiosResponse<any>> {
            throw new Error("not implemented")
        },
        create(request: CreateRequest<Perfomance>): Promise<AxiosResponse<any>> {
            throw new Error("not implemented")
        },
        find(request: ListRequest): Promise<AxiosResponse<Perfomance>> {
            throw new Error("not implemented")
        },
        update(request: UpdateRequest<Perfomance>): Promise<AxiosResponse<any>> {
            throw new Error("not implemented")
        },
        name: "PERFORMANCE"
    }
}

export default PerformanceService()
