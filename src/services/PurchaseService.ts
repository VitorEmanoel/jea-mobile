import {IService} from "../lib/Dataflow";
import {Purchase} from "../modules/PurchaseModule";
import BaseService from "./BaseService";

const PurchaseService = ():IService<Purchase> => {
    return BaseService({name: "PURCHASE", router: "purchases"})
}

export default PurchaseService
