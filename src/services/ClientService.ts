import {IService} from "../lib/Dataflow";
import {Client} from "../modules/ClientsModule";
import BaseService from "./BaseService";

const ClientService = (): IService<Client> => {
    return BaseService({router: "clients", name: "CLIENTS"})
}

export default ClientService
