import {IService} from "../lib/Dataflow";
import {Product} from "../modules/ProductModule";
import BaseService from "./BaseService";

const ProductService = (): IService<Product> => {
    return BaseService({router: "products", name: "PRODUCT"})
}

export default ProductService;
