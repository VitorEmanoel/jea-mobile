import {CreateRequest, DeleteRequest, ListAllRequest, ListRequest, UpdateRequest} from "../lib/Dataflow/request";
import {IService} from "../lib/Dataflow";
import Axios, {AxiosResponse, AxiosInstance} from "axios";
interface BaseServiceConfig {
    name: string;
    router: string;
}

const BaseService = (config: BaseServiceConfig):IService<any> => {
    const { name, router } = config;

    const client: AxiosInstance = Axios.create({
        timeout: 10000
    })

    return {
        create(request: CreateRequest<any>): Promise<AxiosResponse<any>> {
            throw new Error("not implemented")
        },
        find(request: ListRequest): Promise<AxiosResponse<any>> {
            throw new Error("not implemented")
        },
        findAll(request: ListAllRequest): Promise<AxiosResponse<any[]>> {
            if (!request.data || !request.data.token){
                throw new TokenNotFound();
            }
            let params = "?"
            if (request.limit && request.page) {
                params += `limit=${request.limit}&page=${request.page}`
            }
            return client.get(`${request.url}/${router}${params}`, { headers: { "Authorization": request.data.token}})
        },
        name: name,
        update(request: UpdateRequest<any>): Promise<AxiosResponse<any>> {
            throw new Error("not implemented")
        },
        delete(request: DeleteRequest): Promise<AxiosResponse<any>> {
            throw new Error("not implemented")
        }

    }
};

export default BaseService

export class TokenNotFound extends Error{
    constructor() {
        super("Token not found in service request")
    }
}
