import {IService} from "../lib/Dataflow";
import {CarRental} from "../modules/CarRentalModule";
import BaseService from "./BaseService";

export const CarRentalService = (): IService<CarRental> => BaseService({router: "carRentals", name: "CAR_RENTAL"});
