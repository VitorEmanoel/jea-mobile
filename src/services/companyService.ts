import {Company} from "../modules/Company";
import {IService} from "../lib/Dataflow";
import {CreateRequest, DeleteRequest, ListAllRequest, ListRequest, UpdateRequest} from "../lib/Dataflow/request";
import Axios, { AxiosResponse, AxiosInstance } from "axios";
import BaseService from "./BaseService";

export const CompanyService = (): IService<Company> => {

    return BaseService({router: "companies", name: "COMPANY"})
}
