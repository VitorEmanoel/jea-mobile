import Axios, { AxiosResponse, AxiosInstance } from 'axios';
import {IService} from "../lib/Dataflow";
import {AccountPayable, AccountReceivable} from "../modules/AccountModule";
import {CreateRequest, DeleteRequest, ListAllRequest, ListRequest, UpdateRequest} from "../lib/Dataflow/request";
import BaseService from "./BaseService";

export interface APIAccountReceivable {
    id: number;
    cliente_id: number;
    razao_social: string;
    descricao: string;
    vencimento: Date;
    valor: number;
    status: string;
}

export const AccountReceivableService = (): IService<AccountReceivable> => {

    return BaseService({router: "accountReceivable", name: "ACCOUNT_RECEIVABLE"})
}

export const AccountPayableService = (): IService<AccountPayable> => {

    return BaseService({router: "accountPayable", name: "ACCOUNT_PAYABLE"})
}
