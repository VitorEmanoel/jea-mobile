import {IService} from "../lib/Dataflow";
import {Sale} from "../modules/SaleModule";
import BaseService from "./BaseService";

export const SaleService = (): IService<Sale> => {
    return BaseService({router: "sales", name: "SALE"})
}
