import Reactotron from "reactotron-react-native";
import { reactotronRedux } from "reactotron-redux";
import sagaPlugin from "reactotron-redux-saga";
import AsyncStorage from "@react-native-community/async-storage";

// @ts-ignore
const reactoron = Reactotron
    .configure({host: '192.168.2.101', port: 9595, name: "JEA - APP"})
    .useReactNative({overlay: false})
    .setAsyncStorageHandler(AsyncStorage)
    .use(reactotronRedux())
    .use(sagaPlugin({}))
    .connect();
export default reactoron;

