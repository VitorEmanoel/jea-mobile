interface MoneyFormatter {
    Format: (value: number) => string;
    SetCurrency: (currency: string) => void;
}

export const Formatter = ():MoneyFormatter => {

    let Currency = ""

    return {
        Format(value: number): string {
            return Currency + value.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.').replace(".", ",")
        },
        SetCurrency(currency: string){
            Currency = currency
        }
    }
}
