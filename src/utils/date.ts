import moment from "moment";

export enum Month {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
}

export namespace Month {
    export function keys(): string[] {
        return Object.keys(Month).filter(k => typeof Month[k as any] === "number")
    }
    export function translateMonth(month: Month, language: Languages): string {
        return TranslatedMonths[language][month];
    }
}

export enum Languages {
    PTBR
}

export interface TranslateMonth {
    [month: number]: string
}

export interface TranslatedMonthLanguages {
    [language: number]: TranslateMonth
}

export function translateMonth(month: Month, language: Languages): string {
    return TranslatedMonths[language][month];
}

const TranslatedMonthsPTBR: TranslateMonth = {
    0: "Janeiro",
    1: "Fevereiro",
    2: "Março",
    3: "Abril",
    4: "Maio",
    5: "Junho",
    6: "Julho",
    7: "Agosto",
    8: "Setembro",
    9: "Outubro",
    10: "Novembro",
    11: "Dezembro",
}

const TranslatedMonths: TranslatedMonthLanguages = {
    0: TranslatedMonthsPTBR
}

export type DateValidate = (date: Date) => boolean;

export interface DateOptions{
    text: string;
    validate: DateValidate;
}


export const SortingDate = (a: Date, b: Date) => a.getTime() - b.getTime()

export interface TodayValidationType {
    type: typeof DateValidations.Today
}

export interface WeekValidationType {
    type: typeof DateValidations.Week
}

export interface DaysValidationType {
    type: typeof DateValidations.Days
    payload: number;
}

export interface MonthValidationType {
    type: typeof DateValidations.Month
    payload: number;
}

export type DateValidationType = TodayValidationType | WeekValidationType | DaysValidationType | MonthValidationType;

export enum DateValidations {
    Today,
    Week,
    Days,
    Month
}

export namespace DateValidations {
    export function validation(type: DateValidationType): DateValidate {
        const now = new Date()
        switch (type.type) {
            case DateValidations.Days:
                const initialDate = moment(now).startOf("day").subtract(type.payload, "day")
                const finalDate = moment(now).endOf("day")
                return (date: Date): boolean => moment(date).isBetween(initialDate, finalDate)
            case DateValidations.Today:
                return (date: Date): boolean => moment(date).isBetween(moment(now).startOf("day"), moment(now).endOf("day"))
            case DateValidations.Week:
                return (date: Date): boolean => moment(date).isBetween(moment(now).startOf("week"), moment(now).endOf("week"))
            case DateValidations.Month:
                return (date: Date): boolean => moment(date).isBetween(moment(now).set('month', type.payload).startOf("month"), moment(now).set('month', type.payload).endOf('month'))
        }
    }
}
