import styled from 'styled-components/native';
import React from "react";

export const ModuleFormTitle = styled.Text`
  font-size: 16px;
  font-weight: bold;
  margin: 10px 5px;
  color: #6B6B6B;
`;

export const ModuleFormListContainer = styled.View`
  flex: 1;
  border-radius: 5px;
  background-color: #F3F7FF;
`;

export const ModuleFormItemContainer = styled.View`
  flex-direction: row;
  padding: 8px 10px;
  background-color: #ffffff;
  border-radius: 5px;
`;

export const ModuleFormItemsContainer = styled.View`
  flex-direction: column;
  background-color: #fff;
  elevation: 1;
  border-radius: 5px;
`

export const ModuleFormItemPlaceholder = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #5B5B5B;
`;

export const ModuleFormItemValue = styled.Text`
  font-size: 15px;
  color: #8B8B8B;
  margin-left: 5px;
`

export const EmptyContentWrapper = styled.View`
  flex: 1;
  height: 100%;
  width: 100%;
  align-items: center;
  justify-content: center;
`;

export const EmptyContentContainer = styled.View`
  flex-direction: column;
`;

export const EmptyContentTitle = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #6B6B6B;
`;

export const HeaderContainer = styled.View`
   flex-direction: column;
   flex: 1;
`;

export const HeaderTitle = styled.Text`
  font-size: 20px;
  color: #6B6B6B;
  text-align: center;
  margin: 10px 0;
  font-weight: bold;
`

export const ChartTitle = styled.Text`
  font-size: 16px;
  text-align: center;
  margin: 5px 0;
`;

export const ChartsContainer = styled.View`
  margin: 10px 0;
  flex-direction: column;
`

export const MetricsContainer = styled.View`
  justify-content: space-evenly;
  align-items: center;
`;

export const ModuleFormItemTitle = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: #6B6B6B;
  text-align: center;
  padding: 10px 0;
`

interface ModuleFormItemProps {
    placeholder: JSX.Element | string;
    value: string;
}

export const ModuleFormItem = (props: ModuleFormItemProps) => {

    const { placeholder, value } = props;

    return (
        <ModuleFormItemContainer>
            <ModuleFormItemPlaceholder>{placeholder}:</ModuleFormItemPlaceholder>
            <ModuleFormItemValue>{value}</ModuleFormItemValue>
        </ModuleFormItemContainer>
    )
};
