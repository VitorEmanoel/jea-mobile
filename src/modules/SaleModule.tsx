import React from "react";
import {
    GeneralConfiguration,
    Module,
    ModuleColors,
    ModuleForm,
    ModuleIcons,
    ModuleList,
    ModuleStatistics
} from "../lib/Module";
import {FlatList, ListRenderItemInfo, Text} from "react-native";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import {
    EmptyContentContainer,
    EmptyContentTitle,
    EmptyContentWrapper,
    HeaderContainer,
    HeaderTitle,
    ModuleFormItem,
    ModuleFormItemsContainer,
    ModuleFormListContainer,
    ModuleFormTitle
} from "./styles";
import {SaleService} from "../services/saleService";
import {ModuleListItemContainer, ModuleListItemFooter, ModuleListItemHeader} from "../screens/ModuleList/style";
import {Formatter} from "../utils/formatter";
import moment from "moment";
import {Product} from "./ProductModule";
import {ChartType, LineChartConfig, PieChartConfig} from "../components/ModuleChart/charts";
import ChartGroup from "../components/ChartGroup";
import {GroupsConfig} from "../components/ChartGroup/groups";
import {DateOptions, DateValidations} from "../utils/date";

export interface Purchaser {
    id: number;
    name: string;
    type: number;
}

const ProductBestSellerColors = {
    0: "#e8513e",
    1: "#fc620b",
    2: "#fbc62c",
    4: "#339373",
    5: "#1081af",
    6: "#522a5e",
    7: "#9a539f",
    8: "#C3522B",
    9: "#A3320B"
}

export interface PaymentsMapper {
    [key: number]: number;
}

export interface Seller {
    id: number;
    name: string;
}

export enum PaymentType {
    MONEY = 1,
    DEBIT_CARD ,
    CREDIT_CARD,
    DUPLICATE
}

export function translatePaymentType(type: PaymentType): string {
    switch (type) {
        case PaymentType.MONEY:
            return "Dinheiro";
        case PaymentType.DEBIT_CARD:
            return "Cartão de Débito"
        case PaymentType.CREDIT_CARD:
            return "Cartão de Crédito"
        case PaymentType.DUPLICATE:
            return "Duplicata"
    }
}

export function colorPaymentType(type: PaymentType): string {
    switch (type) {
        case PaymentType.MONEY:
            return "#52b788"
        case PaymentType.DEBIT_CARD:
            return "#a7c6da"
        case PaymentType.CREDIT_CARD:
            return "#96acb7"
        case PaymentType.DUPLICATE:
            return "#a4a5a4"
    }
}

export interface SaleProduct{
    id: number;
    product: Product;
    quantity: number;
    increase: number;
    discount: number;
}

export interface Sale {
    id: number;
    total: number;
    purchaser: Purchaser;
    discount: number;
    increase: number;
    createdAt: string;
    paymentType: PaymentType
    seller: Seller;
    products?: SaleProduct[];
}

export class SaleModule implements Module<Sale> {

    configuration: GeneralConfiguration = {
        showInStory: true,
        showInModuleList: true,
    }

    titleIcon = "shopping-cart"

    colors: ModuleColors = {
        module: "rgba(233, 233, 233, 0.56)",
    };

    dataFlow: IDataflow<Sale> = DataFlow(this, SaleService())

    displayName = "Vendas";
    icon: ModuleIcons = {
        module: "credit-card"
    };
    multiCompany = true;
    name = "SALE";

    moduleList: ModuleList<Sale> = {

        orderFilterList: [],
        filterList: [],
        emptyContent: (
            <EmptyContentWrapper>
                <EmptyContentContainer>
                    <EmptyContentTitle>Não foi encontrado nenhum item</EmptyContentTitle>
                </EmptyContentContainer>
            </EmptyContentWrapper>
        ),
        renderItem(info: ListRenderItemInfo<Sale>): React.ReactElement {

            const deltaItem = info.item.increase - info.item.discount;

            return (
                <ModuleListItemContainer style={{borderBottomWidth: 1, borderBottomColor: '#CACACA'}}>
                    <ModuleListItemHeader>
                        <Text style={{fontWeight: "bold"}}>{info.item.id}</Text>
                        <Text style={{marginLeft: 5, maxWidth: "50%"}} numberOfLines={1}>{info.item.purchaser.name}</Text>
                        <Text style={{marginLeft: 'auto', marginRight: 5}}>R$ {Formatter().Format(info.item.total)}</Text>
                    </ModuleListItemHeader>
                    <ModuleListItemFooter>
                        <Text>{moment(new Date(info.item.createdAt)).format("L")}</Text>
                        {(deltaItem != 0) && (
                            <>
                                <Text style={{marginLeft: 'auto', color: deltaItem > 0 ? 'green' : 'red'}}> R$ {deltaItem > 0 && '+'}{Formatter().Format(deltaItem)}</Text>
                            </>
                        )}

                    </ModuleListItemFooter>
                </ModuleListItemContainer>
            )
        }
    }

    form: ModuleForm<Sale> = {
        title: "Venda",
        icon: "shopping-cart",
        form: item => {
            const format = Formatter()

            return (
                <>
                    <ModuleFormItemsContainer>
                        <ModuleFormItem placeholder={"ID"} value={item.id.toString()}/>
                        <ModuleFormItem placeholder={"Forma de pagamento"} value={translatePaymentType(item.paymentType)}/>
                        {item.purchaser && (
                            <ModuleFormItem placeholder={"Cliente"} value={item.purchaser.name}/>
                        )}
                        {item.seller && (
                            <ModuleFormItem placeholder={"Vendedor"} value={item.seller.name}/>
                        )}
                        {item.discount > 0 && (
                            <ModuleFormItem placeholder={"Desconto"} value={`R$ ${format.Format(item.discount)}`}/>
                        )}
                        {item.increase > 0 && (
                            <ModuleFormItem placeholder={"Incremento"} value={`R$ ${format.Format(item.increase)}`}/>
                        )}
                        <ModuleFormItem placeholder={"Data"} value={moment(new Date(item.createdAt)).format("L")}/>
                    </ModuleFormItemsContainer>
                    <ModuleFormTitle>Produtos:</ModuleFormTitle>
                    <ModuleFormListContainer>
                        <FlatList data={item.products} renderItem={(item) => {
                            return (
                                <ModuleListItemContainer style={{borderBottomWidth: 1, borderBottomColor: '#CACACA'}}>
                                    <ModuleListItemHeader>
                                        <Text style={{fontWeight: "bold"}}>{item.item.product.id}</Text>
                                        <Text style={{marginLeft: 5}}>{item.item.product.description}</Text>
                                        <Text style={{marginLeft: 'auto'}}>{item.item.quantity} x R$ {Formatter().Format(item.item.product.price)}</Text>
                                    </ModuleListItemHeader>
                                </ModuleListItemContainer>
                            )
                        }} keyExtractor={(item, key) => key.toString()}/>
                    </ModuleFormListContainer>
                    <ModuleFormItem placeholder={"Total"} value={`R$ ${format.Format(item.total)}`}/>
                </>
            )
        }
    }
    statistics: ModuleStatistics = {
        header : () => {

            const dateOptions: DateOptions[] = [
                {
                    text: "Hoje",
                    validate: DateValidations.validation({type: DateValidations.Today})
                },
                {
                    text: "7 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 7})
                },
                {
                    text: "15 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 15})
                },
                {
                    text: "30 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 30})
                },
            ]

            const salesResumeLinearCharts: LineChartConfig<Sale> = {
                type: ChartType.LINE,
                dataSelector: (item) => item.total,
                title: "Vendas resumida",
                labelSelector: (item => `${moment(item.createdAt).format("DD/MM")}`),
                showNotFoundText: true,
                chartProps: {
                    bezier: true,
                    yAxisInterval: 1,
                    formatYLabel: value => Formatter().Format(parseFloat(value))
                },
                dateSelector: (item) => new Date(item.createdAt),
                preLabelSelector: items => items.filter((int, i) => i == 0 || i % 2 == 0 || i == items.length - 1),
                organizeItems: (items => items),
            }

            const salesPaymentMethodPieChart: PieChartConfig<Sale> = {
                type: ChartType.PIE,
                title: "Vendas por método de pagamento",
                dataSelector: items => {
                    let payments: PaymentsMapper = {};
                    items.forEach(sale => {
                        if (!payments[sale.paymentType]) {
                            payments[sale.paymentType] = 1;
                            return
                        }
                        payments[sale.paymentType] += 1
                    })
                    return Object.keys(payments).map(payment => {
                        const paymentType = parseInt(payment)
                        return {
                            color: colorPaymentType(paymentType),
                            legendFontColor: "#3366FF",
                            legendFontSize: 12,
                            name: translatePaymentType(paymentType),
                            population: payments[paymentType],
                        }
                    })
                },
                chartProps: {
                    accessor: "population"
                },
                organizeItems: items => items,
                dataGrouping: items => items,
                dateSelector: item => new Date(item.createdAt)
            }

            const bestSellingProducts: PieChartConfig<Sale> = {
                type: ChartType.PIE,
                title: "Produtos mais vendidos",
                dataSelector: items => {
                    const products: SaleProduct[] = [];
                    items.forEach(sale => {
                        if (!sale.products)
                            return
                        sale.products.forEach(product => {
                            const findProduct = products.find(item => item.product.id === product.product.id);
                            if (!findProduct) {
                                products.push(product)
                                return
                            }
                            const index = products.indexOf(findProduct)
                            products[index] = {...products[index], quantity: products[index].quantity + product.quantity}
                        })
                    })
                    products.sort((a, b) => b.quantity - a.quantity)
                    return products.slice(0, 10).map((item, index) => {
                        return {
                            population: item.quantity,
                            name: item.product.description,
                            legendFontSize: 12,
                            legendFontColor: "#3366FF",
                            color: ProductBestSellerColors[index]
                        }
                    })
                },
                chartProps: {
                    accessor: "population"
                },
                organizeItems: items => items,
                dataGrouping: items => items,
                dateSelector: item => new Date(item.createdAt)
            }
            const chartsConfig: GroupsConfig = {
                charts: [
                    {
                        config: salesResumeLinearCharts,
                        module: this.name,
                    },
                    {
                        config: salesPaymentMethodPieChart,
                        module: this.name,
                    },
                    {
                        config: bestSellingProducts,
                        module: this.name,
                    }
                ],
                options: dateOptions,
                showDateSelector: true
            }

            return (
                <HeaderContainer>
                    <HeaderTitle>{this.displayName}</HeaderTitle>
                    <ChartGroup config={chartsConfig}/>
                </HeaderContainer>
            )
        }
    }
}
