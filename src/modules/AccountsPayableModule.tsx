import {
    GeneralConfiguration,
    Module,
    ModuleColors,
    ModuleForm,
    ModuleIcons,
    ModuleList,
    ModuleStatistics
} from "../lib/Module";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import {AccountPayableService} from "../services/accountsService";
import {Formatter} from "../utils/formatter";
import {ModuleFormItem, ModuleFormItemsContainer} from "./styles";
import moment from "moment";
import {DateOptions, DateValidations, SortingDate} from "../utils/date";
import {ChartConfig, ChartType} from "../components/ModuleChart/charts";
import {GroupsConfig} from "../components/ChartGroup/groups";
import {EmptyContentContainer, EmptyContentTitle, EmptyContentWrapper, HeaderContainer, HeaderTitle} from "./styles";
import ChartGroup from "../components/ChartGroup";
import {ListRenderItemInfo, Text} from "react-native";
import React from "react";
import {ModuleListItemContainer, ModuleListItemFooter, ModuleListItemHeader} from "../screens/ModuleList/style";
import {Client} from "./ClientsModule";

export interface AccountPayable {
    id: number;
    client: Client;
    description: string;
    createdAt: string;
    amount: number;
    validationDate: string;
}

export default class AccountsPayableModule implements Module<AccountPayable> {

    configuration: GeneralConfiguration = {
        showInModuleList: true,
        showInStory: true
    }
    titleIcon = "attach-money"

    dataFlow: IDataflow<AccountPayable> = DataFlow(this, AccountPayableService());
    multiCompany = true
    colors: ModuleColors = {
        module: "rgba(230, 232, 131, 0.7)",
        story: "rgba(255, 51, 0, 0.4)"
    };
    displayName = "Contas à Pagar";
    icon: ModuleIcons = {
        module: "attach-money"
    };
    name = "ACCOUNT_PAYABLE";

    form:ModuleForm<AccountPayable> = {
        title: "Conta à Pagar",
        icon: "attach-money",
        form: (item) => {
            const formatter = Formatter()
            return (
                <>
                    <ModuleFormItemsContainer>
                        <ModuleFormItem placeholder={"ID"} value={item.id.toString()}/>
                        <ModuleFormItem placeholder={"Descrição"} value={item.description}/>
                        {item.client && (
                            <ModuleFormItem placeholder={"Cliente"} value={item.client.name}/>
                        )}
                        <ModuleFormItem placeholder={"Valor"} value={`R$ ${formatter.Format(item.amount)}`}/>
                        {item.validationDate && (
                            <ModuleFormItem placeholder={"Vencimento"} value={moment(item.validationDate).format("L")}/>
                        )}
                    </ModuleFormItemsContainer>
                </>
            )
        }
    }

    statistics: ModuleStatistics = {
        header: () => {

            const dateOptions: DateOptions[] = [
                {
                    text: "Hoje",
                    validate: DateValidations.validation({type: DateValidations.Today})
                },
                {
                    text: "7 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 7})
                },
                {
                    text: "15 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 15})
                },
                {
                    text: "30 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 30})
                },
            ]

            const linearChartConfig: ChartConfig<AccountPayable> = {
                type: ChartType.LINE,
                showNotFoundText: true,
                dataSelector: (item) => item.amount,
                labelSelector: (item => {
                    return `${moment(item.createdAt).format("DD/MM")}`
                }),
                title: "Contas à Pagar resumida",
                chartProps: {
                    bezier: true,
                    yAxisInterval: 1,
                    formatYLabel: value => Formatter().Format(parseFloat(value))
                },
                dateSelector: (item) => new Date(item.createdAt),
                preLabelSelector: items => items.filter((int, i) => i == 0 || i % 2 == 0 || i == items.length - 1),
                organizeItems: (items => items),
            }

            const barDetailsChartConfig: ChartConfig<AccountPayable> = {
                type: ChartType.BAR,
                dataSelector: (item) => item.amount,
                labelSelector: (item =>`${moment(item.createdAt).format("DD/MM")}`),
                title: "Contas à Pagar por dia",
                chartProps: {
                    formatYLabel: value => Formatter().Format(parseFloat(value))
                },
                dateSelector: (item) => new Date(item.createdAt),
                organizeItems: (items => items)
            }

            const chartsConfig: GroupsConfig = {
                options: dateOptions,
                showDateSelector: true,
                sorting: (a: any, b:any) => SortingDate(new Date(a.createdAt), new Date(b.createdAt)),
                charts: [
                    {
                        module: this.name,
                        config: linearChartConfig,
                    },
                    {
                        module: this.name,
                        config: barDetailsChartConfig,
                    }
                ],
            }

            return (
                <HeaderContainer>
                    <HeaderTitle>{this.displayName}</HeaderTitle>
                    <ChartGroup config={chartsConfig}/>
                </HeaderContainer>
            )
        }
    }

    moduleList: ModuleList<AccountPayable> = {

        filterList: [],

        orderFilterList: [],
        emptyContent: (
            <EmptyContentWrapper>
                <EmptyContentContainer>
                    <EmptyContentTitle>Não foi encontrado nenhum item</EmptyContentTitle>
                </EmptyContentContainer>
            </EmptyContentWrapper>
        ),
        renderItem(info: ListRenderItemInfo<AccountPayable>): React.ReactElement {
            return (
                <ModuleListItemContainer style={{borderBottomWidth: 1, borderBottomColor: '#CACACA'}}>
                    <ModuleListItemHeader>
                        <Text style={{fontWeight: "bold"}}>{info.item.id}</Text>
                        <Text style={{marginLeft: 5}}>{info.item.description}</Text>
                        <Text style={{marginLeft: 'auto', marginRight: 5}}>R$ {Formatter().Format(info.item.amount)}</Text>
                    </ModuleListItemHeader>
                    <ModuleListItemFooter>
                        <Text>{info.item.client.name}</Text>
                    </ModuleListItemFooter>
                </ModuleListItemContainer>
            )
        }
    };
}
