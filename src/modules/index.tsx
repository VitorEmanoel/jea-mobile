import {Module} from "../lib/Module";
import {SaleModule} from "./SaleModule";
import {BillingModule} from "./BillingModule";
import ClientsModule from "./ClientsModule";
import ProductModule from "./ProductModule";
import AccountsPayableModule from "./AccountsPayableModule";
import AccountsReceivableModule from "./AccountsReceivableModule";
import PurchaseModule from "./PurchaseModule";
import PerfomanceModule from "./PerfomanceModule";
import {CarRentalModule} from "./CarRentalModule";
import {EmployerModule} from "./EmployerModule";

export interface ModulesMapper {
    [name: string]: Module<any>
}

export const Modules: Module<any>[] = [
    new PerfomanceModule(),
    new SaleModule(),
    new AccountsReceivableModule(),
    new AccountsPayableModule(),
    new BillingModule(),
    new ClientsModule(),
    new ProductModule(),
    new PurchaseModule(),
    new CarRentalModule(),
    new EmployerModule(),
]

export const ModulesMapped = () => {
    const modulesMapper: ModulesMapper = {};
    Modules.forEach(item => modulesMapper[item.name] = item);
    return modulesMapper
}
