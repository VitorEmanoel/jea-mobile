import React from "react";
import {
    GeneralConfiguration,
    Module,
    ModuleColors,
    ModuleForm,
    ModuleIcons,
    ModuleList,
    ModuleStatistics
} from "../lib/Module";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import ProductService from "../services/ProductService";
import { ListRenderItemInfo, Text} from "react-native";
import {ModuleListItemContainer, ModuleListItemHeader} from "../screens/ModuleList/style";
import {Formatter} from "../utils/formatter";
import {ModuleFormItem, ModuleFormItemsContainer, ModuleFormItemTitle} from "./styles";
import {Company} from "./Company";
import Icon from "react-native-vector-icons/FontAwesome";

export interface ProductGroup {
    id: number;
    name: string;
}

export interface ProductTransfers {
    from: Company
    to: Company
    quantity: number;
}

export interface ProductStock {
    company: Company
    quantity: number;
}

export interface Product {
    id: number;
    createdAt: string;
    description: string;
    price: number;
    profit: number;
    group: ProductGroup;
    stock: ProductStock[]
    stockTransfers: ProductTransfers[]
}


export default class ProductModule implements Module<Product> {
    colors: ModuleColors = {
        module: "#a7ee99"
    };
    configuration: GeneralConfiguration = {
        showInModuleList: true,
        showInStory: false,

    };
    dataFlow: IDataflow<Product> = DataFlow(this, ProductService());
    displayName = "Produtos";
    form: ModuleForm<Product> = {
        title: "Produto",
        form: item => {
            const formatter = Formatter()
            return(
                <>
                    <ModuleFormItemsContainer>
                        <ModuleFormItem placeholder={"ID"} value={item.id.toString()}/>
                        <ModuleFormItem placeholder={"Descrição"} value={item.description}/>
                        <ModuleFormItem placeholder={"Preço"} value={"R$ " + formatter.Format(item.price)}/>
                        <ModuleFormItem placeholder={"Lucro médio"} value={`${item.profit}%`}/>
                        {item.group && (
                            <ModuleFormItem placeholder={"Grupo"} value={item.group.name}/>
                        )}
                    </ModuleFormItemsContainer>
                    {item.stock && (
                        <>
                            <ModuleFormItemTitle>Estoque</ModuleFormItemTitle>
                            <ModuleFormItemsContainer>
                                {item.stock.map((stock, index) => stock.company && <ModuleFormItem key={index.toString()} placeholder={stock.company.name} value={`${stock.quantity}`}/>)}
                            </ModuleFormItemsContainer>
                        </>
                    )}
                    {item.stockTransfers && (
                        <>
                            <ModuleFormItemTitle>Transferências</ModuleFormItemTitle>
                            <ModuleFormItemsContainer>
                                {item.stockTransfers.map((transfer, index) =>
                                    (transfer.to && transfer.from) && <ModuleFormItem key={index.toString()} placeholder={<Text>{transfer.from.name} <Icon size={16} color={"#3366FF"} name={"exchange"}/> {transfer.to.name}</Text>} value={`${transfer.quantity}`} />)}
                            </ModuleFormItemsContainer>
                        </>
                    )}
                </>
            )
        }
    };
    icon: ModuleIcons = {
        story: "local-offer",
        module: "local-offer",
    };
    moduleList: ModuleList<Product> = {
        emptyContent: (
            <></>
        ),
        renderItem(info: ListRenderItemInfo<Product>): React.ReactElement {
            return (
                <ModuleListItemContainer style={{borderBottomWidth: 1, borderBottomColor: '#CACACA'}}>
                    <ModuleListItemHeader>
                        <Text style={{fontWeight: "bold"}}>{info.item.id}</Text>
                        <Text style={{marginLeft: 5}}>{info.item.description}</Text>
                        <Text style={{marginLeft: 'auto'}}>R$ {Formatter().Format(info.item.price)}</Text>
                    </ModuleListItemHeader>
                </ModuleListItemContainer>
            )
        },
        orderFilterList: [],
        filterList: []
    };
    name = "PRODUCT";
    multiCompany = false;
    statistics: ModuleStatistics = {

    };
    titleIcon = "local-offer";

}
