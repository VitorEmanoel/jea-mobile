import {
    GeneralConfiguration,
    Module,
    ModuleColors,
    ModuleForm,
    ModuleIcons,
    ModuleList,
    ModuleStatistics
} from "../lib/Module";
import {ListRenderItemInfo, Text} from "react-native";
import React from "react";
import {ModuleListItemContainer, ModuleListItemFooter, ModuleListItemHeader} from "../screens/ModuleList/style";
import {
    EmptyContentContainer,
    EmptyContentTitle,
    EmptyContentWrapper,
    HeaderContainer,
    HeaderTitle
} from "./styles";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import {AccountReceivableService} from "../services/accountsService";
import {BarChartConfig, ChartType, LineChartConfig} from "../components/ModuleChart/charts";
import {DateOptions, DateValidations, SortingDate} from "../utils/date";
import {Formatter} from "../utils/formatter";
import ChartGroup from "../components/ChartGroup";
import {GroupsConfig} from "../components/ChartGroup/groups";
import moment from "moment";
import {ModuleFormItem, ModuleFormItemsContainer} from "./styles";
import {Client} from "./ClientsModule";

export interface AccountReceivable {
    id: number;
    client: Client;
    description: string;
    amount: number;
    createdAt: string;
    validationDate: string;
}


export default class AccountsReceivableModule implements Module<AccountReceivable> {

    configuration: GeneralConfiguration = {
        showInStory: true,
        showInModuleList: true,
    }
    titleIcon = "attach-money"

    dataFlow: IDataflow<AccountReceivable> = DataFlow(this, AccountReceivableService())
    multiCompany = true

    colors: ModuleColors = {
        module: "rgba(149, 228, 87, 0.74)",
        story: "rgba(0, 204, 0, 0.4)"
    };
    displayName = "Contas à Receber";
    icon: ModuleIcons = {
        module: "account-balance",
        story: "",
    };
    name = "ACCOUNT_RECEIVABLE";

    form:ModuleForm<AccountReceivable> = {
        title: "Conta à Receber",
        icon: "attach-money",
        form: (item) => {
            const formatter = Formatter();
            return (
                <>
                    <ModuleFormItemsContainer>
                        <ModuleFormItem placeholder={"ID"} value={item.id.toString()}/>
                        <ModuleFormItem placeholder={"Descrição"} value={item.description}/>
                        {item.client && (
                            <ModuleFormItem placeholder={"Cliente"} value={item.client.name}/>
                        )}
                        <ModuleFormItem placeholder={"Valor"} value={`R$ ${formatter.Format(item.amount)}`}/>
                        {item.validationDate && (
                            <ModuleFormItem placeholder={"Vencimento"} value={moment(item.validationDate).format("L")}/>
                        )}
                    </ModuleFormItemsContainer>
                </>
            )
        }
    }

    statistics: ModuleStatistics = {
        header: () => {

            const dateOptions: DateOptions[] = [
                {
                    text: "Hoje",
                    validate: DateValidations.validation({type: DateValidations.Today})
                },
                {
                    text: "7 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 7})
                },
                {
                    text: "15 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 15})
                },
                {
                    text: "30 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 30})
                },
            ]

            const linearChartConfig: LineChartConfig<AccountReceivable> = {
                type: ChartType.LINE,
                dataSelector: (item) => item.amount,
                title: "Contas à Receber resumida",
                labelSelector: (item => `${moment(item.createdAt).format("DD/MM")}`),
                chartProps: {
                    bezier: true,
                    yAxisInterval: 1,
                    formatYLabel: value => Formatter().Format(parseFloat(value))
                },
                dateSelector: (item) => new Date(item.createdAt),
                preLabelSelector: items => items.filter((int, i) => i == 0 || i % 2 == 0 || i == items.length - 1),
                organizeItems: (items => items),
            }

            const barDetailsChartConfig: BarChartConfig<AccountReceivable> = {
                type: ChartType.BAR,
                dataSelector: (item) => item.amount,
                title: "Contas à Receber por dia",
                showNotFoundText: true,
                labelSelector: (item => `${moment(item.createdAt).format("DD/MM")}`),
                dateSelector: (item) => new Date(item.createdAt),
                organizeItems: (items => items)
            }

            const chartsConfig: GroupsConfig = {
                options: dateOptions,
                showDateSelector: true,
                sorting: (a:AccountReceivable, b: AccountReceivable) => SortingDate(new Date(a.createdAt), new Date(b.createdAt)),
                charts: [
                    {
                        module: this.name,
                        config: linearChartConfig,
                    },
                    {
                        module: this.name,
                        config: barDetailsChartConfig,
                    }
                ],
            }

            return (
                <HeaderContainer>
                    <HeaderTitle>{this.displayName}</HeaderTitle>
                    <ChartGroup config={chartsConfig}/>
                </HeaderContainer>
            )
        }
    }

    moduleList: ModuleList<AccountReceivable> = {

        orderFilterList: [
            {
                displayName: "ID", name: "ID"
            },
            {
                displayName: "Descrição", name: "DESCRIPTION"
            }
        ],

        filterList: [
            {displayName: "Nome", name: "ID"},
            {displayName: "Status", name: "STATUS"}
        ],

        emptyContent: (
            <EmptyContentWrapper>
                <EmptyContentContainer>
                    <EmptyContentTitle>Não foi encontrado nenhum item</EmptyContentTitle>
                </EmptyContentContainer>
            </EmptyContentWrapper>
        ),

        renderItem(info: ListRenderItemInfo<AccountReceivable>): React.ReactElement {
            return (
                <ModuleListItemContainer style={{borderBottomWidth: 1, borderBottomColor: '#CACACA'}}>
                    <ModuleListItemHeader>
                        <Text style={{fontWeight: "bold"}}>#{info.item.id}</Text>
                        <Text style={{marginLeft: 5}}>{info.item.description}</Text>
                        <Text style={{marginLeft: 'auto', marginRight: 5}}>R$ {Formatter().Format(info.item.amount)}</Text>
                    </ModuleListItemHeader>
                    <ModuleListItemFooter>
                        <Text>{info.item.client.name}</Text>
                    </ModuleListItemFooter>
                </ModuleListItemContainer>
            )
        },
    }
}
