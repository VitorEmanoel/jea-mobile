import React from 'react';
import {
    GeneralConfiguration,
    Module,
    ModuleColors,
    ModuleForm,
    ModuleIcons,
    ModuleList,
    ModuleStatistics
} from "../lib/Module";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import {CarRentalService} from "../services/CarRentalService";
import {ListRenderItemInfo, Text} from "react-native";
import {
    EmptyContentContainer,
    EmptyContentTitle,
    EmptyContentWrapper,
    ModuleFormItem,
    ModuleFormItemsContainer
} from "./styles";
import {ModuleListItemContainer, ModuleListItemHeader} from "../screens/ModuleList/style";
import {Client} from "./ClientsModule";
import {translatePaymentType} from "./SaleModule";
import {Formatter} from "../utils/formatter";

export interface CarRental {
    id: string;
    client: Client
    vehiclePlate: string;
    days: number;
    dayValue: number;
    discount: number;
    outDate: string;
    km: number;
    fuel: number;
    paymentType: number;
    total: number;
}

export class CarRentalModule implements Module<CarRental> {
    colors: ModuleColors = {
        module: "#c7c784"
    };
    configuration: GeneralConfiguration = {
        showInModuleList: true,
        showInStory: false
    };
    dataFlow: IDataflow<CarRental> = DataFlow<CarRental>(this, CarRentalService());
    displayName = "Locação de Veículo";
    form: ModuleForm<CarRental> = {
        title: "Locação de Veículo",
        icon: "directions-car",
        form: item => {
            const formatter = Formatter()
            return (
                <>
                    <ModuleFormItemsContainer>
                        <ModuleFormItem placeholder={"ID"} value={`${item.id}`}/>
                        {item.client && (
                            <ModuleFormItem placeholder={"Cliente"} value={item.client.name}/>
                        )}
                        <ModuleFormItem placeholder={"Placa do Veículo"} value={item.vehiclePlate}/>
                        <ModuleFormItem placeholder={"Dias alugados"} value={`${item.days}`}/>
                        <ModuleFormItem placeholder={"Preço diária"} value={`R$ ${formatter.Format(item.dayValue)}`}/>
                        <ModuleFormItem placeholder={"KM"} value={`${item.km} km`}/>
                        <ModuleFormItem placeholder={"Combustivel"} value={`${item.fuel}`}/>
                        <ModuleFormItem placeholder={"Tipo de Pagamento"} value={translatePaymentType(item.paymentType)}/>
                        <ModuleFormItem placeholder={"Total"} value={`R$ ${formatter.Format(item.total)}`}/>
                    </ModuleFormItemsContainer>
                </>
            )
        }
    };
    icon: ModuleIcons = {
        module: "directions-car"
    };
    moduleList: ModuleList<CarRental> = {
        emptyContent: (
            <EmptyContentWrapper>
                <EmptyContentContainer>
                    <EmptyContentTitle>Não foi encontrado nenhum item</EmptyContentTitle>
                </EmptyContentContainer>
            </EmptyContentWrapper>
        ),
        orderFilterList: [],
        filterList: [],
        renderItem(info: ListRenderItemInfo<CarRental>): React.ReactElement {
            return (
                <ModuleListItemContainer style={{borderBottomWidth: 1, borderBottomColor: '#CACACA'}}>
                    <ModuleListItemHeader>
                        <Text style={{fontWeight: "bold"}}>{info.item.id}</Text>
                        <Text style={{marginLeft: 5}}>{info.item.client.name}</Text>
                    </ModuleListItemHeader>
                </ModuleListItemContainer>
            )
        }
    };
    multiCompany = false;
    name = "CAR_RENTAL";
    titleIcon = "directions-car";

}
