import {
    GeneralConfiguration,
    Module,
    ModuleColors,
    ModuleForm,
    ModuleIcons,
    ModuleList,
    ModuleStatistics
} from "../lib/Module";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import PerfomanceService from "../services/PerfomanceService";
import React from "react";
import PerfomanceDashboard from "../components/PerfomanceDashboard";
import {Company} from "./Company";

export interface CompanyPerfomance{
    company: Company
    totalSale: number;
    totalSaleProfit: number;
    totalItems: number;
    totalPayments: number;
    totalDiscount: number;
    totalProfit: number;
    totalOutgoing: number;
}

export interface Perfomance {
    companies: CompanyPerfomance[];
}

export default class PerfomanceModule implements Module<Perfomance> {
    configuration: GeneralConfiguration = {
        showInStory: true,
        showInModuleList: false
    };
    dataFlow: IDataflow<Perfomance> = DataFlow(this, PerfomanceService);
    displayName = "Perfomance";
    icon: ModuleIcons = {
        story: "insert-chart",
        module: "insert-chart",
    };
    multiCompany = false;
    name = "PERFOMANCE";
    titleIcon = "Perfomance";
    statistics: ModuleStatistics = {
        header: () => <PerfomanceDashboard module={this.name}/>
    }
}
