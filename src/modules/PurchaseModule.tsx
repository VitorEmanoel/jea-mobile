import React from "react";
import {FlatList, ListRenderItemInfo, ScrollView, Text} from "react-native";
import {
    GeneralConfiguration,
    Module,
    ModuleColors,
    ModuleForm,
    ModuleIcons,
    ModuleList,
    ModuleStatistics
} from "../lib/Module";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import PurchaseService from "../services/PurchaseService";
import {
    EmptyContentContainer,
    EmptyContentTitle,
    EmptyContentWrapper, HeaderContainer, HeaderTitle,
    ModuleFormItem,
    ModuleFormItemsContainer, ModuleFormItemTitle, ModuleFormListContainer, ModuleFormTitle
} from "./styles";
import {ModuleListItemContainer, ModuleListItemFooter, ModuleListItemHeader} from "../screens/ModuleList/style";
import {Formatter} from "../utils/formatter";
import moment from "moment";
import {Client} from "./ClientsModule";
import {Product} from "./ProductModule";
import {Company} from "./Company";
import {Sale, SaleProduct, translatePaymentType} from "./SaleModule";
import {DateOptions, DateValidations} from "../utils/date";
import ChartGroup from "../components/ChartGroup";
import {GroupsConfig} from "../components/ChartGroup/groups";
import {ChartType, LineChartConfig, PieChartConfig} from "../components/ModuleChart/charts";
import {number} from "yup";

export interface CargoMap {
    id: number;
    outDate: string;
    driver: Client;
    plate: string;
}

export interface PurchaseProduct {
    id: number;
    createdAt: string;
    product: Product;
    quantity: number;
    increase: number;
    discount: number;
    paymentType: number;
}

export interface Purchase {
    id: number;
    createdAt: string;
    company: Company;
    seller: Client;
    products: PurchaseProduct[];
    total: number;
    discount: number;
    increase: number;
    paymentType: number;
    cargoMaps: CargoMap[]
}

const ProductBestSellerColors = {
    0: "#007C21",
    1: "#0EAC51",
    2: "#2ECC71",
    4: "#F1C40F",
    5: "#0077C0",
    6: "#19B5FE",
    7: "#E3724B",
    8: "#C3522B",
    9: "#A3320B"
}

export default class PurchaseModule implements Module<Purchase> {
    colors: ModuleColors = {
        module: "#9491ec",
    };
    configuration: GeneralConfiguration = {
        showInModuleList: true,
        showInStory: true,
    };
    dataFlow: IDataflow<Purchase> = DataFlow(this, PurchaseService());
    displayName = "Compras";
    form: ModuleForm<Purchase> = {
        icon: "shopping-cart",
        title: "Compra",
        form: item => {
            const format = Formatter()
            return (
                <>
                    <>
                        <ModuleFormItemsContainer>
                            <ModuleFormItem placeholder={"ID"} value={item.id.toString()}/>
                            <ModuleFormItem placeholder={"Forma de pagamento"} value={translatePaymentType(item.paymentType)}/>
                            {item.company && (
                                <ModuleFormItem placeholder={"Comprador"} value={item.company.name}/>
                            )}
                            {item.seller && (
                                <ModuleFormItem placeholder={"Vendedor"} value={item.seller.name}/>
                            )}
                            {item.discount > 0 && (
                                <ModuleFormItem placeholder={"Desconto"} value={`R$ ${format.Format(item.discount)}`}/>
                            )}
                            {item.increase > 0 && (
                                <ModuleFormItem placeholder={"Incremento"} value={`R$ ${format.Format(item.increase)}`}/>
                            )}
                            <ModuleFormItem placeholder={"Data"} value={moment(new Date(item.createdAt)).format("L")}/>
                        </ModuleFormItemsContainer>
                        {item.cargoMaps && (
                            <>
                                <ModuleFormItemTitle>Rastreamento</ModuleFormItemTitle>
                                {item.cargoMaps.map(cargo => (
                                    <ModuleFormItemsContainer>
                                        <ModuleFormItem placeholder={"Motorista"} value={`${cargo.driver.name}`}/>
                                        <ModuleFormItem placeholder={"Saída"} value={moment(cargo.outDate).format("L")}/>
                                        <ModuleFormItem placeholder={"Placa do veículo"} value={cargo.plate}/>
                                    </ModuleFormItemsContainer>
                                ))}
                            </>
                        )}
                    </>
                    <ModuleFormTitle>Produtos:</ModuleFormTitle>
                    <ModuleFormListContainer>
                        <FlatList data={item.products} renderItem={(item) => {
                            return (
                                <ModuleListItemContainer>
                                    <ModuleListItemHeader>
                                        <Text style={{fontWeight: "bold"}}>{item.item.product.id}</Text>
                                        <Text style={{marginLeft: 5}}>{item.item.product.description}</Text>
                                        <Text style={{marginLeft: 'auto'}}>{item.item.quantity} x R$ {Formatter().Format(item.item.product.price)}</Text>
                                    </ModuleListItemHeader>
                                </ModuleListItemContainer>
                            )
                        }} keyExtractor={(item, key) => key.toString()}/>
                    </ModuleFormListContainer>
                    <ModuleFormItem placeholder={"Total"} value={`R$ ${format.Format(item.total)}`}/>
                </>
            )
        }
    };
    icon: ModuleIcons = {
        module: "shopping-cart",
        story: "shopping-cart"
    };
    moduleList: ModuleList<Purchase> = {
        filterList: [],
        orderFilterList: [],
        renderItem(info: ListRenderItemInfo<Purchase>): React.ReactElement {
            const deltaItem = info.item.increase - info.item.discount;

            return (
                <ModuleListItemContainer style={{borderBottomWidth: 1, borderBottomColor: '#CACACA'}}>
                    <ModuleListItemHeader>
                        <Text style={{fontWeight: "bold"}}>{info.item.id}</Text>
                        <Text style={{marginLeft: 5, maxWidth: "50%"}} numberOfLines={1}>{info.item.seller.name}</Text>
                        <Text style={{marginLeft: 'auto', marginRight: 5}}>R$ {Formatter().Format(info.item.total)}</Text>
                    </ModuleListItemHeader>
                    <ModuleListItemFooter>
                        <Text>{moment(new Date(info.item.createdAt)).format("L")}</Text>
                        {(deltaItem != 0) && (
                            <>
                                <Text style={{marginLeft: 'auto', color: deltaItem > 0 ? 'green' : 'red'}}> R$ {deltaItem > 0 && '+'}{Formatter().Format(deltaItem)}</Text>
                            </>
                        )}

                    </ModuleListItemFooter>
                </ModuleListItemContainer>
            )
        },
        emptyContent: (
            <EmptyContentWrapper>
                <EmptyContentContainer>
                    <EmptyContentTitle>Não foi encontrado nenhum item</EmptyContentTitle>
                </EmptyContentContainer>
            </EmptyContentWrapper>
        ),
    };
    multiCompany = true;
    name = "PURCHASE";
    statistics: ModuleStatistics = {
        header: () => {
            const dateOptions: DateOptions[] = [
                {
                    text: "Hoje",
                    validate: DateValidations.validation({type: DateValidations.Today})
                },
                {
                    text: "7 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 7})
                },
                {
                    text: "15 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 15})
                },
                {
                    text: "30 dias",
                    validate: DateValidations.validation({type: DateValidations.Days, payload: 30})
                },
            ]

            const purchaseResumeLinearCharts: LineChartConfig<Purchase> = {
                type: ChartType.LINE,
                dataSelector: (item) => item.total,
                title: "Compras resumida",
                labelSelector: (item => `${moment(item.createdAt).format("DD/MM")}`),
                showNotFoundText: true,
                chartProps: {
                    bezier: true,
                    yAxisInterval: 1,
                    formatYLabel: value => Formatter().Format(parseFloat(value))
                },
                dateSelector: (item) => new Date(item.createdAt),
                preLabelSelector: items => items.filter((int, i) => i == 0 || i % 2 == 0 || i == items.length - 1),
                organizeItems: (items => items),
            }

            const bestSellingProducts: PieChartConfig<Purchase> = {
                type: ChartType.PIE,
                title: "Produtos mais vendidos",
                dataSelector: items => {
                    const products: PurchaseProduct[] = [];
                    items.forEach(pruchase => {
                        if (!pruchase.products)
                            return
                        pruchase.products.forEach(product => {
                            const findProduct = products.find(item => item.product.id === product.product.id);
                            if (!findProduct) {
                                products.push(product)
                                return
                            }
                            const index = products.indexOf(findProduct)
                            products[index] = {...products[index], quantity: products[index].quantity + product.quantity}
                        })
                    })
                    products.sort((a, b) => b.quantity - a.quantity)
                    return products.slice(0, 10).map((item, index) => {
                        return {
                            population: item.quantity,
                            name: item.product.description,
                            legendFontSize: 12,
                            legendFontColor: "#3366FF",
                            color: ProductBestSellerColors[index]
                        }
                    })
                },
                chartProps: {
                    accessor: "population"
                },
                organizeItems: items => items,
                dataGrouping: items => items,
                dateSelector: item => new Date(item.createdAt)
            }

            const chartsConfig: GroupsConfig = {
                charts: [
                    {
                        module: this.name,
                        config: purchaseResumeLinearCharts
                    },
                    {
                        module: this.name,
                        config: bestSellingProducts,
                    }
                ],
                options: dateOptions,
                showDateSelector: true,
            }

            return (
                <HeaderContainer>
                    <HeaderTitle>{this.displayName}</HeaderTitle>
                    <ChartGroup config={chartsConfig}/>
                </HeaderContainer>
            )
        }
    };
    titleIcon = "shopping-cart";

}
