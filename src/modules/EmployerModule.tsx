import React from 'react';
import {
    GeneralConfiguration,
    Module,
    ModuleColors,
    ModuleForm,
    ModuleIcons,
    ModuleList,
    ModuleStatistics
} from "../lib/Module";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import {EmployerService} from "../services/EmployerService";
import {ListRenderItemInfo, Text, View} from "react-native";
import {
    ChartTitle,
    EmptyContentContainer,
    EmptyContentTitle,
    EmptyContentWrapper, HeaderContainer, HeaderTitle,
    ModuleFormItem,
    ModuleFormItemsContainer, ModuleFormItemTitle
} from "./styles";
import {ModuleListItemContainer, ModuleListItemFooter, ModuleListItemHeader} from "../screens/ModuleList/style";
import {Address} from "./ClientsModule";
import ChartGroup from "../components/ChartGroup";
import SimpleList from "../components/SimpleList";
import ReportList from "../components/ReportList";
import {BestSellersReport} from "../reports/BestSellersReport";
import {Formatter} from "../utils/formatter";

function translateEmployerType(type: number): string{
    switch (type) {
        case 0:
            return "Funcionário"
        case 1:
            return "Vendedor"
        default:
            return ""
    }
}

export interface Employer extends Address{
    id: number;
    name: string;
    cpf: string;
    rg: string;
    type: number;
}

export class EmployerModule implements Module<Employer> {
    titleIcon = "people";
    colors:ModuleColors = {
        module: "#828982"
    }
    moduleList: ModuleList<Employer> = {
        filterList: [],
        orderFilterList: [],
        emptyContent: (
            <EmptyContentWrapper>
                <EmptyContentContainer>
                    <EmptyContentTitle>Não foi encontrado nenhum item</EmptyContentTitle>
                </EmptyContentContainer>
            </EmptyContentWrapper>
        ),
        renderItem(info: ListRenderItemInfo<Employer>): React.ReactElement {
            return (
                <ModuleListItemContainer style={{borderBottomWidth: 1, borderBottomColor: '#CACACA'}}>
                    <ModuleListItemHeader>
                        <Text style={{fontWeight: "bold"}}>{info.item.id}</Text>
                        <Text style={{marginLeft: 5}}>{info.item.name}</Text>
                    </ModuleListItemHeader>
                    <ModuleListItemFooter>
                        <Text>{translateEmployerType(info.item.type)}</Text>
                    </ModuleListItemFooter>
                </ModuleListItemContainer>
            )
        }
    }
    form: ModuleForm<Employer> = {
        icon: "people",
        title: "Funcionários",
        form: item => {
            return (
                <>
                    <ModuleFormItemsContainer>
                        <ModuleFormItem placeholder={"ID"} value={`${item.id}`}/>
                        <ModuleFormItem placeholder={"Nome"} value={item.name}/>
                        <ModuleFormItem placeholder={"Cargo"} value={translateEmployerType(item.type)}/>
                        {item.cpf !== "" && (
                            <ModuleFormItem placeholder={"CPF"} value={item.cpf}/>
                        )}
                        {item.rg !== "" && (
                            <ModuleFormItem placeholder={"RG"} value={item.rg}/>
                        )}
                    </ModuleFormItemsContainer>
                    <ModuleFormItemTitle>Endereço</ModuleFormItemTitle>
                    <ModuleFormItemsContainer>
                        <ModuleFormItem placeholder={"Rua"} value={item.street}/>
                        <ModuleFormItem placeholder={"Número"} value={item.number}/>
                        {item.complement !== "" && (
                            <ModuleFormItem placeholder={"Complemento"} value={item.complement}/>
                        )}
                        {item.neighborhood !== "" && (
                            <ModuleFormItem placeholder={"Bairo"} value={item.neighborhood}/>
                        )}
                        <ModuleFormItem placeholder={"CEP"} value={item.postalCode}/>
                        <ModuleFormItem placeholder={"Cidade"} value={item.city}/>
                        {item.city !== "" && (
                            <ModuleFormItem placeholder={"Referência"} value={item.reference}/>
                        )}
                    </ModuleFormItemsContainer>
                </>
            )
        }
    }
    configuration: GeneralConfiguration = {
        showInModuleList: true,
        showInStory: true,
    };
    dataFlow: IDataflow<Employer> = DataFlow(this, EmployerService());
    displayName = "Funcionários";
    icon: ModuleIcons = {
        module: "people",
    };
    statistics: ModuleStatistics = {
        header: () => {

            const formatter = Formatter()
            const bestSellersRender = (item: BestSellersReport, index: number) => {
                return (
                    <ModuleListItemContainer>
                        <ModuleListItemHeader>
                            <Text style={{fontWeight: "bold"}}>{index + 1}</Text>
                            <Text style={{marginLeft: 5}}>{item.seller.name}</Text>
                        </ModuleListItemHeader>
                        <View style={{flexDirection: "column"}}>
                            <Text>Total vendido: R$ {formatter.Format(item.salesTotal)}</Text>
                            <Text>Numero de vendas: {item.salesCount}</Text>
                        </View>
                    </ModuleListItemContainer>
                )
            }

            return (
                <HeaderContainer>
                    <HeaderTitle>{this.displayName}</HeaderTitle>
                    <ChartTitle>Os 10 melhores vendedores</ChartTitle>
                    <ReportList reportName={"bestSellers"} render={bestSellersRender}/>
                </HeaderContainer>
            )
        }
    }
    multiCompany = false;
    name = "EMPLOYER";

}
