import React from 'react';
import {
    GeneralConfiguration,
    Module,
    ModuleColors, ModuleForm,
    ModuleIcons,
    ModuleList,
} from "../lib/Module";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import {ListRenderItemInfo, ScrollView, Text} from "react-native";
import ClientService from "../services/ClientService";
import {
    EmptyContentContainer,
    EmptyContentTitle,
    EmptyContentWrapper, ModuleFormItemTitle,
} from "./styles";
import {ModuleListItemContainer, ModuleListItemHeader} from "../screens/ModuleList/style";
import {ModuleFormItem, ModuleFormItemsContainer} from "./styles";
import moment from "moment";

export interface Address {
    street: string;
    number: string;
    complement: string;
    neighborhood: string;
    postalCode: string;
    uf: string;
    city: string;
    reference: string;
}

export interface Client extends Address {
    id: number;
    name: string;
    type: ClientType;
    cpf: string;
    rg: string;
    birthday: string;
    cnpj: string;
    createdAt: string;
    phone1: string;
    phone2: string;
    celular1: string;
    celular2: string;
    email1: string;
    email2: string;
    fatherName: string;
    motherName: string;
    civilState: number;
    schooling: number;
    observation: string;
}

export enum ClientType {
    FISICA = 1,
    JURIDICA
}

export namespace ClientType {
    export function translate(type: ClientType): string {
        switch (type) {
            case ClientType.FISICA:
                return "Pessoa Fisica"
            case ClientType.JURIDICA:
                return "Pessoa Juridica"
        }
    }
}

export default class ClientsModule implements Module<Client> {
    colors: ModuleColors = {
        module: "#9ba047"
    };
    configuration: GeneralConfiguration = {
        showInModuleList: true,
        showInStory: false,
    };
    multiCompany = true
    dataFlow: IDataflow<Client> = DataFlow(this, ClientService());
    displayName = "Clientes";
    titleIcon = "supervisor-account"
    icon: ModuleIcons = {
        module: "supervisor-account",
        story: "supervisor-account"
    };
    moduleList: ModuleList<Client> = {
        filterList: [],
        orderFilterList: [],
        emptyContent: (
            <EmptyContentWrapper>
                <EmptyContentContainer>
                    <EmptyContentTitle>Não foi encontrado nenhum item</EmptyContentTitle>
                </EmptyContentContainer>
            </EmptyContentWrapper>
        ),
        renderItem(info: ListRenderItemInfo<Client>): React.ReactElement {
            return (
                <ModuleListItemContainer style={{borderBottomWidth: 1, borderBottomColor: '#CACACA'}}>
                    <ModuleListItemHeader>
                        <Text style={{fontWeight: "bold"}}>{info.item.id}</Text>
                        <Text style={{marginLeft: 5}}>{info.item.name}</Text>
                    </ModuleListItemHeader>
                </ModuleListItemContainer>
            )
        }
    };

    name: string = "CLIENT";

    form: ModuleForm<Client> = {
        title: "Cliente",
        icon: "supervisor-account",
        form: item => {
            return (
                <ScrollView showsVerticalScrollIndicator={false}>
                    <ModuleFormItemsContainer>
                        <ModuleFormItem placeholder={"ID"} value={item.id.toString()}/>
                        <ModuleFormItem placeholder={"Nome"} value={item.name}/>
                        <ModuleFormItem placeholder={"Tipo"} value={ClientType.translate(item.type)}/>
                        {item.type === 1 && (
                            <>
                                <ModuleFormItem placeholder={"CPF"} value={item.cpf}/>
                                <ModuleFormItem placeholder={"RG"} value={item.rg}/>
                                <ModuleFormItem placeholder={"Data de nascimento"} value={moment(item.birthday).format("L")}/>
                            </>
                        )}
                        {item.type === 2 && (
                            <>
                                <ModuleFormItem placeholder={"CNPJ"} value={item.cnpj}/>
                            </>
                        )}
                        {item.phone1 !== "" && (
                            <ModuleFormItem placeholder={"Telefone"} value={item.phone1}/>
                        )}
                        {item.phone2 !== "" && (
                            <ModuleFormItem placeholder={"Telefone 2"} value={item.phone2}/>
                        )}
                        {item.celular1 !== "" && (
                            <ModuleFormItem placeholder={"Celular"} value={item.celular1}/>
                        )}
                        {item.celular2 !== "" && (
                            <ModuleFormItem placeholder={"Celular 2"} value={item.celular2}/>
                        )}
                        {item.email1 !== "" && (
                            <ModuleFormItem placeholder={"Email"} value={item.email1}/>
                        )}
                        {item.email2 !== "" && (
                            <ModuleFormItem placeholder={"Email 2"} value={item.email2}/>
                        )}
                        {item.fatherName !== "" && (
                            <ModuleFormItem placeholder={"Nome do Pai"} value={item.fatherName}/>
                        )}
                        {item.motherName !== "" && (
                            <ModuleFormItem placeholder={"Nome da Mãe"} value={item.motherName}/>
                        )}
                        {item.schooling !== 0 && (
                            <ModuleFormItem placeholder={"Escolaridade"} value={`${item.schooling}`}/>
                        )}
                        {item.observation !== "" && (
                            <ModuleFormItem placeholder={"Observações"} value={item.observation}/>
                        )}
                    </ModuleFormItemsContainer>
                    <ModuleFormItemTitle>Endereço:</ModuleFormItemTitle>
                    <ModuleFormItemsContainer>
                        <ModuleFormItem placeholder={"Rua"} value={item.street}/>
                        <ModuleFormItem placeholder={"Numero"} value={item.number}/>
                        {item.complement !== "" && (
                            <ModuleFormItem placeholder={"Complemento"} value={item.complement}/>
                        )}
                        <ModuleFormItem placeholder={"Bairo"} value={item.neighborhood}/>
                        <ModuleFormItem placeholder={"Cidade"} value={item.city}/>
                        <ModuleFormItem placeholder={"UF"} value={item.uf}/>
                        <ModuleFormItem placeholder={"CEP"} value={item.postalCode}/>
                        {item.reference !== "" && (
                            <ModuleFormItem placeholder={"Referência"} value={item.reference}/>
                        )}
                    </ModuleFormItemsContainer>
                </ScrollView>
            )
        }
    }


}
