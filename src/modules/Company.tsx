export interface Company {
    id: number;
    socialName: string;
    name: string;
    cnpj: string;
    phone?: string;
    celular?: string;
    email?: string;
    street?: string;
    number?: string;
    complement?: string;
    neighborhood?: string;
    postalCode?: string;
    uf?: string;
    city?: string;
    reference?: string;
}

export const ModuleColors = {
    1: "#fff",
    2: "#fff",
    3: "#fff",
    4: "#fff",
    5: "#fff",
    6: "#fff",
    7: "#fff",
    8: "#fff",
}
