import {GeneralConfiguration, Module, ModuleColors, ModuleIcons, ModuleList} from "../lib/Module";
import {ListRenderItemInfo, View} from "react-native";
import React from "react";
import {EmptyContentContainer, EmptyContentTitle, EmptyContentWrapper} from "./styles";
import {DataFlow, IDataflow} from "../lib/Dataflow";
import {AccountReceivableService} from "../services/accountsService";

export interface Billing {
    id: number;
}

export class BillingModule implements Module<Billing> {

    configuration: GeneralConfiguration = {
        showInModuleList: false,
        showInStory: false,
    }

    dataFlow: IDataflow<Billing> = DataFlow(this, AccountReceivableService())
    colors: ModuleColors = {
        module: "rgba(101, 254, 76, 0.87)",
        story: "rgba(255, 204, 0, 0.4)"
    };
    displayName = "Faturamento";
    icon: ModuleIcons = {
        module: "monetization-on"
    };
    multiCompany = false;
    name = "BILLING";

    moduleList: ModuleList<Billing> = {
        orderFilterList: [],
        filterList: [],
        emptyContent: (
            <EmptyContentWrapper>
                <EmptyContentContainer>
                    <EmptyContentTitle>Não foi encontrado nenhum item</EmptyContentTitle>
                </EmptyContentContainer>
            </EmptyContentWrapper>
        ),
        renderItem(info: ListRenderItemInfo<Billing>): React.ReactElement {
            return <View>

            </View>
        }
    }
}
