import React, {useEffect} from 'react';
import {useReport} from "../../reports/hooks";
import SimpleList from "../SimpleList";

interface ReportListProps<T> {
    reportName: string;
    render: (item:T, index?: number) => React.ReactElement;
}

const ReportList = <T,>(props: ReportListProps<T>) => {
    const { reportName, render } = props;
    const reportData = useReport<T>(reportName)
    return (
        <SimpleList data={reportData} render={(item:T, index) => render(item, index)}/>
    )
}

export default ReportList
