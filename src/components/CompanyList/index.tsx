import React, { useState } from 'react';
import {Company} from "../../modules/Company";
import {CompanyContainer, CompanyItemMarkerIcon} from "./style";
import {
    CompanyItemContainer,
    CompanyItemDescription,
    CompanyItemIcon,
    CompanyItemTitle,
    CompanyItemButton
} from "./style";
import Icon from "react-native-vector-icons/MaterialIcons";
import {ScrollView} from "react-native";

export interface CompanyListProps {
    company: CompanyItem[]
    onCompanySelected: (company: CompanyItem) => void;
}

export interface CompanyItem extends Company {
    selected?: boolean
}

const CompanyList = (props: CompanyListProps) => {

    const handlerLongPress = (company: CompanyItem) => {
        props.onCompanySelected(company)
    }

    return (
        <ScrollView horizontal={false}>
        <CompanyContainer>
            {props.company.map((company, index) => (
                <CompanyItemButton key={index} delayLongPress={400} onLongPress={() => handlerLongPress(company)}>
                <CompanyItemContainer>
                    {company.selected && (
                        <CompanyItemMarkerIcon>
                            <Icon name={"check-circle"} size={26} color={"#18bf29"}/>
                        </CompanyItemMarkerIcon>
                    )}
                    <CompanyItemIcon><Icon name={"location-city"} size={48} color={"#3366FF"}/></CompanyItemIcon>
                    <CompanyItemTitle>{company.id} {company.name}</CompanyItemTitle>
                    <CompanyItemDescription>CNPJ: {company.cnpj}</CompanyItemDescription>
                    <CompanyItemDescription>{company.city}</CompanyItemDescription>
                </CompanyItemContainer>
                </CompanyItemButton>
            ))}
        </CompanyContainer>
        </ScrollView>
    )

}


export default CompanyList;
