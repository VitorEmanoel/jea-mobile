import styled from 'styled-components/native';

export const CompanyContainer = styled.View`
  flex: 1;
  width: 100%;
  padding: 10px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  margin-top: 20px;
`;

export const CompanyItemButton = styled.TouchableNativeFeedback`

`

export const CompanyItemContainer = styled.View`
   position: relative;
   background-color: white;
   flex-direction: column;
   padding: 10px;
   margin: 10px;
   elevation: 1;
   border-radius: 20px;
   width: 150px;
   height: 150px;
`;

export const CompanyItemIcon = styled.Text`
  margin-top: 10px;
  text-align: center;
`;

export const CompanyItemTitle = styled.Text`
  color: #6B6B6B;
  margin-top: 10px;
  font-weight: bold;
  font-size: 12px;
  text-align: center;
`;

export const CompanyItemDescription = styled.Text`
  font-size: 10px;
  margin-top: 8px;
  color: #6B6B6B;
  text-align: center;
`;

export const CompanyItemMarkerIcon = styled.Text`
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  right: 0;
  padding: 5px;
`;
