import React from 'react'

import Icon from "react-native-vector-icons/MaterialIcons"
import { StoryItemWrapper, StoryItemContainer, StoryItemTitle} from "./style";
import {Module} from "../../lib/Module";
import {useSelector} from "react-redux";
import {RootState} from "../../store/types";
import {TouchableNativeFeedback} from "react-native";

interface StoryItemProps {
    onItemClick?: (item: Module<any>) => void;
    item: Module<any>
}

const StoryItem = (props: StoryItemProps) => {
    const icon = props.item.icon.story ? props.item.icon.story : props.item.icon.module;

    const dashboard = useSelector((state: RootState) => state.dashboard)

    return (
        <TouchableNativeFeedback delayLongPress={1000} onPress={() => props.onItemClick && props.onItemClick(props.item)}>
            <StoryItemWrapper>
                <>
                    <StoryItemContainer>
                        <Icon name={icon} size={24} color={dashboard.selected === props.item.name ? "#bedcfe" : "white"}/>
                    </StoryItemContainer>
                    <StoryItemTitle>{props.item.displayName}</StoryItemTitle>
                </>
            </StoryItemWrapper>
        </TouchableNativeFeedback>
    )
};

export default StoryItem;


