import styled from "styled-components/native";

export const StoryItemWrapper = styled.View`
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 0 15px;
`;

export const StoryItemContainer = styled.View`
  padding: 5px;
`;

export const StoryItemTitle = styled.Text`
  color: white;
  margin-top: 5px;
  font-size: 12px;
`
