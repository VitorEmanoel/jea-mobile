import React, {useEffect, useState} from "react";
import {
    CompaniesContainerInfo,
    ContainerTitle,
    DashboardContainer
} from "./styles";
import * as ModuleActions from "../../lib/Module/actions";
import {useDispatch, useSelector} from "react-redux";
import { bindActionCreators } from "redux";
import {RootState} from "../../store/types";
import {ModulesStates} from "../../store/baseTypes";
import {CompanyPerfomance} from "../../modules/PerfomanceModule";
import {CompanyPerfomanceGeneral, CompanyPerfomanceInfo} from "./components";
import {HeaderTitle} from "../../modules/styles";
import Table from "../Table";
import {Text, View} from "react-native";
import {useReport} from "../../reports/hooks";
import {PerformanceReport} from "../../reports/PerformanceReport";
interface PerfomanceDashboardProps {
    module: string;
}

const PerfomanceDashboard = (props:PerfomanceDashboardProps) => {
    const { module } = props;

    const companies = useReport<PerformanceReport>("performance")

    const register = useSelector((state: RootState) => state.register.register)


    const dispatch = useDispatch();
    const moduleActions = bindActionCreators(ModuleActions, dispatch)

    if (!companies)
        return (
            <></>
        )
    return (
        <DashboardContainer>
            <HeaderTitle>Performance</HeaderTitle>
            <CompaniesContainerInfo>
                {companies.map((item, index) => <CompanyPerfomanceInfo key={index.toString()} company={item}/>)}
            </CompaniesContainerInfo>

            <CompanyPerfomanceGeneral companies={companies}/>
        </DashboardContainer>
    )
}

export default PerfomanceDashboard;
