import React from "react";
import {Company} from "../../modules/Company";
import {
    CompanyPerfomanceBody,
    CompanyPerfomanceCity,
    CompanyPerfomanceColumn,
    CompanyPerfomanceContainer,
    CompanyPerfomanceGeneralBody,
    CompanyPerfomanceGeneralContainer,
    CompanyPerfomanceGeneralItems, CompanyPerfomanceGeneralItemsContent,
    CompanyPerfomanceGeneralItemsTitle,
    CompanyPerfomanceGeneralMainItem,
    CompanyPerfomanceGeneralTitle,
    CompanyPerfomanceHeader,
    CompanyPerfomanceItemContainer,
    CompanyPerfomanceItemTitle,
    CompanyPerfomanceRow,
    CompanyPerfomanceTitle,
    CompanyPerfomanceValueContainer,
    CompanyPerfomanceValueItem,
    CompanyPerfomanceValueSymbol, CompanyPerformanceGeneralItemsContainer
} from "./styles";
import {CompanyPerfomance} from "../../modules/PerfomanceModule";
import {HeaderTitle} from "../../modules/styles";
import {Formatter} from "../../utils/formatter";
import {PerformanceReport} from "../../reports/PerformanceReport";

export interface CompanyPerfomanceProps {
    company: PerformanceReport;
}

export const CompanyPerfomanceInfo = (props: CompanyPerfomanceProps) => {
    const { company } = props;
    return (
        <CompanyPerfomanceContainer>
            <CompanyPerfomanceHeader>
                <CompanyPerfomanceTitle>
                    {company.company.id} - {company.company.name}
                </CompanyPerfomanceTitle>
                <CompanyPerfomanceCity>
                    {company.company.city}
                </CompanyPerfomanceCity>
            </CompanyPerfomanceHeader>
            <CompanyPerfomanceBody>
                <CompanyPerfomanceRow>
                    <CompanyPerfomanceColumn>
                        <CompanyPerfomaceItem title={"Total de Vendas"} value={company.salesTotal}/>
                    </CompanyPerfomanceColumn>
                    <CompanyPerfomanceColumn>
                        <CompanyPerfomaceItem title={"Total de Descontos"} value={company.salesDiscountTotal}/>
                    </CompanyPerfomanceColumn>
                </CompanyPerfomanceRow>
                <CompanyPerfomanceRow>
                    <CompanyPerfomanceColumn>
                        <CompanyPerfomaceItem title={"Total de Vendas Liq."} value={company.salesTotal}/>
                    </CompanyPerfomanceColumn>
                    <CompanyPerfomanceColumn>
                        <CompanyPerfomaceItem title={"Total de Pagamentos"} value={company.accountsPayableTotal}/>
                    </CompanyPerfomanceColumn>
                </CompanyPerfomanceRow>
                <CompanyPerfomanceRow>
                    <CompanyPerfomanceRow>
                        <CompanyPerfomaceItem title={"Total de Lucro Líquido"} value={company.salesTotal}/>
                    </CompanyPerfomanceRow>
                </CompanyPerfomanceRow>
            </CompanyPerfomanceBody>
        </CompanyPerfomanceContainer>
    )
}


export interface CompanyPerfomanceItemProps {
    title: string;
    value: number;
}

const CompanyPerfomaceItem = (props: CompanyPerfomanceItemProps) => {
    const { title, value } = props;

    const formatter = Formatter()
    return (
        <CompanyPerfomanceItemContainer>
            <CompanyPerfomanceItemTitle>{title}</CompanyPerfomanceItemTitle>
            <CompanyPerfomanceValueContainer>
                <CompanyPerfomanceValueSymbol>R$ </CompanyPerfomanceValueSymbol>
                <CompanyPerfomanceValueItem>{formatter.Format(value)}</CompanyPerfomanceValueItem>
            </CompanyPerfomanceValueContainer>
        </CompanyPerfomanceItemContainer>
    )
}


export interface CompanyPerfomanceGeneralProps {
    companies: PerformanceReport[];
}

export const CompanyPerfomanceGeneral = (props: CompanyPerfomanceGeneralProps) => {

    const { companies } = props

    const format = Formatter()
    let totalProfit = 0;
    let totalOutgoing = 0;
    companies.forEach(item => {
        totalProfit += item.salesTotal;
        totalOutgoing += item.salesTotal;
    })
    return (
        <CompanyPerfomanceGeneralContainer>
            <CompanyPerfomanceGeneralTitle>Resumo de Vendas</CompanyPerfomanceGeneralTitle>
            <CompanyPerfomanceGeneralBody>
                <CompanyPerfomanceGeneralMainItem>
                    <CompanyPerfomanceGeneralItemsTitle style={{fontWeight: "bold"}}>Empresas</CompanyPerfomanceGeneralItemsTitle>
                    {companies.map((item, index) => <CompanyPerformanceGeneralItemsContainer><CompanyPerfomanceGeneralItemsContent key={index.toString()}>{item.company.id} - {item.company.name}</CompanyPerfomanceGeneralItemsContent><CompanyPerfomanceGeneralItemsContent>{item.company.city}</CompanyPerfomanceGeneralItemsContent></CompanyPerformanceGeneralItemsContainer>)}
                </CompanyPerfomanceGeneralMainItem>
                <CompanyPerfomanceGeneralItems >
                    <CompanyPerfomanceGeneralItemsTitle style={{fontWeight: "bold"}}>Despesas</CompanyPerfomanceGeneralItemsTitle>
                    {companies.map((item, index) => <CompanyPerfomanceGeneralItemsContent key={index.toString()}>R$ {format.Format(item.accountsPayableTotal)}</CompanyPerfomanceGeneralItemsContent>)}
                </CompanyPerfomanceGeneralItems>
                <CompanyPerfomanceGeneralItems style={{borderRightWidth: 0}}>
                    <CompanyPerfomanceGeneralItemsTitle style={{fontWeight: "bold"}}>Lucro Liq.</CompanyPerfomanceGeneralItemsTitle>
                    {companies.map((item, index) => <CompanyPerfomanceGeneralItemsContent key={index.toString()}>R$ {format.Format(item.salesTotal)}</CompanyPerfomanceGeneralItemsContent>)}
                </CompanyPerfomanceGeneralItems>
            </CompanyPerfomanceGeneralBody>
            <CompanyPerfomanceGeneralBody style={{borderTopWidth: 1, borderTopColor: '#3366FF'}}>
                <CompanyPerfomanceGeneralMainItem>
                    <CompanyPerfomanceGeneralItemsTitle style={{fontWeight: 'bold'}}>Total</CompanyPerfomanceGeneralItemsTitle>
                </CompanyPerfomanceGeneralMainItem>
                <CompanyPerfomanceGeneralItems>
                    <CompanyPerfomanceGeneralItemsTitle>R$ {format.Format(totalOutgoing)}</CompanyPerfomanceGeneralItemsTitle>
                </CompanyPerfomanceGeneralItems>
                <CompanyPerfomanceGeneralItems>
                    <CompanyPerfomanceGeneralItemsTitle >R$ {format.Format(totalProfit)}</CompanyPerfomanceGeneralItemsTitle>
                </CompanyPerfomanceGeneralItems>
            </CompanyPerfomanceGeneralBody>
        </CompanyPerfomanceGeneralContainer>
    )
};
