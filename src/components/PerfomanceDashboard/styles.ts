import styled from "styled-components/native";

const tableColor = "#3366FF";

export const DashboardContainer = styled.View`
  flex: 1;
  flex-direction: column;
  padding: 10px;
`;

export const CompaniesContainerInfo = styled.View`
  margin-top: 15px;
`;

export const ContainerTitle = styled.Text`
  font-size: 18px;
  color: ${tableColor};
  text-align: center;
  margin: 10px 0;
`;

export const CompanyPerfomanceContainer = styled.View`
  background-color: white;
  border-radius: 5px;
  elevation: 1.5;
  flex-direction: column;
  padding: 10px;
  margin: 10px 0;
`;

export const CompanyPerfomanceHeader = styled.View`
  flex-direction: row;
`

export const CompanyPerfomanceTitle = styled.Text`
  font-size: 17px;
  color: ${tableColor};
`;

export const CompanyPerfomanceCity = styled.Text`
  font-size: 17px;
  color: #6B6B6B;
  margin-left: auto;
  margin-right: 5px;
`;

export const CompanyPerfomanceBody = styled.View`
  flex-direction: column;
`;

export const CompanyPerfomanceRow = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  max-width: 100%;
  align-items: center;
  justify-content: center;
`

export const CompanyPerfomanceColumn = styled.View`
  flex: 1;
  padding: 10px;
  flex-direction: column;
`

export const CompanyPerfomanceItemContainer = styled.View`
  flex-direction: column;
`;

export const CompanyPerfomanceItemTitle = styled.Text`
  font-size: 14px;
  color: #6B6B6B;
  font-weight: bold;
  text-align: center;
  margin-bottom: 5px;
`;

export const CompanyPerfomanceValueContainer = styled.View`
  flex-direction: row;
`;

export const CompanyPerfomanceValueItem = styled.Text`
  font-size: 14px;
  color: #6b6b6b;
  margin-left: auto;
  margin-right: 5px;
`

export const CompanyPerfomanceValueSymbol = styled.Text`
  font-size: 14px;
  color: #6b6b6b;
`

export const CompanyPerfomanceGeneralContainer = styled.View`
  flex: 1;
  border: 1px solid ${tableColor};
  border-radius: 5px;
  margin-top: 40px;
  flex-direction: column;
`;

export const CompanyPerfomanceGeneralTitle = styled.Text`
  color: #6B6B6B;
  font-size: 16px;
  font-weight: bold;
  width: 100%;;
  padding: 10px 0;
  text-align: center;
  border-bottom-width: 1px;
  border-bottom-color: ${tableColor};
`

export const CompanyPerfomanceGeneralBody = styled.View`
  flex: 1;
  flex-direction: row;
`;

export const CompanyPerfomanceGeneralMainItem = styled.View`
  flex: 2;
  flex-direction: column;
  border-right-width: 1px;
  border-right-color: ${tableColor};
`

export const CompanyPerfomanceGeneralItems = styled.View`
  flex: 1;
  flex-direction: column;
  border-right-width: 1px;
  border-right-color: ${tableColor};
`;

export const CompanyPerfomanceGeneralItemsContent = styled.Text`
  color: #6B6B6B;
  font-size: 13px;
  padding: 5px;
`;

export const CompanyPerformanceGeneralItemsContainer = styled.View`
  flex-direction: column;
`

export const CompanyPerfomanceGeneralItemsTitle = styled.Text`
  font-size: 14px;
  text-align: center;
  padding: 2.5px 0;
  color: #6B6B6B;
`;

