import styled from 'styled-components/native';

interface PickerContainerProps {
    background: string
}

export const PickerContainer = styled.View`
  flex-direction: row;
  padding: 6px 10px;
  border-radius: 5px;
  align-items: center;
  position: relative;
  background-color: ${(props: PickerContainerProps) => props.background};
  width: auto;
`;

interface PickerTextProps {
    fontColor?:string;
}

export const PickerText = styled.Text`
  color: ${(props: PickerTextProps) => props.fontColor ? props.fontColor : 'white'};
`;

export const PickerIcon = styled.Text`
  margin-left: 10px;
  align-items: center;
`

export const PickerItems = styled.ScrollView`
  position: absolute;
  top: 30px;
  right: 0;
  padding: 10px;
  border-radius: 10px;
  background-color: white;
  z-index: 9999;
  elevation: 3;
`;

interface PickerItemProps {
    last: boolean
}

export const PickerItem = styled.TouchableNativeFeedback`
`

export const PickerItemText = styled.Text`
  color: #6B6B6B;
  border-bottom-color: rgba(0,0,0,0.5);
  border-bottom-width: ${(props: PickerItemProps) => props.last ? "0" : "1px" };
  padding: 5px 0;
`;

