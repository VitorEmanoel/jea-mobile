import React, {useState} from 'react';
import {PickerContainer, PickerIcon, PickerText} from "./styles";
import { TouchableNativeFeedback} from "react-native";
import Icon  from "react-native-vector-icons/MaterialIcons"

export interface PickerColors {
    background: string;
    font?: string;
    icon?: string;

}

interface PickerProps {
    onOpenItems: () => void;
    placeholder: string;
    colors: PickerColors
}

const Picker = (props: PickerProps) => {

    const handlerOpenList = () => {
        props.onOpenItems()
    }

    return (
        <TouchableNativeFeedback onPress={handlerOpenList}>
            <PickerContainer background={props.colors.background}>
                <PickerText fontColor={props.colors.font ? props.colors.font : 'white'}>{props.placeholder}</PickerText>
                <PickerIcon><Icon name={"keyboard-arrow-down"} size={18} color={props.colors.icon ? props.colors.icon : 'white'}/></PickerIcon>
            </PickerContainer>
        </TouchableNativeFeedback>
    )

}

export default Picker;
