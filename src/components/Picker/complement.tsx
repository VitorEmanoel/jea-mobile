import React from 'react';
import {PickerItem, PickerItems, PickerItemText} from "./styles";
import {ScrollView} from "react-native";

interface PickerComplementProps {
    items: string[];
    show: boolean;
    onItemSelected: (item: string) => void;
}

const PickerComplement = (props: PickerComplementProps) => {
    return (
        <>
            {props.show && (
                <PickerItems>
                    {props.items.map((item, index) => <PickerItem onPress={() => props.onItemSelected(item)} key={`${item}-index`}><PickerItemText numberOfLines={1} last={index === props.items.length - 1}>{item}</PickerItemText></PickerItem>)}
                </PickerItems>
            )}
        </>
    )
};

export default PickerComplement
