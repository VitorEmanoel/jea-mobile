import styled from "styled-components/native";

export const ModuleLineChartContainer = styled.View`
  flex-direction: column;
  padding: 5px;
  z-index: 1;
  height: auto;
`

export const ModuleNotFoundDataContainer = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
  margin: 10px 0;
`;

export const ModuleNotFoundData = styled.Text`
  font-size: 18px;
`;

export const ModuleChartTitle = styled.Text`
  font-size: 18px;
  color: #6B6B6B;
  margin: 10px 0 5px;
  text-align: center;
`;

export const ModuleLineChartHeader = styled.View`
  margin: 5px 0;
  flex-direction: row;
  align-items: center;
`

export const ModuleLineChartTitle = styled.Text`
  font-size: 16px;
  color: #6B6B6B;
`;

export const DetailsContainer = styled.View`
  margin-top: 10px;
  position: relative;
  width: 100%;
  padding: 10px;
  flex-direction: column;
  border-radius: 10px;
  background-color: #fff;
  elevation: 3;
  min-height: 40px;
`;

export const DetailsClose = styled.TouchableNativeFeedback`
`;

export const DetailsIcon = styled.Text`
  padding: 5px;
  position: absolute;
  top: -2px;
  right: -2px;
  align-items: center;
`;
