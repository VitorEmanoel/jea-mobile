import React, {useEffect, useState} from 'react';
import {Module, ModuleState} from "../../lib/Module";
import {ActivityIndicator, Dimensions, ScrollView, View} from "react-native";
import {useSelector} from "react-redux";
import {ChartConfig as KChartConfig} from "react-native-chart-kit";
import {
    ModuleChartTitle,
    ModuleLineChartContainer, ModuleNotFoundData, ModuleNotFoundDataContainer,
} from "./styles";
import {ChartConfig, getChart, processItem} from "./charts";
import {ModulesStates} from "../../store/baseTypes";
import {useCompany} from "../../store/Company/hooks";
import {ModulesMapped} from "../../modules";

interface ModuleChartProps<T> {
    module: string;
    config: ChartConfig<T>
    height?: number;
    width?: number;
}

const ModuleChart = <T,>(props: ModuleChartProps<T>) => {

    const Chart = getChart(props.config.type)
    const module = ModulesMapped()[props.module];
    const moduleState: ModuleState<T> = useCompany(useSelector((state:ModulesStates) => state[props.module]), module.multiCompany)
    const chartWidth = Dimensions.get("window").width;
    const chartHeight = props.height ? props.height : 220;

    const chartConfig: KChartConfig = {
        backgroundGradientFrom: "#fff",
        backgroundGradientTo: "#fff",
        labelColor: () => "#6B6B6B",
        backgroundColor: "#fff",
        color: () => `#36f`,
        strokeWidth: 2,
        barPercentage: 0.5,
    };

    const organizedDatas = props.config.organizeItems(moduleState.rows)

    const groupedData = props.config.dataGrouping ? props.config.dataGrouping(organizedDatas) : organizedDatas

    const width = groupedData.length * 50;
    return (
        <>
            {moduleState.loading && (
                <View style={{flex: 1, justifyContent: 'center', alignItems: "center"}}>
                    <ActivityIndicator color={"#3366FF"} size={"large"}/>
                </View>
            )}
            {(groupedData && !moduleState.loading && groupedData.length > 0 ) ? (
            <ModuleLineChartContainer>
                {props.config.title && (
                    <ModuleChartTitle>{props.config.title}</ModuleChartTitle>
                )}
                <ScrollView showsHorizontalScrollIndicator={false} contentContainerStyle={{alignItems: "center", padding: 10, zIndex: 1}} style={{zIndex: 1}} horizontal={true}>
                    <Chart
                        {...props.config.chartProps}
                        data={processItem(props.config, groupedData)}
                        width={width < chartWidth ? chartWidth : width}
                        height={chartHeight}
                        chartConfig={props.config.chartConfig ? props.config.chartConfig : chartConfig}
                        style={{alignItems: "center", zIndex: 1}}
                    />
                </ScrollView>
            </ModuleLineChartContainer>
            ) : (
                <>
                    {(props.config.showNotFoundText && !moduleState.loading) && (
                        <ModuleNotFoundDataContainer>
                            <ModuleNotFoundData>Sem informações</ModuleNotFoundData>
                        </ModuleNotFoundDataContainer>
                    )}
                </>
            )}
        </>
    )

}

export default ModuleChart
