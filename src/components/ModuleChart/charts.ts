import {
    LineChart,
    BarChart,
    ProgressChart,
    StackedBarChart,
    PieChart,
    ContributionGraph,
    ChartData,
    LineChartData,
    StackedBarChartData,
    ProgressChartData,
    LineChartProps,
    BarChartProps,
    ProgressChartProps,
    StackedBarChartProps, PieChartProps, ContributionGraphProps,
    ChartConfig as KChartConfig
} from "react-native-chart-kit";

export enum ChartType {
    LINE,
    BAR,
    PROGRESS,
    STACKED,
    PIE,
    CONTRIBUTION
}

export interface BaseChartConfig<T> {
    organizeItems: (items: T[]) => T[]
    dateSelector: (item: T) => Date;
    dataGrouping?: (items: T[]) => T[]
    chartConfig?: KChartConfig
    showNotFoundText?: boolean;
    title?: string;
}

export interface LineChartConfig<T> extends BaseChartConfig<T>{
    type: typeof ChartType.LINE
    dataSelector: (item: T) => number;
    preLabelSelector?: (items: T[]) => T[]
    labelSelector: (item: T) => string;
    chartProps?: LineChartProps
}

export interface BarChartConfig<T> extends BaseChartConfig<T> {
    type: typeof ChartType.BAR
    dataSelector: (item: T) => number;
    preLabelSelector?: (items: T[]) => T[]
    labelSelector: (item: T) => string;
    chartProps?: BarChartProps
}

export interface ProgressChartConfig<T> extends BaseChartConfig<T> {
    type: typeof ChartType.PROGRESS
    dataSelector: (item: T) => number;
    preLabelSelector?: (items: T[]) => T[]
    labelSelector: (item: T) => string;
    chartProps?: ProgressChartProps
}

export interface StackedChartConfig<T> extends BaseChartConfig<T> {
    type: typeof ChartType.STACKED
    dataSelector: (item: T) => number[];
    preLabelSelector?: (items: T[]) => T[]
    labelSelector: (item: T) => string;
    legendSelector: (item: T) => string;
    colorSelector: (item: T) => string;
    chartProps?: StackedBarChartProps
}

export interface PieChartConfig<T> extends BaseChartConfig<T> {
    type: typeof ChartType.PIE
    dataSelector: (items: T[]) => PieChartInfo[]
    chartProps?: PieChartProps
}

export interface ContributionChartConfig<T> extends BaseChartConfig<T> {
    type: typeof ChartType.CONTRIBUTION
    dataSelector: (item: T) => ContributionChartInfo
    chartProps?: ContributionGraphProps
}

export type ChartConfig<T> = LineChartConfig<T> | BarChartConfig<T> | ProgressChartConfig<T> | StackedChartConfig<T> | PieChartConfig<T> | ContributionChartConfig<T>

export function getChart(type: ChartType): Function{
    switch (type) {
        case ChartType.BAR:
            return BarChart;
        case ChartType.LINE:
            return LineChart;
        case ChartType.PIE:
            return PieChart;
        case ChartType.PROGRESS:
            return ProgressChart;
        case ChartType.STACKED:
            return StackedBarChart;
        case ChartType.CONTRIBUTION:
            return ContributionGraph;
    }
}

export interface PieChartInfo {
    name: string;
    population: number;
    color: string;
    legendFontColor: string;
    legendFontSize: number;
}

export interface ContributionChartInfo {
    date: string;
    count: number;
}

export type ChartsData = ChartData | LineChartData | StackedBarChartData | ProgressChartData | PieChartInfo[] | ContributionChartInfo[];

export function processItem<T>(config: ChartConfig<T>, items: T[]): ChartsData{
    switch (config.type) {
        case ChartType.LINE:
             return {
                 labels: config.preLabelSelector ? config.preLabelSelector(items).map(i => config.labelSelector(i)) : items.map(i => config.labelSelector(i)),
                 datasets: [
                     {
                         data: items.map(i => config.dataSelector(i))
                     }
                 ]
             }
        case ChartType.BAR:
            return {
                labels: config.preLabelSelector ? config.preLabelSelector(items).map(i => config.labelSelector(i)) : items.map(i => config.labelSelector(i)),
                datasets: [
                    {
                        data: items.map(i => config.dataSelector(i))
                    }
                ]
            }
        case ChartType.PROGRESS:
            return {
                labels: config.preLabelSelector ? config.preLabelSelector(items).map(i => config.labelSelector(i)) : items.map(i => config.labelSelector(i)),
                data: items.map(i => config.dataSelector(i))
            }
        case ChartType.STACKED:
            return {
                labels: config.preLabelSelector ? config.preLabelSelector(items).map(i => config.labelSelector(i)) : items.map(i => config.labelSelector(i)),
                legend: items.map(i => config.legendSelector(i)),
                barColors: items.map(i => config.colorSelector(i)),
                data: items.map(i => config.dataSelector(i)),
            }
        case ChartType.PIE:
            return config.dataSelector(items)
        case ChartType.CONTRIBUTION:
            return items.map(i => config.dataSelector(i))
        default:
            throw new Error("invalid type")
    }
}
