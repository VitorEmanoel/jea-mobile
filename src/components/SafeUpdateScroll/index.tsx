import React, {useState} from 'react';
import {RefreshControl, SafeAreaView, ScrollView, Text} from "react-native";
import {UpdatedDateContainer, UpdatedDateText} from "./style";
import Icon from 'react-native-vector-icons/MaterialIcons'

interface Index {
    children: any

    onRefresh?: () => Promise<void>;
}

const SafeUpdateScroll = (props: Index) => {

    const [refresh, setRefresh] = useState(false)
    const [updatedDate, setUpdatedDate] = useState(new Date())

    const handlerRefresh = () => {
        setRefresh(true)
        setUpdatedDate(new Date())
        props.onRefresh ? props.onRefresh().then(result => setRefresh(false)).catch(error => setRefresh(false)) : setRefresh(false)
    }

    return (
        <SafeAreaView style={{flex: 1}}>
            <ScrollView contentContainerStyle={{flex: 1}} refreshControl={<RefreshControl refreshing={refresh} onRefresh={handlerRefresh}/>}>
                {props.children}
                <UpdatedDateContainer>
                    <UpdatedDateText>
                        Atualizado em {updatedDate.getDay()}/{updatedDate.getMonth()}/{updatedDate.getFullYear()} - {updatedDate.toLocaleTimeString()}
                    </UpdatedDateText>
                </UpdatedDateContainer>
            </ScrollView>
        </SafeAreaView>
    )
}

export default SafeUpdateScroll
