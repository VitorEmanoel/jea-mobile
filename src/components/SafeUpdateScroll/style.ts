import styled from 'styled-components/native';

export const UpdatedDateContainer = styled.View`
  position: absolute;
  bottom: 0;
  right: 0;
  flex-direction: row;
  padding: 4px 0;
  margin-right: 30px;
`;

export const UpdatedDateText = styled.Text`
  color: white;
  font-size: 10px;
  margin-right: 5px;
  align-items: center;
`;
