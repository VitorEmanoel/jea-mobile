import React from 'react';
import {SearchBarContainer, SearchBarIcon, SearchBarInput, SearchBarWrapper} from "./style";
import Icon from 'react-native-vector-icons/MaterialIcons'

interface SearchBarPorps {
    complementOpened: (open: boolean) => void
    complementOpen: boolean
}

const SearchBar = (props: SearchBarPorps) => {

    const handlerInputFocus = () => {
        props.complementOpened(true)
    }

    const handlerInputBlur = () => {
        props.complementOpened(false)
    }

    return (
        <SearchBarWrapper>
            <SearchBarContainer opened={props.complementOpen}>
                <SearchBarInput placeholder={"Digite o nome que deseja buscar"} onFocus={handlerInputFocus} onBlur={handlerInputBlur} />
                <SearchBarIcon>
                    <Icon name={"search"} size={28} color={"#3366FF"}/>
                </SearchBarIcon>
            </SearchBarContainer>
        </SearchBarWrapper>
    )
}

export default SearchBar;
