import React from 'react';
import {Text} from "react-native";
import {ComplementContainer, ComplementFilterItem, ComplementFilterItemText} from "./style";

export interface FilterItem {
    name: string
    displayName: string;
}

interface ComplementProps {
    visible: boolean;
    filters: FilterItem[];
    onItemFilterClick: (item: FilterItem) => void
}

const SearchBarComplement = (props: ComplementProps) => {
    return (
        <>
            {props.visible && (
                <ComplementContainer style={{shadowOffset: {width: 0, height: 3}}}>
                    <ComplementFilterItem>
                        <ComplementFilterItemText>Nome</ComplementFilterItemText>
                    </ComplementFilterItem>
                    <ComplementFilterItem isLast={true}>
                        <ComplementFilterItemText>Status</ComplementFilterItemText>
                    </ComplementFilterItem>
                </ComplementContainer>
            )}
        </>
    )
}

export default SearchBarComplement;
