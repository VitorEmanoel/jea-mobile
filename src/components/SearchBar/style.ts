import styled from 'styled-components/native';

export const SearchBarWrapper = styled.View`
  padding: 10px;
  z-index: 0;
`;

interface ContainerProps {
    opened: boolean
}

export const SearchBarContainer = styled.View<ContainerProps>`
  flex-direction: row;
  width: 100%;
  padding: 0 5px;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  border-bottom-left-radius: ${props => props.opened ? '0px': '20px'};
  border-bottom-right-radius: ${props => props.opened ? '0px': '20px'};
  background-color: #FFF;
  elevation: 1;
  box-shadow: 5px 10px rgba(0,0,0, 0.3);
  align-items: center;
`;

export const SearchBarInput = styled.TextInput`
  color: #6b6b6b;
  padding: 10px;
`;

export const SearchBarIcon = styled.Text`
  margin-left: auto;
  margin-right: 5px;
`;

export const ComplementContainer = styled.View`
  position: absolute;
  border-bottom-right-radius: 10px;
  border-bottom-left-radius: 10px;
  width: 94%;
  background-color: white;
  z-index: 1;
  top: 60px;
  left: 21px;
  elevation: 2;
  box-shadow: 0px 5px 10px rgba(0,0,0,0.5);
`;

interface ComplementItemProps {
    isLast?: boolean
}

export const ComplementFilterItem = styled.TouchableNativeFeedback<ComplementItemProps>`
  padding: 10px 20px;
  border-bottom-width: ${props => props.isLast ? '0px' : '1px'};
  border-bottom-color: #6B6B6B;
`

export const ComplementFilterItemText = styled.Text`
   color: #6B6B6B;
   font-size: 12px;
`;
