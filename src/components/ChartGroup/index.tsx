import React, {useEffect, useState} from 'react';
import {GroupsConfig} from "./groups";
import ModuleChart from "../ModuleChart";
import DateSelector from "../DateSelector";
import {DateOptions} from "../../utils/date";

export interface ChartGroupProps {
    config: GroupsConfig
}

const ChartGroup = (props: ChartGroupProps) => {
    const { showDateSelector, options } = props.config

    const [charts, setCharts] = useState(props.config.charts)
    const [selectedDate, setSelectedDate] = useState(options[0])

    const updateChartTime = () => {
        setCharts(charts => props.config.charts.map(item => {
            if (props.config.sorting)
                item.config.organizeItems = items => items.filter(orgItem => selectedDate.validate(new Date(orgItem.createdAt))).sort(props.config.sorting)
            else
                item.config.organizeItems = items => items.filter(orgItem => selectedDate.validate(new Date(orgItem.createdAt)))
            return item
        }))
    }

    useEffect(() => {
        updateChartTime()
    }, [props.config])

    useEffect(() => {
        updateChartTime()
    }, [selectedDate])

    const handlerDateSelector = (date: DateOptions) => {
        setSelectedDate(date)
    }

    return (
        <>
            {showDateSelector && (
                <DateSelector options={options} onChangeDateSelector={handlerDateSelector}/>
            )}
            {charts.map((item, index) => (
                <ModuleChart key={`${item.module}-${index}`} module={item.module} config={item.config}/>
            ))}
        </>
    )
};

export default ChartGroup;
