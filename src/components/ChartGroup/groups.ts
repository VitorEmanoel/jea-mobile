import {DateOptions} from "../../utils/date";
import {ChartConfig} from "../ModuleChart/charts";
import {Module} from "../../lib/Module";

export interface ChartItem {
    module: string;
    config: ChartConfig<any>
}

export interface GroupsConfig {
    showDateSelector?: boolean;
    options: DateOptions[]
    charts: ChartItem[]
    inCollapseBox?: boolean;
    sorting?: <T>(a: T, b: T) => number;
}
