import styled from "styled-components/native";

export const CompanySelectWrapper = styled.View`
  position: relative;
  padding: 0 5px;
  flex-direction: column;
  background-color: #3366FF;
`;

export const ComapnySelectContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const CompanySelectTitle = styled.Text`
  font-size: 14px;
  color: white;
  width: 40%;
`

export const CompanySelectIcon = styled.Text`
  margin-left: 5px;
`

export const CompanySelectModal = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`

export const CompanyModalContainer = styled.View`
  flex: 1;
  flex-direction: column;
  padding: 5px;
  background-color: #fff;
  border-radius: 5px;
  margin: 10px;
  elevation: 2;
`;


export const CompanyModalHeader = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 10px 0;
`


export const CompanyModalCloseIcon = styled.Text`
    position: absolute;
    top: 0;
    right: 0;
    padding: 5px;
`;

export interface ContainerProps {
    selected: boolean;
}

export const CompanyItemContainer = styled.View`
  flex: 1;
  flex-direction: column;
  border-radius: 5px;
  elevation: 1.8;
  padding: 10px;
  margin: 5px;
  background-color: ${(props: ContainerProps) => props.selected ? "#e3e3e3" : "#FFF"};
`;

export const CompanyItemHeader = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 5px;
`;

export const CompanyItemIdentify = styled.Text`
  font-size: 14px;
  color: #6B6B6B;
  font-weight: bold;
`;

export const CompanyItemName = styled.Text`
  font-size: 16px;
  color: #6B6B6B;
  margin-left: 5px;
`;

export const CompanyItemCity = styled.Text.attrs({numberOfLines: 1})`
  font-size: 16px;
  color: #6B6B6B;
  margin-left: auto;
  max-width: 40%;
`;

export const CompanyItemDescription = styled.View`
  flex-direction: row;
  align-items: center;
`

export const CompanyItemInfo = styled.Text`
  margin-left: auto;
`;

export const CompanyModalTitle = styled.Text`
  text-align: center;
  width: 100%;
  font-size: 16px;
  font-weight: bold;
`;

export const CompanyItemCNPJ = styled.Text`
  color: #6B6B6B;
  font-size: 14px;
`

export const CompanySelectLogo = styled.View`
  align-items: center;
  margin-left: auto;
`;
