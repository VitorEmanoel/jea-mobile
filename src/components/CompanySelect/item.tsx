import React from "react";
import {View, Text, TouchableNativeFeedback, TouchableWithoutFeedback} from "react-native";
import {
    CompanyItemCity,
    CompanyItemContainer,
    CompanyItemHeader,
    CompanyItemIdentify,
    CompanyItemName,
    CompanyItemInfo,
    CompanyItemCNPJ, CompanyItemDescription, CompanyModalTitle
} from "./styles";
import {Company} from "../../modules/Company";
import Icon from "react-native-vector-icons/MaterialIcons";
import {CustomCompany} from "../../store/Company/types";

export interface CompanyItemProps {
    company: CustomCompany
    onCompanyClick: (company: CustomCompany) => void;
}

const CompanyItem = ({company, onCompanyClick}: CompanyItemProps) => {
    return (
        <TouchableWithoutFeedback onPress={() => onCompanyClick(company)}>
        <CompanyItemContainer selected={company.selected}>
            <CompanyItemHeader>
                <CompanyItemIdentify>{company.id}</CompanyItemIdentify>
                <CompanyItemName>{company.name}</CompanyItemName>
                <CompanyItemInfo><Icon name={"info"} size={18} color={"#3366FF"} /></CompanyItemInfo>
            </CompanyItemHeader>
            <CompanyItemDescription>
                <CompanyItemCNPJ>{company.cnpj}</CompanyItemCNPJ>
                <CompanyItemCity>{company.city}</CompanyItemCity>
            </CompanyItemDescription>
        </CompanyItemContainer>
        </TouchableWithoutFeedback>
    )
}

export default CompanyItem;
