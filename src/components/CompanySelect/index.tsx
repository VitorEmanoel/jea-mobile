import React from 'react';
import Icon from "react-native-vector-icons/MaterialIcons"
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../store/types";
import {
    ComapnySelectContainer,
    CompanySelectIcon, CompanySelectLogo,
    CompanySelectTitle,
    CompanySelectWrapper
} from "./styles";
import {TouchableNativeFeedback} from "react-native";
import { bindActionCreators } from "redux";
import * as CompanyActions from "../../store/Company/actions";
import {Logo} from "../../assets/svg";

const CompanySelect = () => {

    const register = useSelector((state: RootState) => state.register)
    const company = useSelector((state: RootState) => state.company)
    const dispatch = useDispatch()

    const companyActions = bindActionCreators(CompanyActions, dispatch)


    const handlerClick = () => {
        companyActions.companyOpenSelect()
        companyActions.findAll({url: register.register.url, data: {token: register.register.token}})
    }

    const selectedItem = company.companies.find(item => item.selected);

    return (
            <TouchableNativeFeedback onPress={handlerClick}>
                <CompanySelectWrapper>
                    <ComapnySelectContainer>
                        {selectedItem && (
                            <CompanySelectTitle numberOfLines={1}>{selectedItem.id} - {selectedItem.name} - {selectedItem.city}</CompanySelectTitle>
                        )}
                        <CompanySelectIcon><Icon name={"sync"} size={18} color={"white"}/></CompanySelectIcon>
                        <CompanySelectLogo>
                            <Logo width={70} height={42}/>
                        </CompanySelectLogo>
                    </ComapnySelectContainer>
                </CompanySelectWrapper>
            </TouchableNativeFeedback>
    )
}

export default CompanySelect;
