import React from 'react';
import {FlatList, Modal, Text, ToastAndroid, TouchableNativeFeedback} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../store/types";
import {
    CompanyItemContainer,
    CompanyModalCloseIcon,
    CompanyModalContainer,
    CompanyModalHeader, CompanyModalTitle,
    CompanySelectModal
} from "./styles";
import Icon from "react-native-vector-icons/MaterialIcons"
import { bindActionCreators } from "redux";
import * as CompanyActions from "../../store/Company/actions";
import CompanyItem from "./item";
import {CustomCompany} from "../../store/Company/types";
import * as ModuleActions from "../../lib/Module/actions";

const CompanyModal = () => {

    const register = useSelector((state: RootState) => state.register)
    const dashboard = useSelector((state: RootState) => state.dashboard)
    const companies = useSelector((state: RootState) => state.company)

    const dispatch = useDispatch();

    const moduleActions = bindActionCreators(ModuleActions, dispatch)
    const companyActions = bindActionCreators(CompanyActions, dispatch)

    const handlerClickClose = () => {
        companyActions.companyCloseSelect()
    }

    const handlerCompanyClicked = (company: CustomCompany) => {
        if (companies.companies.filter(i => i.selected).length === 1 && company.selected ){
            ToastAndroid.showWithGravity("Mantenha pelomenos uma empresa selecionada!", 4000, ToastAndroid.BOTTOM)
            return
        }
        companyActions.companySelect(company.id)
        moduleActions.FindAll(dashboard.selected, {url: register.register.url, data: {token: register.register.token}})
    }

    return (
        <Modal
            visible={companies.open}
            presentationStyle={"overFullScreen"}
            animationType={"slide"}
            transparent={true}
        >
            <CompanyModalContainer>
                <CompanyModalHeader>
                    <CompanyModalTitle>Empresas:</CompanyModalTitle>
                    <TouchableNativeFeedback onPress={handlerClickClose}>
                        <CompanyModalCloseIcon><Icon name={"close"} size={24} color={"#3366FF"}/></CompanyModalCloseIcon>
                    </TouchableNativeFeedback>
                </CompanyModalHeader>
                <FlatList data={companies.companies} renderItem={(item) => <CompanyItem onCompanyClick={handlerCompanyClicked} company={item.item}/>} keyExtractor={i => i.id.toString()}/>
            </CompanyModalContainer>
        </Modal>
    )
}

export default CompanyModal;
