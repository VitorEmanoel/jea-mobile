import styled from "styled-components/native";

interface WrapperProps {
    open: boolean;
}

export const CollapseWrapper = styled.View`
  flex-direction: column;
  overflow: hidden;
  background-color: white;
  height: ${(props: WrapperProps) => props.open ? 'auto' : '50px'} ;
  elevation: 1;
  margin: 0 5px;
  border-radius: 5px;
`;

interface HeaderProps {
    open: boolean
}

export const CollapseHeader = styled.View`
  align-items: center;
  flex-direction: row;
  margin-bottom: 10px;
  padding: 10px;
  border-bottom-width: ${(props: HeaderProps) => props.open ? '1px' : '0'};
  border-bottom-color: rgba(0,0,0,0.1);
`;

interface BodyProps {
    open: boolean
}

export const CollapseBody = styled.View`
   flex-direction: column;
   width: ${(props: BodyProps) => props.open ? 'auto' : '0'};
`

export const CollapseTitle = styled.Text`
  font-size: 18px;
  color: #6B6B6B;
`

export const CollapseIcon = styled.Text`
  margin-left: auto;
`
