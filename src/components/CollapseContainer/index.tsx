import React, {useState} from "react";
import {CollapseWrapper, CollapseHeader, CollapseBody, CollapseTitle, CollapseIcon} from "./styles";
import {TouchableNativeFeedback} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons"

export interface CollapseContainerProps {
    title: string;
    children: JSX.Element[]
    startOpen?: boolean;
}


const CollapseContainer = (props: CollapseContainerProps) => {

    const { title, children, startOpen } = props;

    const [open, setOpen] = useState(startOpen === undefined ? true : startOpen)

    const handlerOpen = () => {
        setOpen(!open)
    }

    return (
        <CollapseWrapper open={open}>
            <CollapseHeader open={open}>
                <CollapseTitle>{title}</CollapseTitle>
                <TouchableNativeFeedback onPress={handlerOpen}><CollapseIcon><Icon size={24} name={open ? "keyboard-arrow-up" : "keyboard-arrow-down"}/></CollapseIcon></TouchableNativeFeedback>
            </CollapseHeader>
            <CollapseBody open={open}>
                {children}
            </CollapseBody>
        </CollapseWrapper>
    );
};

export default CollapseContainer;
