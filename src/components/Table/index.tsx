import React from "react";
import {TableColumn, TableContainer, TableRow} from "./styles";

export interface TableProps {
    components: JSX.Element[][];
    color: string;
}

const Table = (props: TableProps) => {
    const { components, color } = props;
    return (
        <TableContainer color={color}>
            {components.map((row, index) =>
                <TableRow color={color} isLast={components.length - 1 === index}>
                    {row.map((column, cIndex) => <TableColumn color={color} isLast={row.length - 1 === cIndex}>{column}</TableColumn>)}
                </TableRow>
            )}
        </TableContainer>
    )
}

export default Table;
