import styled from "styled-components/native";

interface TableProps {
    color: string;
}

export const TableContainer = styled.View`
  flex: 1;
  flex-direction: column;
  border: 1px solid ${(props: TableProps) => props.color}; 
`;

interface TableRowProps {
    color: string;
    isLast: boolean;
}

export const TableRow = styled.View`
  flex: 1;
  flex-direction: row;
  border-bottom-width: ${(props: TableRowProps) => !props.isLast ? '1px' : '0'};
  border-bottom-color: ${(props: TableRowProps) => !props.isLast ? props.color : 'transparent'};
`;

interface TableColumnProps {
    color: string;
    isLast: boolean;
}

export const TableColumn = styled.View`
  flex: 1;
  flex-direction: column;
  border-right-width: ${(props: TableColumnProps) => !props.isLast ? '1px' : '0'};
  border-right-color: ${(props: TableColumnProps) => !props.isLast ? props.color : 'transparent'};
`;
