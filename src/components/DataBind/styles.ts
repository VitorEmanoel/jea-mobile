// @ts-ignore
import styled from 'styled-components/native';

interface  DataBindWrapperProps {
    color: string
}


export const DataBindTitle = styled.Text`
  width: 100%;
  text-align: center;
  justify-content: center;
  margin-bottom: 5px;
  font-size: 22px;
  color: white;  
`;

export const DataBindValue = styled.Text`
  margin-top: 20px;
  font-size: 26px;
  color: white;
  text-align: center;
  width: 100%;
`;

export const DataBindWrapper = styled.View`
  position: relative;
  width: 100%;
  padding: 10px;
  border-radius: 10px;
  background-color: ${(props:DataBindWrapperProps) => props.color};
`;

export const DataBindTitleIcon = styled.View`
  position: absolute;
  right: 0;
  top: 0;
  margin-left: 10px;
`

export const DataBindTitleContainer = styled.View`
  position: relative;
  flex-direction: row;
  align-items: center;
`;
