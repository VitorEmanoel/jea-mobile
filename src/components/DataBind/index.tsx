import * as React from 'react'
import {
    DataBindWrapper,
    DataBindTitle,
    DataBindValue,
    DataBindTitleContainer,
    DataBindTitleIcon,
} from './styles';
import Icon from 'react-native-vector-icons/MaterialIcons';

export interface DataBindProps {
    title: string;
    value: number;
    color: string;
    chart?: boolean;
}

const DataBind = (props: DataBindProps) => {

    return (
        <DataBindWrapper color={props.color}>
            <DataBindTitleContainer>
                <DataBindTitle>{props.title}</DataBindTitle>
                <DataBindTitleIcon><Icon color={"white"} size={24} name={"info"}/></DataBindTitleIcon>
            </DataBindTitleContainer>
            <DataBindValue>R$ {props.value.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</DataBindValue>
        </DataBindWrapper>
    )
}

export default DataBind;
