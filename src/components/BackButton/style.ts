import styled from 'styled-components/native';

export const BackButtonContainer = styled.TouchableOpacity`
  position: absolute;
  top: 0;
  left: 0;
  align-items: center;
  padding: 10px;
  margin-left: 10px;
  margin-top: 2.5px;
  justify-content: center;
  flex-direction: row;
`;

export const BackButtonText = styled.Text`
  color: white;
  font-weight: bold;
  font-size: 14px;
  margin-left: 5px;
`;

