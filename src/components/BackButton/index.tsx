import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons'
import {BackButtonContainer, BackButtonText} from "./style";

import { StackNavigationProp} from "@react-navigation/stack"


interface BackButtonProps {
    navigation: StackNavigationProp<any>
    backRoute: string;
    data?: any
}

const BackButton = (props: BackButtonProps) => {

    const handlerBackButtonPress = () => {
        props.navigation.push(props.backRoute, props.data)
    }

    return (
        <BackButtonContainer onPress={handlerBackButtonPress}>
            <Icon name={"keyboard-backspace"} color={"white"} size={32}/>
            <BackButtonText>Voltar</BackButtonText>
        </BackButtonContainer>
    )
}

export default BackButton;
