import styled from "styled-components/native";

export const SimpleListContainer = styled.View`
  margin: 10px;
  padding: 2.5px;
  border-radius: 5px;
  background-color: #F3F7FF;
`;
