import React from 'react';
import {FlatList, FlatListProps} from "react-native";
import {SimpleListContainer} from "./styles";

export interface SimpleListProps<T> {
    data: T[] | undefined
    render: (item:T, index?:number) => React.ReactElement;
    config?: FlatListProps<T>
}
const SimpleList = <T,>(props: SimpleListProps<T>) => {
    const { render, data, config } = props;
    return (
        <SimpleListContainer>
            <FlatList keyExtractor={((item, index) => index.toString())} {...config} data={data} renderItem={info => render(info.item, info.index)}/>
        </SimpleListContainer>
    )
}

export default SimpleList
