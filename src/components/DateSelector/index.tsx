import React, {useState} from 'react';
import {DateSelectorItemContainer, DateSelectorItemText, DateSelectorWrapper} from "./styles";
import {DateOptions, Languages, Month} from "../../utils/date";
import {TouchableNativeFeedback} from "react-native";

interface DateSelectorProps {
    options: DateOptions[]
    onChangeDateSelector: (option: DateOptions) => void;
    center?: boolean;
}

const DateSelector = (props: DateSelectorProps) => {

    const { options, onChangeDateSelector } = props;

    const [selected, setSelected] = useState("")

    const handlerSelectMonth = (option: DateOptions) => {
        setSelected(option.text)
        onChangeDateSelector(option)
    }

    return (
        <DateSelectorWrapper horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{width: "100%",alignItems: "center", justifyContent: "center"}}>
            {options.map((item, index) => {
                if (index === 0 && selected === "")
                    handlerSelectMonth(item)
                return (
                    <TouchableNativeFeedback key={item.text} onPress={() => handlerSelectMonth(item)}>
                        <DateSelectorItemContainer selected={item.text === selected}>
                            <DateSelectorItemText selected={item.text === selected}>{item.text}</DateSelectorItemText>
                        </DateSelectorItemContainer>
                    </TouchableNativeFeedback>
                )
            })}
        </DateSelectorWrapper>
    )
}

export default DateSelector;
