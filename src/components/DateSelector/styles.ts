import styled from 'styled-components/native';
export const DateSelectorWrapper = styled.ScrollView`
   flex-direction: row;
   width: 100%;
`;

interface ItemContainerProps {
    selected: boolean
}

export const DateSelectorItemContainer = styled.View`
  background-color: ${(props: ItemContainerProps) => props.selected ? '#3366FF' : 'white'};
  elevation: 1.5;
  padding: 5px 10px;
  border-radius: 5px;
  margin: 5px;
`;

interface ItemTextProps {
    selected: boolean
}

export const DateSelectorItemText = styled.Text`
  color: ${(props: ItemTextProps) => props.selected ? 'white' : '#6B6B6B'};
  font-size: 16px;
`

