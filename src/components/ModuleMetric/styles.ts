import styled from "styled-components/native";

export const MetricWrapper = styled.View`
  flex-direction: column;
  padding: 10px;
  margin: 5px;
  width: 100%;
`;

export const MetricContainer = styled.View`
  background-color: white;
  flex-direction: column;
  elevation: 1;
  border-radius: 10px;
`;

export const MetricTitle = styled.Text`
  font-size: 18px;
  color: #6B6B6B;
  margin-right: 20px;
`;

export const MetricCollapseIcon = styled.Text`
  margin-left: auto;
  align-items: center;
`;

export const MetricIcon = styled.Text`
  margin-right: 8px;
`

export const MetricHeader = styled.View`
  flex-direction: row;
  padding: 7px 10px;
  align-items: center;
`;


interface BodyProps {
    open: boolean
}

export const MetricBody = styled.View`
  flex-direction: column;
  overflow: hidden;
  padding: ${(props: BodyProps) => props.open ? '0px 10px 10px 10px' : '0'};
  height: ${(props: BodyProps) => props.open ? 'auto' : '0px'};
`;


export const MetricValue = styled.Text`
  font-size: 28px;
  color: #6B6B6B;
  margin-left: 10px;
  text-align: center;
`;

export const MetricStatistics = styled.Text`
  margin-top: 5px;
  font-size: 15px;
  color: #7b7b7b;
  text-align: center;
`;

interface StatisticsValueProps {
    positve: boolean;
}

export const MetricStatisticsValue = styled.Text<StatisticsValueProps>`
  font-size: 15px;
  color: ${(props: StatisticsValueProps) => props.positve ? "#498b3d" : "#e72008"}
`;
