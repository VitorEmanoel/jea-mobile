import React, {useState} from 'react';
import {
    MetricBody,
    MetricCollapseIcon,
    MetricContainer,
    MetricHeader,
    MetricIcon, MetricStatistics, MetricStatisticsValue,
    MetricTitle, MetricValue,
    MetricWrapper
} from "./styles";
import {Module, ModuleState} from "../../lib/Module";
import Icon from "react-native-vector-icons/MaterialIcons"
import {TouchableNativeFeedback, Text} from "react-native";
import {useSelector} from "react-redux";

interface ModuleMetricProps<T> {
    module: Module<T>
    title: string;
    icon?: string;
    conifg: MetricConfig<T>
}

const ModuleMetric = <T,>(props: ModuleMetricProps<T>) => {

    const { title, icon } = props

    const [open, setOpen] = useState(false)

    const moduleState:ModuleState<T> = useSelector((state:any) => state[props.module.name])
    const data = props.conifg.dataSelector(moduleState.rows)
    const dataCompareValue = props.conifg.dataCompareValue(moduleState.rows)

    const handlerToggleOpen = () => {
        setOpen(!open)
    }

    return (
        <MetricWrapper>
            <MetricContainer>
                <MetricHeader>
                    {icon && <MetricIcon><Icon name={icon} size={20}/></MetricIcon>}
                    <MetricTitle>{title}</MetricTitle>
                    <TouchableNativeFeedback onPress={handlerToggleOpen}>
                        <MetricCollapseIcon><Icon name={open ? "keyboard-arrow-up": "keyboard-arrow-down"} size={24}/></MetricCollapseIcon>
                    </TouchableNativeFeedback>
                </MetricHeader>
                <MetricBody open={open}>
                    <MetricValue>{data}</MetricValue>
                    {props.conifg.dataCompare && (
                        <MetricStatistics><MetricStatisticsValue positve={dataCompareValue >= 0}>{dataCompareValue}%</MetricStatisticsValue> a mais que mês passado</MetricStatistics>
                    )}
                </MetricBody>
            </MetricContainer>
        </MetricWrapper>
    )
}

export default ModuleMetric;
