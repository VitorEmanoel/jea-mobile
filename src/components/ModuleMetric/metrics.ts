interface MetricConfig<T> {
    organize?: (items: T[]) => T[]
    dataSelector(items: T[]): number;
    dataCompare: boolean;
    dataCompareValue: (items: T[]) => number;
}
