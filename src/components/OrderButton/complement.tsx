import React from 'react';
import {OrderButtonContainer, OrderButtonFilterItem, OrderButtonFilterItemText} from "./style";

export interface OrderFilterItem {
    displayName: string;
    name: string;
    default?: boolean
}

interface ComplementProps {
    visible: boolean;
    orderFilterItems: OrderFilterItem[];
    onItemClick: (item: OrderFilterItem) => void;
}

const OrderButtonComplement = (props: ComplementProps) => {

    return (
        <>
            {props.visible && (
                <OrderButtonContainer>
                    {props.orderFilterItems.map((item, index) => (
                        <OrderButtonFilterItem isLast={props.orderFilterItems.length - 1 === index} key={`${item.name}-${index}`} onPress={() => props.onItemClick(item)}>
                            <OrderButtonFilterItemText>
                                {item.displayName}
                            </OrderButtonFilterItemText>
                        </OrderButtonFilterItem>
                    ))}
                </OrderButtonContainer>
            )}
        </>
    )
}

export default OrderButtonComplement;
