import React, { useState } from 'react';
import {Text} from "react-native";
import {
    OrderButtonButton,
    OrderButtonContainer,
    OrderButtonFilterItem,
    OrderButtonIcon,
    OrderButtonWrapper
} from "./style";
import Icon from "react-native-vector-icons/MaterialIcons";

export interface OrderButtonProps {
    onOrderClick: () => void
}

const OrderButton = (props: OrderButtonProps) => {

    return (
        <OrderButtonWrapper>
            <OrderButtonButton onPress={props.onOrderClick}>
                <OrderButtonIcon><Icon name={"filter-list"} size={24} color={"#3366FF"}/></OrderButtonIcon>
            </OrderButtonButton>
        </OrderButtonWrapper>
    )
}

export default OrderButton;
