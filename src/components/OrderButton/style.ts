import styled from 'styled-components/native';

export const OrderButtonWrapper = styled.View`
  position: relative;
`;

export const OrderButtonButton = styled.TouchableNativeFeedback`
`;

export const OrderButtonIcon = styled.Text`
`;

export const OrderButtonContainer = styled.View`
  flex-direction: column;
  position: absolute;
  elevation: 1;
  top: 30px;
  right: 30px;
  background-color: white;
  z-index: 9999;
  border-radius: 5px;
`;

interface FilterItemProps {
    isLast: boolean;
}

export const OrderButtonFilterItem = styled.TouchableNativeFeedback<FilterItemProps>`
  border-bottom-width: ${(props) => props.isLast ? '0px' : '1px'};
  border-bottom-color: #6B6B6B;
  padding: 5px 30px 5px 10px;
`;

export const OrderButtonFilterItemText = styled.Text`
  font-size: 12px;
  padding: 2.5px 0;
  color: #6B6B6B;
`
