import React from "react";
import {ModuleItemContainer, ModuleItemIcon, ModuleItemTitle} from "./style";
import Icon from 'react-native-vector-icons/MaterialIcons'
import {TouchableNativeFeedback} from "react-native";
import {Module} from "../../lib/Module";

interface ModuleItemProps {
    onModuleClick?: (item: Module<any>) => void
}

const ModuleItem = (props: ModuleItemProps & Module<any> ) => {

    return (
        <TouchableNativeFeedback delayLongPress={1000} onLongPress={() => {}} onPress={() => props.onModuleClick && props.onModuleClick({...props})}>
            <ModuleItemContainer color={props.colors.module}>
                <ModuleItemIcon><Icon name={props.icon.module} size={32} color={"white"}/></ModuleItemIcon>
                <ModuleItemTitle numberOfLines={2} ellipsizeMode={"head"}>{props.displayName}</ModuleItemTitle>
            </ModuleItemContainer>
        </TouchableNativeFeedback>
    )

}

export default ModuleItem
