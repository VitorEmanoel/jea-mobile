import styled from "styled-components/native";

interface ModuleItemContainerProps {
    color?: string
}


export const ModuleItemContainer = styled.View<ModuleItemContainerProps>`
  padding: 20px;
  border-radius: 10px;
  box-shadow: 0 2px 10px rgba(0,0,0, 0.1);
  flex-direction: column;
  background-color: ${props => props.color};
  align-items: center;
  justify-content: center;
  width: 150px;
  height: 150px;
  margin: 10px;
`;

export const ModuleItemIcon = styled.View`
  align-items: center;
  justify-content: center;
  padding-bottom: 5px;
`;

export const ModuleItemTitle = styled.Text`
  width: 100%;
  text-align: center;
  color: white;
  font-size: 16px;
`;
