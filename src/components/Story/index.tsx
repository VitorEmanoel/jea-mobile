import React from 'react'
import {FlatList, View} from "react-native";
import {StoryWrapper} from "./style";
import StoryItem from "../StoryItem";
import {Module} from "../../lib/Module";
import {useDispatch, useSelector} from "react-redux";
import { bindActionCreators } from "redux";
import * as DashboardActions from "../../store/Dashboard/actions";
import {RootState} from "../../store/types";

interface StoryProps {
    storyItems: Module<any>[]
    onItemClick?: (item: Module<any>) => void
}

const Story = (props: StoryProps) => {

    const dashboard = useSelector((state: RootState) => state.dashboard)

    const dispatch = useDispatch()
    const dashboardAction = bindActionCreators(DashboardActions, dispatch)



    return (
        <StoryWrapper>
            <FlatList showsHorizontalScrollIndicator={false} horizontal={true} data={props.storyItems} renderItem={(item) => {
                if (dashboard.selected === "" && item.index === 0) {
                    dashboardAction.selectModule(item.item.name)
                }

                return  (
                    <>{item.item.configuration.showInStory && (<StoryItem item={item.item} onItemClick={props.onItemClick}/>)}</>
                )
            }} keyExtractor={(item, index) => index.toString()}/>
        </StoryWrapper>
    )
}

export default Story;


