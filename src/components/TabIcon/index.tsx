import React from 'react';
import Icon from "react-native-vector-icons/MaterialIcons";

interface TabIconProps {
    focused: boolean;
    icon: string;
}

const TabIcon = (props: TabIconProps) => <Icon name={props.icon} color={props.focused ? "#bedcfe": "#fff"} size={36}/>

export default TabIcon;
