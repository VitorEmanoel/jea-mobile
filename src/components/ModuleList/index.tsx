import React, {useEffect, useState} from "react";
import ModuleItem from "../ModuleItem";
import {
    ModuleItemsContainer,
    ModuleListWrapper,
    ModulesListLeft,
    ModulesListRight,
    ModulesListWrapper
} from "./style";
import Icon from 'react-native-vector-icons/MaterialIcons'
import {Module} from "../../lib/Module";
import {useSelector} from "react-redux";
import {NativeScrollEvent, NativeSyntheticEvent} from "react-native";

interface ModulesProps {
    modules: Module<any>[]
    onModuleClick?: (item: Module<any>) => void;
}
const ModuleList = (props: ModulesProps) => {


    return (
        <ModulesListWrapper showsVerticalScrollIndicator={false}>
        <ModuleListWrapper>
            {props.modules.filter(item => item.configuration.showInModuleList).map((module, index) => (
                <ModuleItem {...module} key={index + "-" + module.name} onModuleClick={props.onModuleClick} />
            ))}
        </ModuleListWrapper>
        </ModulesListWrapper>
    )
}

export default ModuleList;
