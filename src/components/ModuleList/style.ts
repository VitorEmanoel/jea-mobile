import styled from "styled-components/native";

import { Dimensions } from "react-native";
import {Module} from "../../lib/Module";

export const ModuleListWrapper = styled.View`
  justify-content: center;
  margin: 20px auto 0 auto;
  position: relative;
  flex-direction: row;
  flex-wrap: wrap;
`;

interface ModuleItemsProps {
    last: boolean;
}

export const ModuleItemsContainer = styled.View`
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: row;
  width: ${Dimensions.get("window").width - 40}px;
  flex-wrap: wrap;
  margin-right: 25px;
`;

export const ModulesListWrapper = styled.ScrollView`
  flex: 1;
  width: 100%;
`;

export const ModulesListRight = styled.Text`
  position: absolute;
  right: 0;
`;

export const ModulesListLeft = styled.Text`
  position: absolute;
  left: 0;
`
